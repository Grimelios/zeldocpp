#include "MainGame.h"
#include "NibbleEngine/Resolution.h"

namespace Zeldo
{
	MainGame::MainGame(const int windowWidth, const int windowHeight) : Game("Zeldo", windowWidth, windowHeight)
	{
	}

	void MainGame::Initialize()
	{
		glClearColor(0, 0, 0, 1);
	}

	void MainGame::Update(const float dt)
	{
		inputProcessor.Update(dt);
		player.Update(dt);
	}

	void MainGame::Draw()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glViewport(0, 0, Nibble::Resolution::WindowWidth, Nibble::Resolution::WindowHeight);
		glDepthFunc(GL_NEVER);

		player.Draw(sb);
		sb.Flush();
	}
}
