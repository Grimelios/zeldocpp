#pragma once
#include "NibbleEngine/Game.h"
#include "NibbleEngine/SpriteBatch.h"
#include "Player.h"

namespace Zeldo
{
	class MainGame : public Nibble::Game
	{
	private:

		static constexpr int GroundLevel = 500;

		Player player;

		Nibble::SpriteBatch sb;

	protected:

		void Update(float dt) override;
		void Draw() override;

	public:

		MainGame(int windowWidth, int windowHeight);

		void Initialize() override;
	};
}

