#pragma once
#include "NibbleEngine/IDynamic.h"
#include "NibbleEngine/IRenderable2D.h"
#include "NibbleEngine/Sprite.h"
#include "NibbleEngine/MessageHandle.h"
#include "PlayerData.h"
#include "PlayerControls.h"

namespace Nibble
{
	class AggregateData;
}

namespace Zeldo
{
	class Player : public Nibble::IDynamic, public Nibble::IRenderable2D
	{
	private:

		Nibble::Sprite sprite;
		Nibble::MessageHandle messageHandle;

		glm::vec2 position;
		glm::vec2 velocity;

		PlayerData playerData;
		PlayerControls controls;

		void ProcessInput(const Nibble::AggregateData& data, float dt);

	public:

		Player();

		void SetPosition(const glm::vec2& position);
		void Update(float dt) override;
		void Draw(Nibble::SpriteBatch& sb) override;
	};
}
