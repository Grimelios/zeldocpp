#include "MainGame.h"

int main(const int argc, const char* argv[])
{
	if (argc != 3)
	{
		throw std::exception("Two program arguments expected (window width and height).");
	}

	const int width = std::stoi(argv[1]);
	const int height = std::stoi(argv[2]);

	Zeldo::MainGame game(width, height);
	game.Initialize();
	game.Run();

	return 0;
}
