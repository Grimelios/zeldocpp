#include "PlayerData.h"
#include "NibbleEngine/JsonUtilities.h"

namespace Zeldo
{
	PlayerData::PlayerData()
	{
		const auto j = Nibble::JsonUtilities::Load("PlayerData.json");

		runAcceleration = j.at("RunAcceleration").get<float>();
		runDeceleration = j.at("RunDeceleration").get<float>();
		runMaxSpeed = j.at("RunMaxSpeed").get<float>();
	}
}
