#pragma once

namespace Zeldo
{
	class PlayerData
	{
	private:

		friend class Player;

		float runAcceleration = 0;
		float runDeceleration = 0;
		float runMaxSpeed = 0;

	public:

		PlayerData();
	};
}
