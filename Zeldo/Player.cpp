#include "Player.h"
#include "NibbleEngine/AggregateData.h"
#include "NibbleEngine/MessageSystem.h"
#include "NibbleEngine/GameFunctions.h"

namespace Zeldo
{
	Player::Player() : sprite("Player.png")
	{
		Nibble::MessageSystem::Subscribe(Nibble::MessageTypes::Input, messageHandle,
			[this](const std::any& data, const float dt)
			{
				ProcessInput(std::any_cast<Nibble::AggregateData>(data), dt);
			});
	}

	void Player::ProcessInput(const Nibble::AggregateData& data, const float dt)
	{
		const bool left = data.Query(controls.runLeft, Nibble::InputStates::Held);
		const bool right = data.Query(controls.runRight, Nibble::InputStates::Held);
		const bool up = data.Query(controls.runUp, Nibble::InputStates::Held);
		const bool down = data.Query(controls.runDown, Nibble::InputStates::Held);

		glm::vec2 acceleration = glm::vec2(0);

		if (left ^ right)
		{
			acceleration.x = left ? -1.0f : 1.0f;
		}

		if (up ^ down)
		{
			acceleration.y = up ? -1.0f : 1.0f;
		}

		if (acceleration != glm::vec2(0))
		{
			velocity += normalize(acceleration) * playerData.runAcceleration * dt;

			const float maxSpeed = playerData.runMaxSpeed;

			if (Nibble::GameFunctions::ComputeLengthSquared(velocity) > maxSpeed * maxSpeed)
			{
				velocity = normalize(velocity) * maxSpeed;
			}
		}
		else if (velocity != glm::vec2(0))
		{
			const bool useX = velocity.x != 0;
			const int oldSign = Nibble::GameFunctions::ComputeSign(useX ? velocity.x : velocity.y);

			velocity -= normalize(velocity) * playerData.runDeceleration * dt;

			const int newSign = Nibble::GameFunctions::ComputeSign(useX ? velocity.x : velocity.y);

			if (oldSign != newSign)
			{
				velocity = glm::vec2(0);
			}
		}

		if (data.Query(controls.primaryAttack, Nibble::InputStates::PressedThisFrame))
		{
			
		}
	}

	void Player::SetPosition(const glm::vec2& position)
	{
		sprite.SetPosition(position);
	}

	void Player::Update(const float dt)
	{
		position += velocity * dt;

		SetPosition(position);
	}

	void Player::Draw(Nibble::SpriteBatch& sb)
	{
		sprite.Draw(sb);
	}
}
