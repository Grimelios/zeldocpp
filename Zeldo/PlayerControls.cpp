#include "PlayerControls.h"
#include "NibbleEngine/InputHelper.h"

namespace Zeldo
{
	PlayerControls::PlayerControls()
	{
		const auto binds = Nibble::InputHelper::ParseBinds("PlayerControls.json");

		runLeft = binds.at("RunLeft");
		runRight = binds.at("RunRight");
		runUp = binds.at("RunUp");
		runDown = binds.at("RunDown");
	}
}
