#pragma once
#include "NibbleEngine/InputBind.h"
#include <vector>

namespace Zeldo
{
	class PlayerControls
	{
	private:

		friend class Player;

		std::vector<Nibble::InputBind> runLeft;
		std::vector<Nibble::InputBind> runRight;
		std::vector<Nibble::InputBind> runUp;
		std::vector<Nibble::InputBind> runDown;
		std::vector<Nibble::InputBind> primaryAttack;

	public:

		PlayerControls();
	};
}
