#pragma once
#include "MeshConverter.h"

namespace Nibble
{
	class ObjConverter : public MeshConverter
	{
	private:

		std::string ParseTexture(const std::string& materialPath) const;

	public:

		MeshPair ParseData(const std::string& path) override;
	};
}
