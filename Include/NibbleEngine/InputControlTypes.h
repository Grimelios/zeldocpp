#pragma once

namespace Nibble
{
	enum class InputControlTypes
	{
		Button,
		Checkbox,
		Dropdown,
		Slider,
		Textbox,
		Count
	};
}
