#pragma once

namespace Nibble
{
	class Camera;
	class IRenderable3D
	{
	public:

		virtual ~IRenderable3D() = default;
		virtual void Draw(const Camera& camera) = 0;
	};
}
