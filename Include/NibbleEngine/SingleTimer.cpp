#include "SingleTimer.h"

namespace Nibble
{
	SingleTimer::SingleTimer(const SingleFunction& trigger, const float elapsed) :
		SingleTimer(0, trigger, elapsed)
	{
	}

	SingleTimer::SingleTimer(const int duration, SingleFunction trigger, const float elapsed) :	Timer(duration, elapsed),
		trigger(std::move(trigger))
	{
	}

	void SingleTimer::Update(const float dt)
	{
		if (paused)
		{
			return;
		}

		elapsed += dt;

		if (elapsed >= duration)
		{
			if (tick.has_value())
			{
				tick.value()(1);
			}

			trigger(elapsed - duration);
			completed = true;

			return;
		}

		if (tick.has_value())
		{
			tick.value()(elapsed / duration);
		}
	}
}
