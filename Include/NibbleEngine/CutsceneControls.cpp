#include "CutsceneControls.h"
#include "InputHelper.h"

namespace Nibble
{
	void CutsceneControls::Refresh(const Json& j)
	{
		const auto map = InputHelper::ParseBinds(j);

		pause = map.at("Pause");
		skip = map.at("Skip");
	}
}
