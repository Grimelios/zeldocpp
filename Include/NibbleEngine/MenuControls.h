#pragma once
#include <vector>
#include "InputBind.h"

namespace Nibble
{
	class MenuControls
	{
	public:

		std::vector<InputBind> up;
		std::vector<InputBind> down;
		std::vector<InputBind> left;
		std::vector<InputBind> right;
		std::vector<InputBind> submit;
		std::vector<InputBind> back;
	};
}
