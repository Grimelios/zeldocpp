#pragma once
#include <array>
#include "Shader.h"
#include <glm/mat4x4.hpp>
#include <glad/glad.h>
#include "MessageHandle.h"

namespace Nibble
{
	enum class BatchCoords
	{
		Screen,
		World
	};

	class Color;
	class Bounds2D;
	class Rectangle;
	class Texture2D;
	class SpriteBatch
	{
	private:

		static const int BufferCapacity = 40000;
		static const int IndexCapacity = 2000;

		using IndexList = std::vector<unsigned short>;

		Shader spriteShader;
		Shader primitiveShader;
		Shader* activeShader = nullptr;

		MessageHandle handle;

		GLuint bufferId = 0;
		GLuint indexBufferId = 0;
		GLuint activeTexture = 0;
		GLenum mode = 0;

		glm::mat4 projection = glm::mat4(0);
		glm::mat4 screenMatrix = glm::mat4(0);

		std::array<std::byte, BufferCapacity> buffer { };
		std::array<unsigned short, IndexCapacity> indexBuffer { };
		std::byte* currentPointer = &buffer[0];

		int bufferSize = 0;

		// Using two variables allows for primitive restarting.
		int indexCount = 0;
		int indexStart = 0;

		BatchCoords coords = BatchCoords::Screen;

		bool restartEnabled = false;

		void OnResize();
		void FillArea(const std::array<glm::ivec2, 4>& points, const Color& color);
		void Buffer(const std::byte* buffer, unsigned vertexCount, unsigned byteCount);

	public:

		SpriteBatch();
		~SpriteBatch();

		void Apply(Shader& shader);
		void UseSpriteShader();
		void SetMode(GLenum mode);
		void BindTexture(GLuint textureId);
		void DrawLine(const glm::vec2& p1, const glm::vec2& p2, const Color& color);
		void DrawBounds(const Bounds2D& bounds, const Color& color);
		void DrawRectangle(const Rectangle& rect, const Color& color);
		void FillBounds(const Bounds2D& bounds, const Color& color);
		void FillRectangle(const Rectangle& rect, const Color& color);

		template<class T, unsigned S>
		void Buffer(const std::array<T, S>& data);

		template<class T>
		void Buffer(const T* data, unsigned size);
		void Flush();
	};

	template<class T, unsigned S>
	void SpriteBatch::Buffer(const std::array<T, S>& data)
	{
		const unsigned byteCount = static_cast<unsigned>(sizeof(T)) * S;
		const unsigned vertexCount = byteCount / activeShader->GetStride();

		Buffer(reinterpret_cast<const std::byte*>(&data[0]), vertexCount, byteCount);
	}

	template<class T>
	void SpriteBatch::Buffer(const T* data, const unsigned size)
	{
		const unsigned byteCount = static_cast<unsigned>(sizeof(T)) * size;
		const unsigned vertexCount = byteCount / activeShader->GetStride();

		Buffer(reinterpret_cast<const std::byte*>(data), vertexCount, byteCount);
	}
}
