#pragma once
#include "Shape3D.h"

namespace Nibble
{
	class Sphere : public Shape3D
	{
	public:

		Sphere();
		explicit Sphere(float radius);

		float radius;

		float ComputeVolume() const override;
	};
}
