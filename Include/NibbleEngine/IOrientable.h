#pragma once
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class IOrientable
	{
	public:

		virtual ~IOrientable() = default;

		virtual const glm::quat& GetOrientation() const = 0;
		virtual void SetOrientation(const glm::quat& orientation) = 0;
	};
}
