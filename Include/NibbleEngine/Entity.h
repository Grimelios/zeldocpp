#pragma once
#include <glm/vec3.hpp>
#include <nlohmann/json.hpp>
#include "BodyTypes.h"
#include "IDynamic.h"
#include "ITransformable.h"

// These files are included here because nearly every entity uses some combination of them.
#include "Model.h"
#include "Sensor.h"
#include "SensorHandle.h"
#include "RigidBody3D.h"
#include "RigidBodyHandle.h"

namespace Nibble
{
	// Some of these forward declarations aren't used in this class, but are commonly used in derived entities.
	class Scene;
	class Shape3D;
	class Entity : public ITransformable, public IDynamic
	{
	private:

		// Many entities have a master physics body controlling overall movement. Since the body is itself an
		// attachment, a separate update function is needed to prevent a redundant transform set on the body. That
		// function is called during the World's main update loop for all bodies.
		friend class World;

		using Attachment = std::tuple<ITransformable*, glm::vec3, glm::quat>;

		int entityGroup;

		std::vector<Attachment> attachments;

		// Note that "update" in this function means setting position, orientation, and scale (rather than calling any
		// actual update functions).
		void UpdateAttachments();
		void SetTransformFromBody(const glm::vec3& position, const glm::quat& orientation);

	protected:

		using Json = nlohmann::json;

		explicit Entity(int entityGroup);

		// Note that scale is intentionally ignored. I'm applying a simplifying assumption throughout this project that
		// only 3D models need to scale. Stranger entities (such as soft-body entities) are handled separately.
		glm::vec3 position = glm::vec3(0);
		glm::quat orientation = glm::quat(glm::vec3(0));

		void Attach(ITransformable* item);
		void Attach(ITransformable* item, const glm::vec3& position);
		void Attach(ITransformable* item, const glm::vec3& position, const glm::quat& orientation);

		virtual bool OnCollision(CollisionTypes collisionType, void* target, const Manifold& manifold);
		virtual void OnSeparation(Entity& entity);

		// Using these functions simplifies body/sensor creation in derived entities.
		Sensor* CreateSensor(SensorHandle& handle, Shape3D& shape);

		// Using a variadic template to construct bodies won't work since it would require including the scene in this
		// class (causing a circular dependency).
		RigidBody3D* CreateBody(RigidBodyHandle& handle, BodyTypes bodyType, Shape3D& shape, float density = 1.0f,
			bool useCollisionCallback = false, bool useSeparationCallback = false);

	public:

		virtual ~Entity() = 0;

		// Making this variable public allows it to be set externally. This in turn means that Initialize can be kept
		// empty (so derived classes don't need to call down).
		Scene* scene = nullptr;

		const glm::vec3& GetPosition() const override;
		const glm::quat& GetOrientation() const override;

		int GetEntityGroup() const;

		template<class T>
		T* GetAttachment();
		
		// This flag allows entities to be safely removed from the scene following an update tick without causing weird
		// memory issues (or logical issues).
		bool markedForDestruction = false;

		// Passing a Json block allows entities to load relevant instance data (i.e. fields that can vary on an entity-
		// by-entity basis). Using a nullptr means that the object has no custom instance data to load (which should
		// rarely or never happen in practice since all entities probably have at least position).
		virtual void Initialize(Scene* scene, const Json* j);

		void SetPosition(const glm::vec3& position) override;
		void SetOrientation(const glm::quat& orientation) override;
		void SetTransform(const glm::vec3& position, const glm::quat& orientation) override;
		void Update(float dt) override;
	};

	template<class T>
	T* Entity::GetAttachment()
	{
		for (auto& a :  attachments)
		{
			T* item = dynamic_cast<T*>(std::get<0>(a));

			// This returns the first attachment of the given type.
			if (item != nullptr)
			{
				return item;
			}
		}

		return nullptr;
	}

	inline Entity::~Entity() = default;
}
