#pragma once
#include <vector>
#include "Timeline.h"

namespace Nibble
{
	class SkeletalAnimation
	{
	private:

		static SkeletalAnimation Load(const std::string& filename);

		std::vector<Timeline> timelines;

		float length;

	public:

		SkeletalAnimation(std::vector<Timeline> timelines, float length);
	};
}
