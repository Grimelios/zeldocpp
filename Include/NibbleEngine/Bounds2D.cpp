#include "Bounds2D.h"

namespace Nibble
{
	Bounds2D::Bounds2D() : Bounds2D(0, 0, 0, 0)
	{
	}

	Bounds2D::Bounds2D(const int width, const int height) : Bounds2D(0, 0, width, height)
	{
	}

	Bounds2D::Bounds2D(const int x, const int y, const int width, const int height) :
		x(x),
		y(y),
		width(width),
		height(height)
	{
	}

	Bounds2D::Bounds2D(const glm::ivec2& location, const int width, const int height) :
		Bounds2D(location.x, location.y, width, height)
	{
	}

	int Bounds2D::GetLeft() const
	{
		return x;
	}

	int Bounds2D::GetRight() const
	{
		return x + width - 1;
	}

	int Bounds2D::GetTop() const
	{
		return y;
	}

	int Bounds2D::GetBottom() const
	{
		return y + height - 1;
	}

	glm::ivec2 Bounds2D::GetLocation() const
	{
		return glm::ivec2(x, y);
	}

	glm::ivec2 Bounds2D::GetCenter() const
	{
		return glm::ivec2(x + width / 2, y + height / 2);
	}

	void Bounds2D::SetLocation(const glm::ivec2& location)
	{
		x = location.x;
		y = location.y;
	}

	void Bounds2D::SetCenter(const glm::ivec2& center)
	{
		x = center.x - width / 2;
		y = center.y - height / 2;
	}

	void Bounds2D::SetLeft(const int left)
	{
		x = left;
	}

	void Bounds2D::SetRight(const int right)
	{
		x = right - width + 1;
	}

	void Bounds2D::SetTop(const int top)
	{
		y = top;
	}

	void Bounds2D::SetBottom(const int bottom)
	{
		y = bottom - height + 1;
	}

	void Bounds2D::Expand(const int amount)
	{
		x -= amount;
		y -= amount;
		width += amount * 2;
		height += amount * 2;
	}

	bool Bounds2D::Contains(const glm::vec2& position) const
	{
		return position.x >= x && position.x <= GetRight() && position.y >= y && position.y <= GetBottom();
	}

	std::array<glm::ivec2, 4> Bounds2D::ComputeCorners() const
	{
		const int right = GetRight();
		const int bottom = GetBottom();

		return
		{
			glm::vec2(x, y),
			glm::vec2(right, y),
			glm::vec2(right, bottom),
			glm::vec2(x, bottom)
		};
	}
}
