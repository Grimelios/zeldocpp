#include "SensorHandle.h"
#include "Space.h"

namespace Nibble
{
	SensorHandle::~SensorHandle()
	{
		space->RemoveSensor(sensor);
	}
}
