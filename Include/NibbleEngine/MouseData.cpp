#include "MouseData.h"

namespace Nibble
{
	MouseData::MouseData(const glm::vec2& position, const glm::vec2& oldPosition, const ButtonArray& buttonArray) :

		position(position),
		oldPosition(oldPosition),
		buttonArray(buttonArray)
	{
	}

	glm::vec2 MouseData::GetPosition() const
	{
		return position;
	}

	glm::vec2 MouseData::GetOldPosition() const
	{
		return oldPosition;
	}

	InputStates MouseData::GetState(const int button) const
	{
		return buttonArray[button];
	}

	bool MouseData::AnyButtonPressed() const
	{
		for (InputStates state : buttonArray)
		{
			if (state == InputStates::PressedThisFrame)
			{
				return true;
			}
		}

		return false;
	}

	bool MouseData::Query(const int data, const InputStates state) const
	{
		return (buttonArray[data] & state) == state;
	}
}
