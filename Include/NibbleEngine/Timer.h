#pragma once
#include <functional>
#include <optional>
#include "IDynamic.h"

namespace Nibble
{
	class Timer : public IDynamic
	{
	private:

		using TickFunction = std::function<void(float)>;

	protected:

		bool completed = false;

		explicit Timer(int duration, float elapsed = 0);

	public:

		virtual ~Timer() = default;

		float duration;
		float elapsed;

		std::optional<TickFunction> tick = std::nullopt;

		bool paused = false;
		bool HasCompleted() const;
	};
}
