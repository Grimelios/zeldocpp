#include "PropertyCache.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"
#include "MapUtilities.h"

namespace Nibble
{
	PropertyCache::Cache PropertyCache::cache;

	const PropertyCache::Map& PropertyCache::Get(const std::string& filename)
	{
		Map* existingMap;

		if (MapUtilities::HasValue(cache, filename, existingMap))
		{
			return *existingMap;
		}

		const auto lines = FileUtilities::ReadAllLines(Paths::Properties + filename);

		Map map;

		for (const auto& line : lines)
		{
			if (line.empty())
			{
				continue;
			}

			const int index = StringUtilities::IndexOf(line, '=');

			const std::string key = line.substr(0, index - 1);
			const std::string value = line.substr(index + 2);

			MapUtilities::Add(map, key, value);
		}

		return MapUtilities::Add(cache, filename, map);
	}

	void PropertyCache::Clear(const std::string& key)
	{
	}
}
