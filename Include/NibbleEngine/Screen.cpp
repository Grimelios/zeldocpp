#include "Screen.h"

namespace Nibble
{
	void Screen::Update(const float dt)
	{
		for (auto& c : controls)
		{
			c->Update(dt);
		}
	}

	void Screen::Draw(SpriteBatch& sb)
	{
		for (auto& c : components)
		{
			c->Draw(sb);
		}

		for (auto& c : controls)
		{
			c->Draw(sb);
		}
	}
}
