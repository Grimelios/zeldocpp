#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	// See http://www.peroxide.dk/papers/collision/collision.pdf (Appendix B).
	class Plane
	{
	private:

		glm::vec3 normal;
		
		float equation;

	public:

		Plane(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2);

		const glm::vec3& GetNormal() const;
		
		float ComputeSignedDistance(const glm::vec3& p) const;
	};
}
