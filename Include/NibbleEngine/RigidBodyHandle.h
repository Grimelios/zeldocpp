#pragma once

namespace Nibble
{
	class World;
	class RigidBody3D;
	class RigidBodyHandle
	{
	public:

		~RigidBodyHandle();

		World* world = nullptr;
		RigidBody3D* body = nullptr;
	};
}
