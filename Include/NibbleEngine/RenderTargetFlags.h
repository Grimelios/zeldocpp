#pragma once

namespace Nibble
{
	enum class RenderTargetFlags
	{
		None = 0,
		Color = 1<<0,
		Depth = 1<<1,
		DepthStencil = 1<<2
	};

	inline int operator&(RenderTargetFlags f1, RenderTargetFlags f2)
	{
		return static_cast<int>(f1) & static_cast<int>(f2);
	}

	inline RenderTargetFlags operator|(RenderTargetFlags f1, RenderTargetFlags f2)
	{
		return static_cast<RenderTargetFlags>(static_cast<int>(f1) | static_cast<int>(f2));
	}
}
