#include "Texture2D.h"
#include <vector>
#include <lodepng.h>

namespace Nibble
{
	Texture2D Texture2D::Load(const std::string& filename)
	{
		GLuint textureId;

		std::vector<unsigned char> buffer;

		unsigned int width;
		unsigned int height;

		// The texture folder is included with the filename (since textures are required for fonts as well).
		lodepng::decode(buffer, width, height, filename);

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &buffer[0]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glGenerateMipmap(GL_TEXTURE_2D);

		return Texture2D(width, height, textureId);
	}

	Texture2D::Texture2D(const int width, const int height, const GLuint textureId) :
		QuadSource(width, height, textureId)
	{
	}
}
