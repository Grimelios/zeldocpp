#pragma once

namespace Nibble
{
	class AggregateData;
	class IControllable
	{
	public:

		virtual ~IControllable() = default;
		virtual void ProcessInput(const AggregateData& data, float dt) = 0;
	};
}
