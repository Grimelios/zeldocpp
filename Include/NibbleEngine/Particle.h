#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class Particle
	{
	public:

		glm::vec3 position;
		glm::vec3 velocity;
	};
}
