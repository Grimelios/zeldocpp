#include "ReloadHelper.h"
#include "KeyboardData.h"
#include "IReloadable.h"
#include "MessageSystem.h"

namespace Nibble
{
	std::vector<IReloadable*> ReloadHelper::items;

	void ReloadHelper::Initialize()
	{
		MessageSystem::Subscribe(MessageTypes::Keyboard, [](const std::any& data, const float dt)
		{
			ProcessKeyboard(std::any_cast<KeyboardData>(data));
		});
	}

	void ReloadHelper::Add(IReloadable* item)
	{
		items.push_back(item);
		item->Reload();
	}

	void ReloadHelper::ProcessKeyboard(const KeyboardData& data)
	{
		if (data.QueryPress(GLFW_KEY_R, KeyModifiers::Control))
		{
			for (IReloadable* target : items)
			{
				target->Reload();
			}
		}
	}
}
