#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class IScalable3D
	{
	public:

		virtual ~IScalable3D() = default;

		virtual const glm::vec3& GetScale() const = 0;
		virtual void SetScale(const glm::vec3& scale) = 0;
	};
}
