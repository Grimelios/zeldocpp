#include "Capsule.h"

namespace Nibble
{
	Capsule::Capsule() : Capsule(0.0f, 0.0f)
	{
	}

	Capsule::Capsule(const float radius, const float height) : Shape3D(ShapeTypes3D::Capsule),
		radius(radius),
		height(height)
	{
	}

	float Capsule::ComputeVolume() const
	{
		constexpr float pi = glm::pi<float>();

		return 4.0f / 3.0f * pi * radius * radius * radius + pi * radius * radius * height;
	}
}
