#include "Rectangle.h"

namespace Nibble
{
	Rectangle::Rectangle() : Rectangle(0, 0, 0, 0)
	{
	}

	Rectangle::Rectangle(const int width, const int height) : Rectangle(0, 0, width, height)
	{
	}

	Rectangle::Rectangle(const int x, const int y, const int width, const int height) :
		x(x),
		y(y),
		width(width),
		height(height)
	{
	}

	std::array<glm::ivec2, 4> Rectangle::ComputeCorners() const
	{
		const int right = x + width - 1;
		const int bottom = y + height - 1;

		return std::array<glm::ivec2, 4>
		{
			glm::ivec2(x, y),
			glm::ivec2(right, y),
			glm::ivec2(right, bottom),
			glm::ivec2(x, bottom)
		};
	}
}
