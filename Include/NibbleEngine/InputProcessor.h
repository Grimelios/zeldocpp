#pragma once
#include "IDynamic.h"
#include "KeyPress.h"
#include <vector>
#include <glad/glad.h>
#include <glfw3.h>
#include <array>
#include "KeyboardData.h"
#include "MouseData.h"
#include <glm/vec2.hpp>

namespace Nibble
{
	class InputProcessor : public IDynamic
	{
	private:

		std::array<InputStates, GLFW_KEY_LAST> keyArray = { };
		std::array<InputStates, GLFW_MOUSE_BUTTON_LAST> buttonArray = { };
		std::vector<KeyPress> keyPresses;

		glm::vec2 mousePosition = glm::vec2(0);
		glm::vec2 oldMousePosition = glm::vec2(0);

		// On the first frame of the game, the mouse's old position will be set to (0, 0), meaning that the first
		// computed mouse delta will be artificially large. This variable accounts for this by setting both positions
		// to be equal on the first frame. This technically shouldn't be necessary in the final game since the splash
		// screen exists, but it's more correct to keep it.
		bool firstFrame = true;

		KeyboardData CreateKeyboardData();
		MouseData CreateMouseData();

	public:

		InputProcessor();

		void OnKeyPress(int key, int mods);
		void OnKeyRelease(int key);
		void OnMouseButtonPress(int button);
		void OnMouseButtonRelease(int button);
		void OnMouseMove(float x, float y);
		void Update(float dt) override;
	};
}
