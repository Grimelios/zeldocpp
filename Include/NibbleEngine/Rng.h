#pragma once
#include <random>

namespace Nibble
{
	class Rng
	{
	private:

		static std::mt19937 generator;

	public:

		static int GetValue(int min, int max);
		static float GetValue(float min, float max);
	};
}
