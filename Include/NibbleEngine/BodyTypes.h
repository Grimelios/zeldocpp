#pragma once

namespace Nibble
{
	enum class BodyTypes
	{
		Dynamic,
		Kinematic,
		Static
	};
}
