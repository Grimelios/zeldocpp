#pragma once
#include <string>
#include <map>
#include "Rectangle.h"

namespace Nibble
{
	class Atlas
	{
	private:

		using SourceMap = std::map<std::string, Rectangle>;

		SourceMap map;

		// Since this constructor is private, all atlases should be retrieved using the ContentCache.
		explicit Atlas(SourceMap map);

	public:

		static Atlas Load(const std::string& filename);

		const Rectangle& operator[](const std::string& key) const;
	};
}
