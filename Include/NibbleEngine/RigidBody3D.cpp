#include "RigidBody3D.h"
#include "Shape3D.h"

namespace Nibble
{
	RigidBody3D::RigidBody3D(const BodyTypes bodyType, Entity* parent, Shape3D& shape, const float density) :
		shape(shape),
		bodyType(bodyType),
		parent(*parent),
		density(density)
	{
		RecomputeMass();
	}

	const glm::vec3& RigidBody3D::GetPosition() const
	{
		return shape.position;
	}

	const glm::quat& RigidBody3D::GetOrientation() const
	{
		return shape.orientation;
	}

	void RigidBody3D::ApplyForce(const glm::vec3& force)
	{
		netForce += force;
	}

	void RigidBody3D::ApplyForce(const glm::vec3& force, const glm::vec3& p)
	{
		const glm::vec3 f = force * (p - shape.position);
	}

	glm::vec3 RigidBody3D::ComputeVelocityAtPoint(const glm::vec3& p) const
	{
		return velocity + angularVelocity * (p - shape.position);
	}

	void RigidBody3D::SetPosition(const glm::vec3& position)
	{
		shape.position = position;
	}

	void RigidBody3D::SetOrientation(const glm::quat& orientation)
	{
		shape.orientation = orientation;
	}

	void RigidBody3D::SetTransform(const glm::vec3& position, const glm::quat& orientation)
	{
		shape.position = position;
		shape.orientation = orientation;
	}	

	void RigidBody3D::RecomputeMass()
	{
		mass = shape.ComputeVolume() * density;
		inverseMass = 1.0f / mass;
	}
}
