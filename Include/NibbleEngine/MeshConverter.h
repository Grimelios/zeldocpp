#pragma once
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include "Mesh.h"

namespace Nibble
{
	class MeshConverter
	{
	protected:

		using IndexType = unsigned short;
		using MeshVertex = std::tuple<IndexType, IndexType, IndexType>;
		using MeshPair = std::pair<Mesh, std::string>;

		// These functions are similar to the ones in GameFunctions, but different enough to warrant new versions.
		static glm::vec2 ParseVec2(const std::string& line);
		static glm::vec3 ParseVec3(const std::string& line);

	public:

		virtual ~MeshConverter() = default;

		// Actual meshes load textures as appropritate. For conversion purposes, though, only the texture's filename
		// is needed.
		virtual MeshPair ParseData(const std::string& path) = 0;
	};
}
