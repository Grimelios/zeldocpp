#include "MessageHandle.h"
#include "MessageSystem.h"

namespace Nibble
{
	MessageHandle::~MessageHandle()
	{
		MessageSystem::Unsubscribe(messageType, index);
	}
}
