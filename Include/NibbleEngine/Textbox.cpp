#include "Textbox.h"
#include "KeyboardData.h"
#include "InputRenderer.h"

namespace Nibble
{
	const std::array<char, 10> Textbox::NumericSpecials = { ')', '!', '@', '#', '$', '%', '^', '&', '*', '(' };

	Textbox::Textbox() : InputControl(InputControlTypes::Textbox)
	{
		valuePointer = &value;
	}

	void Textbox::ProcessKeyboard(const KeyboardData& data)
	{
		// This list (once processed in the loop below) will contain only key presses that aren't tied to control
		// actions (such as backspace or moving the cursor).
		std::vector<const KeyPress*> charPresses;

		// See the note below (above the GLFW_KEY_BACKSPACE case).
		bool backspaceOrDeleteUsed = false;

		const int length = static_cast<int>(value.GetCount());

		for (const KeyPress& keyPress : data.GetKeysPressedThisFrame())
		{
			switch (keyPress.GetKey())
			{
				// Submit (enter) is intentionally processed first. On a valid submit, all other keys pressed this
				// frame are ignored.
				case GLFW_KEY_ENTER:
				case GLFW_KEY_KP_ENTER:
				// See https://stackoverflow.com/questions/5136295/switch-transfer-of-control-bypasses-initialization-of-when-calling-a-function
				// for why these braces are required.
				{
					const std::string s = GetValueString();

					if (!onValidate.HasValue() || onValidate.Invoke(s))
					{
						onSubmit.Invoke(s);

						return;
					}

					break;
				}

				// Backspace and delete are processed next. For simplicity, if either backspace or delete (or both) are
				// pressed this frame, all cursor manipulation keys are ignored (such as arrow keys or home/end). Note
				// that in these cases, all new characters are still processed and inserted as usual (following the
				// backspace/delete).
				case GLFW_KEY_BACKSPACE:
					if (cursorPosition > 0)
					{
						Backspace();

						backspaceOrDeleteUsed = true;
					}

					break;

				case GLFW_KEY_DELETE:
					if (cursorPosition < length - 1)
					{
						Delete();

						backspaceOrDeleteUsed = true;
					}

					break;

				default: charPresses.push_back(&keyPress); break;
			}
		}
	}

	void Textbox::Backspace()
	{
		auto* previous = cursorNode->previous;

		value.Remove(cursorNode);
		cursorNode = previous;
		cursorPosition--;
	}

	void Textbox::Delete()
	{
		auto* next = cursorNode->next;

		value.Remove(cursorNode);
		cursorNode = next;
	}

	std::string Textbox::GetValueString() const
	{
		if (value.IsEmpty())
		{
			return "";
		}

		const int length = static_cast<int>(value.GetCount());

		std::string s;
		s.reserve(length);

		auto* node = value.GetHead();

		for (int i = 0; i < length; i++)
		{
			s.push_back(node->data);
			node = node->next;
		}

		return s;
	}

	std::optional<char> Textbox::GetCharacter(const int key, const bool shift)
	{
		// These conditional checks are ordered roughly based on how commonly I'd expect them to be used (e.g. I expect
		// most users to type regular A-Z characters more frequently than numbers).
		if (key >= GLFW_KEY_A && key <= GLFW_KEY_Z)
		{
			return key - GLFW_KEY_A + (shift ? 'A' : 'a');
		}

		if (key == GLFW_KEY_SPACE)
		{
			return ' ';
		}

		if (key >= GLFW_KEY_0 && key <= GLFW_KEY_9)
		{
			const int v = key - GLFW_KEY_0;

			return shift ? NumericSpecials[v] : '0' + v;
		}

		if (key >= GLFW_KEY_KP_0 && key <= GLFW_KEY_KP_9)
		{
			// By this point, num lock is guaranteed not pressed.
			return key - GLFW_KEY_KP_0 + '0';
		}

		switch (key)
		{
			case GLFW_KEY_COMMA: return shift ? '<' : ',';
			case GLFW_KEY_PERIOD: return shift ? '>' : '.';
			case GLFW_KEY_SLASH: return shift ? '?' : '/';
			case GLFW_KEY_SEMICOLON: return shift ? ':' : ';';
			case GLFW_KEY_APOSTROPHE: return shift ? '"' : '\'';
			case GLFW_KEY_LEFT_BRACKET: return shift ? '{' : '[';
			case GLFW_KEY_RIGHT_BRACKET: return shift ? '}' : ']';
			case GLFW_KEY_BACKSLASH: return shift ? '|' : '\\';
			case GLFW_KEY_MINUS: return shift ? '_' : '-';
			case GLFW_KEY_EQUAL: return shift ? '+' : '=';
			case GLFW_KEY_GRAVE_ACCENT: return shift ? '~' : '`';

			// As before, num lock is guaranteed to not be pressed here.
			case GLFW_KEY_KP_ADD: return '+';
			case GLFW_KEY_KP_SUBTRACT: return '-';
			case GLFW_KEY_KP_MULTIPLY: return '*';
			case GLFW_KEY_KP_DIVIDE: return '/';
			case GLFW_KEY_KP_DECIMAL: return '.';
		}

		return std::nullopt;
	}

	void Textbox::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Textbox::Draw(SpriteBatch& sb)
	{
		renderer->Draw(sb);
	}
}
