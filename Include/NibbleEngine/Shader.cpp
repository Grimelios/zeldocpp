#include "Shader.h"
#include "Paths.h"
#include <vector>
#include "FileUtilities.h"
#include "MapUtilities.h"
#include "BufferStore.h"
#include <glm/gtc/type_ptr.hpp>

namespace Nibble
{
	void Shader::LoadShader(const std::string& filename, const GLenum type, GLuint& shaderId)
	{
		shaderId = glCreateShader(type);

		// See https://stackoverflow.com/questions/22100408/what-is-the-meaning-of-the-parameters-to-glshadersource.
		std::string source = FileUtilities::ReadAllText(Paths::Shaders + filename);

		GLchar const* file = source.c_str();
		GLint length = static_cast<GLint>(source.size());

		glShaderSource(shaderId, 1, &file, &length);
		glCompileShader(shaderId);

		GLint status;

		glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

		if (status == GL_FALSE)
		{
			GLint logSize = 0;

			glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logSize);

			std::vector<GLchar> message = std::vector<GLchar>(logSize);

			glGetShaderInfoLog(shaderId, logSize, nullptr, &message[0]);
			glDeleteShader(shaderId);

			throw std::exception(&message[0]);
		}
	}

	Shader::~Shader()
	{
		glDeleteProgram(program);
		glDeleteVertexArrays(1, &vao);
	}

	GLuint Shader::GetProgram() const
	{
		return program;
	}

	unsigned Shader::GetStride() const
	{
		return stride;
	}

	bool Shader::IsBindingComplete() const
	{
		return bindingComplete;
	}

	void Shader::Attach(const ShaderTypes shaderType, const std::string& filename)
	{
		switch (shaderType)
		{
			// This assumes that file extension will be passed in (.vert, .geom, or .frag).
			case ShaderTypes::Vertex: LoadShader(filename, GL_VERTEX_SHADER, vShader); break;
			case ShaderTypes::TesselationControl: LoadShader(filename, GL_TESS_CONTROL_SHADER, tcShader); break;
			case ShaderTypes::TesselationEvaluation: LoadShader(filename, GL_TESS_EVALUATION_SHADER, teShader); break;
			case ShaderTypes::Geometry: LoadShader(filename, GL_GEOMETRY_SHADER, gShader); break;
			case ShaderTypes::Fragment: LoadShader(filename, GL_FRAGMENT_SHADER, fShader); break;
		}
	}

	void Shader::Use() const
	{
		glUseProgram(program);
	}

	void Shader::Apply() const
	{
		glUseProgram(program);
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	}

	void Shader::CreateProgram()
	{
		program = glCreateProgram();

		glAttachShader(program, vShader);

		if (gShader != 0)
		{
			glAttachShader(program, gShader);
		}

		glAttachShader(program, fShader);
		glLinkProgram(program);

		int status;

		glGetProgramiv(program, GL_LINK_STATUS, &status);

		if (status == GL_FALSE)
		{
			GLint logSize = 0;

			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);

			std::vector<GLchar> message = std::vector<GLchar>(logSize);

			glGetProgramInfoLog(program, logSize, nullptr, &message[0]);
			glDeleteProgram(program);
			glDeleteShader(vShader);
			glDeleteShader(tcShader);
			glDeleteShader(teShader);
			glDeleteShader(gShader);
			glDeleteShader(fShader);

			throw std::exception(&message[0]);
		}

		GetUniforms();

		// If an ID is zero, its value is silently ignored when trying to delete the shader.
		glDeleteShader(vShader);
		glDeleteShader(tcShader);
		glDeleteShader(teShader);
		glDeleteShader(gShader);
		glDeleteShader(fShader);

		// The vao is generated here, but binding is completed later.
		glGenVertexArrays(1, &vao);
	}

	void Shader::CompleteBinding(const BufferStore& store)
	{
		CompleteBinding(store.GetBufferId(), store.GetIndexBufferId());
	}

	void Shader::CompleteBinding(const GLuint buffer, const GLuint indexBuffer)
	{
		this->buffer = buffer;
		this->indexBuffer = indexBuffer;

		glBindVertexArray(vao);

		// Index buffer can be zero when array rendering is being used.
		if (indexBuffer != 0)
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
		}

		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		
		for (unsigned i = 0; i < attributes.size(); i++)
		{
			const auto& a = attributes[i];

			glVertexAttribPointer(i, a.count, a.type, a.normalized, stride,
				reinterpret_cast<const void*>(static_cast<intptr_t>(a.offset)));

			glEnableVertexAttribArray(i);
		}

		bindingComplete = true;
	}

	void Shader::GetUniforms()
	{
		GLint uniformCount;
		GLsizei length;
		GLint size;
		GLenum type;

		std::vector<GLchar> name(32);

		glGetProgramiv(program, GL_ACTIVE_UNIFORMS, &uniformCount);

		for (GLint i = 0; i < uniformCount; i++)
		{
			glGetActiveUniform(program, i, static_cast<GLsizei>(name.capacity()), &length, &size, &type, &name[0]);

			std::string sName;

			for (int j = 0; j < length; j++)
			{
				sName.push_back(name[j]);
			}

			GLint location = glGetUniformLocation(program, &sName[0]);

			MapUtilities::Add(uniforms, sName, location);
		}
	}

	void Shader::SetUniform(const std::string& name, const int value)
	{
		glUniform1i(uniforms.at(name), value);
	}

	void Shader::SetUniform(const std::string& name, const float value)
	{
		glUniform1f(uniforms.at(name), value);
	}

	void Shader::SetUniform(const std::string& name, const glm::vec2& value)
	{
		glUniform2f(uniforms.at(name), value.x, value.y);
	}

	void Shader::SetUniform(const std::string& name, const glm::ivec2& value)
	{
		glUniform2i(uniforms.at(name), value.x, value.y);
	}

	void Shader::SetUniform(const std::string& name, const glm::vec3& value)
	{
		glUniform3f(uniforms.at(name), value.x, value.y, value.z);
	}

	void Shader::SetUniform(const std::string& name, const glm::vec4& value)
	{
		glUniform4f(uniforms.at(name), value.x, value.y, value.z, value.w);
	}

	void Shader::SetUniform(const std::string& name, const glm::mat4& value)
	{
		glUniformMatrix4fv(uniforms.at(name), 1, GL_FALSE, value_ptr(value));
	}
}
