#pragma once
#include <glm/vec3.hpp>
#include "EventSingular.h"
#include "BodyTypes.h"
#include "ITransformable.h"
#include "CollisionTypes.h"
#include "PhysicsMaterial.h"

namespace Nibble
{
	class Face;
	class Entity;
	class Shape3D;
	class Manifold;
	class RigidBody3D : public ITransformable
	{
	private:

		friend class World;
		friend class MapPhysicsHelper;
		
		Shape3D& shape;
		BodyTypes bodyType;

		// Keeping a parent pointer allows parent transforms to stay synchronized with physics bodies. This guarantees
		// that entity transforms will be accurate during entity update functions.
		Entity& parent;

		glm::vec3 netForce = glm::vec3(0);

		float mass = 0;
		float inverseMass = 0;
		float density;

	public:

		RigidBody3D(BodyTypes bodyType, Entity* parent, Shape3D& shape, float density = 1.0f);

		glm::vec3 velocity = glm::vec3(0);
		glm::quat angularVelocity = glm::quat(glm::vec3(0));

		bool enabled = true;
		bool affectedByGravity = true;
		bool fixedRotation = false;

		// Angular damping simulates air resistance. It only applies in the air (possibly after a short period of time
		// has passed).
		float angularDamping = 0.0f;

		PhysicsMaterial material;

		// A body's tag is conceptually different from its parent entity references. Tags are useful to differentiate
		// multiple bodies attached to single entities.
		void* tag = nullptr;

		// By default, physics bodies collide with everything.
		unsigned int physicsGroup = 0;
		unsigned int collidesWith = 2147483647;

		EventSingular<bool, CollisionTypes, void*, const Manifold&> onCollision;
		EventSingular<void, Entity&> onSeparation;

		const glm::vec3& GetPosition() const override;
		const glm::quat& GetOrientation() const override;

		void ApplyForce(const glm::vec3& force);
		void ApplyForce(const glm::vec3& force, const glm::vec3& p);

		void SetPosition(const glm::vec3& position) override;
		void SetOrientation(const glm::quat& orientation) override;
		void SetTransform(const glm::vec3& position, const glm::quat& orientation) override;

		glm::vec3 ComputeVelocityAtPoint(const glm::vec3& p) const;

		// This function should be called if a rigid shape changes in size. Density is assumed constant.
		void RecomputeMass();
	};
}
