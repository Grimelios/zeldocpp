#include "InputHelper.h"
#include "IControllable.h"
#include "AggregateData.h"
#include "MessageSystem.h"
#include "MapUtilities.h"
#include "JsonUtilities.h"

namespace Nibble
{
	void InputHelper::Subscribe(IControllable* target, MessageHandle& handle)
	{
		MessageSystem::Subscribe(MessageTypes::Input, handle, [target](const std::any& data, const float dt)
		{
			target->ProcessInput(std::any_cast<AggregateData>(data), dt);
		});
	}

	InputHelper::BindMap InputHelper::ParseBinds(const std::string& filename)
	{
		const auto j = JsonUtilities::Load(filename);

		BindMap map;
		BindBlocks blocks = j;

		for (const auto& pair : blocks)
		{
			const std::vector<Json>& entries = pair.second;

			std::vector<InputBind> binds;
			binds.reserve(entries.size());

			for (const Json& entry : entries)
			{
				const InputTypes type = entry.at("InputType").get<InputTypes>();
				const int data = entry.at("Data").get<int>();

				binds.emplace_back(type, data);
			}

			MapUtilities::Add(map, pair.first, binds);
		}

		return map;
	}
}
