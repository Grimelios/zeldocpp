#pragma once
#include "Shader.h"
#include "Color.h"
#include <glm/vec3.hpp>
#include "DynamicBufferStore.h"

namespace Nibble
{
	class Camera;
	class PrimitiveRenderer3D
	{
	private:

		using IndexType = unsigned short;

		static constexpr IndexType RestartIndex = 65535;

		Shader defaultShader;
		DynamicBufferStore storage;
		GLenum mode = 0;

		void VerifyMode(GLenum mode);

		template<int S>
		void Buffer(const std::array<glm::vec3, S>& data, const Color& color);

	public:

		PrimitiveRenderer3D(int bufferCapacity, int indexCapacity);

		const Camera* camera = nullptr;

		void DrawLine(const glm::vec3& p0, const glm::vec3& p1, const Color& color);
		void DrawTriangle(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2, const Color& color);

		// Primitives can be rendered with custom shaders. If no shader is provided, the default 3D primitive shader
		// is used.
		void Flush(Shader* customShader = nullptr);
	};

	template<int S>
	void PrimitiveRenderer3D::Buffer(const std::array<glm::vec3, S>& data, const Color& color)
	{
		const float f = color.ToFloat();

		std::array<float, S * 4> dataArray = { };
		std::array<IndexType, S> indexArray = {};

		const int indexStart = static_cast<int>(storage.GetIndexBuffer().size());

		for (int i = 0; i < S; i++)
		{
			const glm::vec3& p = data[i];

			const int start = i * 4;

			dataArray[start] = p.x;
			dataArray[start + 1] = p.y;
			dataArray[start + 2] = p.z;
			dataArray[start + 3] = f;

			indexArray[i] = indexStart + i;
		}

		storage.Append(dataArray);
		storage.AppendIndex(indexArray);
	}
}
