#pragma once
#include <string>
#include <vector>

namespace Nibble::StringUtilities
{
	std::vector<std::string> Split(const std::string& s, char delimeter, bool removeEmpty = false);
	std::string RemoveExtension(const std::string& s);
	std::string RemovePath(const std::string& s);
	std::string GetExtension(const std::string& s);
	
	int IndexOf(const std::string& s, char c);
	int IndexOf(const std::string& s, char c, int start);
	int IndexOf(const std::string& s, const std::string& value);
	int IndexOf(const std::string& s, const std::string& value, int start);
	int LastIndexOf(const std::string& s, char c);

	bool StartsWith(const std::string& s, const std::string& start);
	bool EndsWith(const std::string& s, const std::string& end);
}
