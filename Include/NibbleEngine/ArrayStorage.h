#pragma once
#include <vector>
#include <glad/glad.h>

namespace Nibble
{
	class ArrayStorage
	{
	private:

		GLuint bufferId = 0;

		std::vector<float> buffer;

	public:

		explicit ArrayStorage(int capacity);

		GLuint GetBufferId() const;

		std::vector<float>& GetBuffer();

		void Apply(bool clearBuffer = true);
	};
}
