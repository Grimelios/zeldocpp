#pragma once
#include <vector>
#include <string>
#include <optional>
#include <filesystem>
#include "WriteModes.h"
#include "StringUtilities.h"

// I should be able to simply use std::filesystem based on the C++ 17 standard, but for whatever reason, it's not
// recognized.
namespace fs = std::experimental::filesystem;
namespace Nibble::FileUtilities
{
	std::string ReadAllText(const std::string& filename);
	std::string ToString(const std::experimental::filesystem::directory_entry& entry, bool fullPath);

	std::vector<std::string> ReadAllLines(const std::string& filename);
	std::vector<std::string> GetFiles(const std::string& directory, bool fullPath, bool recursive);
	std::vector<std::string> GetFiles(const std::string& directory, const std::string& extension, bool fullPath,
		bool recursive);

	void SkipLine(std::ifstream& stream);
	void SkipLines(std::ifstream& stream, int count);
	void Write(const std::string& filename, const std::string& value, WriteModes mode = WriteModes::Overwrite);

	template<int S>
	bool CheckExtension(const std::experimental::filesystem::directory_entry& entry,
		const std::array<std::string, S>& extensions, const bool fullPath, std::string& result)
	{
		const auto& p = entry.path();
		const auto& f = p.filename().generic_string();

		bool validExtension = false;

		for (const auto& s : extensions)
		{
			if (StringUtilities::EndsWith(f, s))
			{
				result = fullPath ? p.generic_string() : f;

				return true;
			}
		}

		return false;
	}

	template<int S>
	std::vector<std::string> GetFiles(const std::string& directory, const std::array<std::string, S>& extensions,
		const bool fullPath, const bool recursive)
	{
		std::vector<std::string> files;

		// See https://stackoverflow.com/questions/612097/how-can-i-get-the-list-of-files-in-a-directory-using-c-or-c/37494654#37494654.
		if (recursive)
		{
			for (const auto& entry : fs::recursive_directory_iterator(directory))
			{
				std::string result;

				if (CheckExtension(entry, extensions, fullPath, result))
				{
					files.push_back(result);
				}
			}
		}
		else
		{
			for (const auto& entry : fs::directory_iterator(directory))
			{
				std::string result;

				if (CheckExtension(entry, extensions, fullPath, result))
				{
					files.push_back(result);
				}
			}
		}

		return files;
	}
}
