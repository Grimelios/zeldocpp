#pragma once
#include "InputControl.h"
#include <memory>

namespace Nibble
{
	class Button : public InputControl
	{
	private:

		friend class InputRenderer<Button>;

		std::unique_ptr<InputRenderer<Button>> renderer;

	public:

		Button();

		template<class T>
		T* CreateRenderer();

		void Draw(SpriteBatch& sb) override;
		void Update(float dt) override;
	};

	template<class T>
	T* Button::CreateRenderer()
	{
		renderer = std::make_unique<T>(*this);

		return static_cast<T*>(renderer.get());
	}
}
