#include "RigidBodyHandle.h"
#include "World.h"

namespace Nibble
{
	RigidBodyHandle::~RigidBodyHandle()
	{
		world->RemoveBody(body);
	}
}
