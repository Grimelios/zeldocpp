#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class Manifold
	{
	private:

		glm::vec3 point;
		glm::vec3 normal;

		float depth;

	public:

		Manifold(const glm::vec3& point, const glm::vec3& normal, float depth);

		const glm::vec3& GetPoint() const;
		const glm::vec3& GetNormal() const;

		float GetDepth() const;
	};
}
