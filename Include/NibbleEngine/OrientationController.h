#pragma once
#include "GenericController.h"
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class IOrientable;
	class OrientationController : GenericController<glm::quat>
	{
	private:

		IOrientable* target;

	protected:

		void Lerp(float t) override;

	public:

		explicit OrientationController(IOrientable* target);
	};
}
