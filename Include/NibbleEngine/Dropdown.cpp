#include "Dropdown.h"
#include "InputRenderer.h"

namespace Nibble
{
	Dropdown::Dropdown() : InputControl(InputControlTypes::Dropdown)
	{
	}

	void Dropdown::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Dropdown::Draw(SpriteBatch& sb)
	{
		renderer->Draw(sb);
	}
}
