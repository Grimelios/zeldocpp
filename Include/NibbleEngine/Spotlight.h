#pragma once
#include "LightSource.h"
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class Spotlight : public LightSource
	{
	public:

		glm::quat orientation = glm::quat(glm::vec3(0));

		float spread = 0;
	};
}
