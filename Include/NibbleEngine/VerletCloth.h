#pragma once
#include "NibbleEngine/Grid.h"
#include "VerletPoint.h"

namespace Nibble
{
	template<int W, int H>
	class VerletCloth
	{
	private:

		Grid<VerletPoint, W, H> grid;

		float segmentLength;

	public:

		explicit VerletCloth(float segmentLength);
	};

	template<int W, int H>
	VerletCloth<W, H>::VerletCloth(const float segmentLength) : segmentLength()
	{
	}
}
