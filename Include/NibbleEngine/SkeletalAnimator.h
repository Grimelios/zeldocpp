#pragma once
#include "IDynamic.h"

namespace Nibble
{
	class SkeletalAnimator : public IDynamic
	{
	public:

		void Update(float dt) override;
	};
}
