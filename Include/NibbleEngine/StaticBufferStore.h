#pragma once
#include "BufferStore.h"
#include <vector>

namespace Nibble
{
	class StaticBufferStore : public BufferStore
	{
	private:

		// These values are in bytes.
		int bufferSize = 0;
		int indexBufferSize = 0;

	public:

		StaticBufferStore(int bufferCapacity, int indexCapacity);

		void Append(const std::vector<float>& data);
		void AppendIndex(const std::vector<IndexType>& data);
	};
}
