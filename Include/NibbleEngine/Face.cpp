#include "Face.h"
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	Face::Face(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2, const int flags) :
		points({ p0, p1, p2 }),
		normal(normalize(cross(p1 - p0, p2 - p0))),
		flags(flags)
	{
	}

	const std::array<glm::vec3, 3>& Face::GetPoints() const
	{
		return points;
	}

	const glm::vec3& Face::GetNormal() const
	{
		return normal;
	}

	int Face::GetFlags() const
	{
		return flags;
	}
}
