#pragma once
#include <glm/vec2.hpp>

namespace Nibble
{
	class IScalable2D
	{
	public:

		virtual ~IScalable2D() = default;

		virtual const glm::vec2& GetScale() const = 0;
		virtual void SetScale(const glm::vec2& scale) = 0;
	};
}
