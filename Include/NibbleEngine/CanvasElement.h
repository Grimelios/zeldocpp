#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"
#include <glm/vec2.hpp>
#include "Alignments.h"

namespace Nibble
{
	class CanvasElement : public IDynamic, public IRenderable2D
	{
	public:

		virtual ~CanvasElement() = default;

		Alignments anchor = Alignments::Center;

		glm::ivec2 offset = glm::ivec2(0);

		bool visible = true;

		// Note that this class intentionally doesn't implement IPositionable2D because the getter isn't needed (i.e.
		// it wouldn't be useful).
		virtual void SetPosition(const glm::ivec2& position) = 0;

		void Update(float dt) override;
	};
}
