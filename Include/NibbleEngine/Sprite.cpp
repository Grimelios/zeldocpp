#include "Sprite.h"
#include "GameFunctions.h"
#include "ContentCache.h"
#include "SpriteBatch.h"
#include "Texture2D.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Nibble
{
	Sprite::Sprite(const std::string& filename, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const QuadSource& source, const Alignments alignment) :
		Sprite(source, std::nullopt, alignment)
	{
	}

	Sprite::Sprite(const std::string& filename, const Rectangle& sourceRect, const Alignments alignment) :
		Sprite(ContentCache::GetTexture(filename), SourceRect(sourceRect), alignment)
	{
	}

	Sprite::Sprite(const QuadSource& source, const Rectangle& sourceRect, const Alignments alignment) :
		Sprite(source, SourceRect(sourceRect), alignment)
	{
	}

	Sprite::Sprite(const QuadSource& source, const SourceRect& sourceRect, const Alignments alignment) :
		Component2D(alignment),
		
		source(source),
		sourceRect(sourceRect)
	{
		RecomputeOrigin();
	}

	void Sprite::RecomputeOrigin()
	{
		origin = sourceRect == std::nullopt
			? GameFunctions::ComputeOrigin(source.GetWidth(), source.GetHeight(), alignment)
			: GameFunctions::ComputeOrigin(sourceRect->width, sourceRect->height, alignment);
	}

	const QuadSource& Sprite::GetSource() const
	{
		return source;
	}

	void Sprite::ScaleTo(const int width, const int height)
	{
		const float wF = static_cast<float>(width);
		const float wH = static_cast<float>(height);

		SetScale(glm::vec2(wF / source.GetWidth(), wH / source.GetHeight()));
	}

	void Sprite::SetSourceRect(const SourceRect& sourceRect)
	{
		this->sourceRect = sourceRect;

		RecomputeOrigin();

		sourceChanged = true;
		positionChanged = true;
	}

	void Sprite::Draw(SpriteBatch& sb)
	{
		if (positionChanged)
		{
			int width;
			int height;

			if (sourceRect.has_value())
			{
				width = sourceRect.value().width;
				height = sourceRect.value().height;
			}
			else
			{
				width = source.GetWidth();
				height = source.GetHeight();
			}

			glm::vec2 corners[4] =
			{
				glm::vec2(0),
				glm::vec2(0, height),
				glm::vec2(width, 0),
				glm::vec2(width, height)
			};

			for (glm::vec2& p : corners)
			{
				p = (p - origin) * scale;
			}

			if (rotation != 0)
			{
				const glm::mat4 rotationMatrix = rotate(glm::mat4(1), rotation, glm::vec3(0, 0, 1));

				for (glm::vec2& p : corners)
				{
					const glm::vec4 v = rotationMatrix * glm::vec4(p, 0, 1);

					p = glm::vec2(v.x, v.y);
				}
			}

			for (unsigned i = 0; i < 4; i++)
			{
				const glm::vec2 p = corners[i] + position;
				const int index = i * FloatsPerVertex;

				vertexData[index] = p.x;
				vertexData[index + 1] = p.y;
			}

			positionChanged = false;
		}

		if (sourceChanged)
		{
			std::array<glm::vec2, 4> sourceCoords = { };

			if (sourceRect.has_value())
			{
				const Rectangle& rect = sourceRect.value();

				const int right = rect.x + rect.width;
				const int bottom = rect.y + rect.height;

				sourceCoords =
				{
					glm::vec2(rect.x, rect.y),
					glm::vec2(rect.x, bottom),
					glm::vec2(right, rect.y),
					glm::vec2(right, bottom)
				};

				for (glm::vec2& v : sourceCoords)
				{
					v.x /= source.GetWidth();
					v.y /= source.GetHeight();
				}
			}
			else
			{
				sourceCoords =
				{
					glm::vec2(0),
					glm::vec2(0, 1),
					glm::vec2(1, 0),
					glm::vec2(1)
				};
			}

			if ((mods & SpriteModifiers::FlipVertical) > 0)
			{
				std::swap(sourceCoords[0], sourceCoords[1]);
				std::swap(sourceCoords[2], sourceCoords[3]);
			}

			if ((mods & SpriteModifiers::FlipHorizontal) > 0)
			{
				std::swap(sourceCoords[0], sourceCoords[2]);
				std::swap(sourceCoords[1], sourceCoords[3]);
			}

			for (unsigned i = 0; i < 4; i++)
			{
				const glm::vec2& v = sourceCoords[i];
				const int index = i * FloatsPerVertex + SourceIndex;

				vertexData[index] = v.x;
				vertexData[index + 1] = v.y;
			}
			
			sourceChanged = false;
		}

		if (colorChanged)
		{
			const float f = color.ToFloat();

			for (unsigned i = ColorIndex; i < vertexData.size(); i += FloatsPerVertex)
			{
				vertexData[i] = f;
			}

			colorChanged = false;
		}

		sb.SetMode(GL_TRIANGLE_STRIP);
		sb.BindTexture(source.GetTextureId());
		sb.Buffer(vertexData);
	}
}
