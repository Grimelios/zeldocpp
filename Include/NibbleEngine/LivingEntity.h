#pragma once
#include "Entity.h"
#include "ITargetable.h"

namespace Nibble
{
	class LivingEntity : public Entity, public ITargetable
	{
	protected:

		explicit LivingEntity(int entityType);

		int health = 0;
		int maxHealth = 0;

		bool onGround = false;

		// Note that OnHealthChange is always called before OnDeath (even when the entity is explicitly killed through
		// the Kill function).
		virtual void OnHealthChange(int oldHealth, int newHealth);
		virtual void OnDeath();

	public:

		virtual ~LivingEntity() = 0;

		void ApplyDamage(int damage) override;
		void Kill();
	};

	inline LivingEntity::~LivingEntity() = default;
}
