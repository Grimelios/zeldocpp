#pragma once
#include "MessageTypes.h"

namespace Nibble
{
	class MessageHandle
	{
	public:

		~MessageHandle();

		MessageTypes messageType = MessageTypes::Undefined;

		int index = -1;
	};
}
