#include "XmlUtilities.h"
#include "FileUtilities.h"
#include "Paths.h"

namespace Nibble
{
	void XmlUtilities::Parse(const std::string& filename, XDoc& doc)
	{
		const auto raw = FileUtilities::ReadAllText(Paths::Xml + filename);

		char* text = doc.allocate_string(&raw[0], raw.size());
		doc.parse<0>(text);
	}
}
