#include "CameraFixEvent.h"
#include "Scene.h"
#include "Camera.h"
#include "GameFunctions.h"

namespace Nibble
{
	CameraFixEvent::CameraFixEvent(const Json& j) :
		position(GameFunctions::ParseVec3(j.at("Position").get<std::string>())),
		orientation(GameFunctions::ParseQuat(j.at("Orientation").get<std::string>()))
	{
	}

	void CameraFixEvent::Execute(Scene& scene, const float elapsed)
	{
		Camera* camera = scene.camera;
		camera->mode = CameraModes::Fixed;
		camera->position = position;
		camera->orientation = orientation;
	}
}
