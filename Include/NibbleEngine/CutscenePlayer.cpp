#include "CutscenePlayer.h"
#include <nlohmann/json.hpp>
#include "JsonUtilities.h"
#include "CameraFixEvent.h"
#include "CameraPanEvent.h"

namespace Nibble
{
	CutscenePlayer::CutscenePlayer(Scene& scene) :
		scene(scene),
		eventTypeMap({
			{ "CameraFix", CutsceneEventTypes::CameraFix },
			{ "CameraFollow", CutsceneEventTypes::CameraFollow },
			{ "CameraPan", CutsceneEventTypes::CameraPan },
			{ "EntityAnimation", CutsceneEventTypes::EntityAnimation },
			{ "EntityMovement", CutsceneEventTypes::EntityMovement },
			{ "Sound", CutsceneEventTypes::Sound },
			{ "Speech", CutsceneEventTypes::Speech },
		})
	{
	}

	void CutscenePlayer::Load(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/709146/how-do-i-clear-the-stdqueue-efficiently. In theory, the queue
		// shouldn't need to be cleared if the previous cutscene fully finished. That said, clearing the queue anyway
		// ensures that new cutscenes will play properly if cutscenes stack accidentally.
		eventQueue = { };

		const std::map<std::string, Json> eventBlocks = JsonUtilities::Load("Cutscenes/" + filename);

		for (const auto& pair : eventBlocks)
		{
			auto event = CreateEvent(eventTypeMap.at(pair.first), pair.second);

			eventQueue.push(std::move(event));
		}
	}

	std::unique_ptr<CutsceneEvent> CutscenePlayer::CreateEvent(const CutsceneEventTypes eventType, const Json& j) const
	{
		switch (eventType)
		{
			case CutsceneEventTypes::CameraFix: return std::make_unique<CameraFixEvent>(j);
			case CutsceneEventTypes::CameraPan: return std::make_unique<CameraPanEvent>(j);
		}

		return nullptr;
	}

	void CutscenePlayer::Play()
	{
		accumulator = 0;

		// This manages calling events that are meant to trigger immediately on cutscene start.
		CutsceneEvent* event;

		while (!eventQueue.empty() && (event = eventQueue.front().get())->timestamp == 0.0f)
		{
			event->Execute(scene, 0.0f);
			eventQueue.pop();
		}
	}

	void CutscenePlayer::Update(const float dt)
	{
		accumulator += dt;

		CutsceneEvent* event;

		while (!eventQueue.empty() && (event = eventQueue.front().get())->timestamp >= accumulator)
		{
			event->Execute(scene, accumulator - event->timestamp);
			eventQueue.pop();
		}
	}
}
