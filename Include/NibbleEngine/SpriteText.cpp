#include "SpriteText.h"
#include "SpriteFont.h"
#include <utility>
#include "ContentCache.h"
#include "GameFunctions.h"
#include "SpriteBatch.h"

namespace Nibble
{
	SpriteText::SpriteText(const Alignments alignment) : Component2D(alignment),
		font(nullptr)
	{
	}

	SpriteText::SpriteText(const std::string& font, const Alignments alignment) :
		SpriteText(ContentCache::GetFont(font), alignment)
	{
	}

	SpriteText::SpriteText(const SpriteFont& font, const Alignments alignment) : Component2D(alignment),
		font(&font)
	{
	}

	SpriteText::SpriteText(const std::string& font, const std::string& value, const Alignments alignment) :
		SpriteText(ContentCache::GetFont(font), value, alignment)
	{
	}

	SpriteText::SpriteText(const SpriteFont& font, const std::string& value, const Alignments alignment) : Component2D(alignment),
		font(&font)
	{
		SetValue(value);
	}

	const std::optional<std::string>& SpriteText::GetValue() const
	{
		return value;
	}

	const SpriteFont& SpriteText::GetFont() const
	{
		return *font;
	}

	void SpriteText::Clear()
	{
		this->value = std::nullopt;

		buffer.clear();
	}

	void SpriteText::SetFont(const SpriteFont& font)
	{
		this->font = &font;

		if (value.has_value())
		{
			SetValue(value.value());
		}
	}

	void SpriteText::SetValue(const std::string& value)
	{
		if (value.empty())
		{
			Clear();
		}

		this->value = value;

		const glm::ivec2 dimensions = font->Measure(this->value.value());

		origin = GameFunctions::ComputeOrigin(dimensions.x, dimensions.y, alignment);
		renderCount = 0;

		for (char c : value)
		{
			// In the font data, spaces are exported with incorrect coordinates. As such, spaces in the string are used
			// to advance character positions, but not actually rendered.
			if (c != ' ')
			{
				renderCount++;
			}
		}

		buffer.clear();
		buffer.reserve(renderCount * FloatsPerQuad);

		std::fill_n(std::back_inserter(buffer), buffer.capacity(), 0.0f);

		positionChanged = true;
		sourceChanged = true;
		colorChanged = true;
	}

	void SpriteText::Draw(SpriteBatch& sb)
	{
		if (!value.has_value())
		{
			return;
		}

		const std::string& s = value.value();
		const SpriteFont::GlyphArray& glyphs = font->GetGlyphs();

		if (positionChanged)
		{
			unsigned index = 0;

			glm::vec2 local = position - origin;
			local.x = std::round(local.x);
			local.y = std::round(local.y);

			for (char c : s)
			{
				const GlyphData& data = glyphs[c].value();

				// Spaces advance the position, but aren't rendered.
				if (c != ' ')
				{
					const int width = data.GetWidth();
					const int height = data.GetHeight();

					const glm::ivec2& offset = data.GetOffset();
					const glm::vec2 p = local + glm::vec2(offset.x, offset.y);

					glm::vec2 corners[4] =
					{
						p,
						p + glm::vec2(0, height),
						p + glm::vec2(width, 0),
						p + glm::vec2(width, height)
					};

					for (const auto& corner : corners)
					{
						buffer[index] = corner.x;
						buffer[index + 1] = corner.y;

						index += FloatsPerVertex;
					}
				}

				local.x += data.GetAdvance();
			}

			positionChanged = false;
		}

		if (sourceChanged)
		{
			int index = SourceIndex;

			for (char c : s)
			{
				if (c == ' ')
				{
					continue;
				}

				for (const auto& p : glyphs[c].value().GetCoords())
				{
					buffer[index] = p.x;
					buffer[index + 1] = p.y;

					index += FloatsPerVertex;
				}
			}

			sourceChanged = false;
		}

		if (colorChanged)
		{
			const float f = color.ToFloat();

			for (unsigned i = ColorIndex; i < buffer.size(); i += FloatsPerVertex)
			{
				buffer[i] = f;
			}

			colorChanged = false;
		}

		sb.SetMode(GL_TRIANGLE_STRIP);
		sb.BindTexture(font->GetTexture().GetTextureId());
		sb.UseSpriteShader();

		for (unsigned i = 0; i < renderCount; i++)
		{
			sb.Buffer(&buffer[i * FloatsPerQuad], FloatsPerQuad);
		}
	}
}
