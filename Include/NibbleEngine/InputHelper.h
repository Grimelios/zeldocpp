#pragma once
#include <vector>
#include <map>
#include <string>
#include "InputBind.h"
#include <nlohmann/json.hpp>

namespace Nibble
{
	class IControllable;
	class MessageHandle;

	namespace InputHelper
	{
		using Json = nlohmann::json;
		using BindMap = std::map<std::string, std::vector<InputBind>>;
		using BindBlocks = std::map<std::string, std::vector<Json>>;

		void Subscribe(IControllable* target, MessageHandle& handle);

		BindMap ParseBinds(const std::string& filename);
	}
}
