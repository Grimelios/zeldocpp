#pragma once

namespace Nibble
{
	class IDynamic
	{
	public:

		virtual ~IDynamic() = default;
		virtual void Update(float dt) = 0;
	};
}
