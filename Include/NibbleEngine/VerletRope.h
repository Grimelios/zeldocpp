#pragma once
#include <array>
#include "VerletPoint.h"

namespace Nibble
{
	template<int S>
	class VerletRope
	{
	private:

		std::array<VerletPoint, S> points;

		float segmentLength;

	public:

		explicit VerletRope(float segmentLength);
	};

	template<int S>
	VerletRope<S>::VerletRope(const float segmentLength) : segmentLength(segmentLength)
	{
	}
}
