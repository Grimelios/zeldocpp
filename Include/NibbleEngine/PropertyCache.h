#pragma once
#include <map>
#include <string>

namespace Nibble
{
	class PropertyCache
	{
	private:

		using Map = std::map<std::string, std::string>;
		using Cache = std::map<std::string, Map>;

		static std::map<std::string, Map> cache;

	public:

		static const Map& Get(const std::string& filename);
		static void Clear(const std::string& key);
	};
}
