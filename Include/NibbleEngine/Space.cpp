#include "Space.h"
#include "Sensor.h"
#include "SensorHandle.h"
#include "Shape3D.h"

namespace Nibble
{
	Sensor* Space::CreateSensor(void* parent, const SensorTypes parentType, Shape3D& shape, SensorHandle& handle)
	{
		handle.sensor = sensorList.Add(parent, parentType, shape);;
		handle.space = this;

		return handle.sensor;
	}

	void Space::RemoveSensor(Sensor* sensor)
	{
		auto* node = sensor->contactList.GetHead();
		void* parent = sensor->GetParent();

		const SensorTypes parentType = sensor->GetParentType();

		while (node != nullptr)
		{
			node->data->onSeparate.Invoke(parent, parentType);
			node = node->next;
		}

		sensorList.Remove(sensor);
	}

	void Space::Step(const float dt) const
	{
		if (sensorList.GetCount() < 2)
		{
			return;
		}

		auto* node1 = sensorList.GetHead();

		while (node1 != nullptr)
		{
			Sensor& sensor1 = node1->data;

			if (!sensor1.IsEnabled())
			{
				node1 = node1->next;

				continue;
			}

			const auto& shape1 = sensor1.GetShape();

			// Loop through existing contacts.
			auto& contactList = sensor1.contactList;
			auto* contactNode = contactList.GetHead();
			void* parent1 = sensor1.GetParent();

			const SensorTypes parentType1 = sensor1.GetParentType();

			while (contactNode != nullptr)
			{
				Sensor* other = contactNode->data;

				if (!shape1.Intersects(other->GetShape()))
				{
					sensor1.onSeparate.Invoke(other->GetParent(), other->GetParentType());
					other->onSeparate.Invoke(parent1, parentType1);
					contactList.Remove(contactNode);
				}

				contactNode = contactNode->next;
			}

			// Loop through all sensors.
			auto* node2 = node1->next;

			while (node2 != nullptr)
			{
				Sensor& sensor2 = node2->data;

				if (contactList.Contains(&sensor2))
				{
					continue;
				}

				// Note that this intersection check is possibly pointless if the current sensor pair had already
				// separated this frame. This could be avoided by using std::pair to track sensors. For the time being,
				// that optimization doesn't seem necessary.
				if (shape1.Intersects(sensor2.GetShape()))
				{
					sensor1.onSense.Invoke(sensor2.GetParent(), sensor2.GetParentType());
					sensor2.onSense.Invoke(parent1, parentType1);
					contactList.Add(&sensor2);
					sensor2.contactList.Add(&sensor1);
				}

				node2 = node2->next;
			}

			node1 = node1->next;
		}
	}
}
