#include "FileUtilities.h"
#include <fstream>
#include <array>

namespace Nibble
{
	std::string FileUtilities::ReadAllText(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/195323/what-is-the-most-elegant-way-to-read-a-text-file-with-c/195350.
		std::ifstream stream(filename);

		if (!stream.good())
		{
			throw std::runtime_error("File [" + filename + "] not found.");
		}

		std::string value((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());

		stream.close();

		return value;
	}

	std::string FileUtilities::ToString(const std::experimental::filesystem::directory_entry& entry, const bool fullPath)
	{
		const auto& p = entry.path();

		return fullPath ? p.generic_string() : p.filename().generic_string();
	}

	std::vector<std::string> FileUtilities::ReadAllLines(const std::string& filename)
	{
		// See https://stackoverflow.com/questions/7868936/read-file-line-by-line/7868998.
		std::ifstream stream(filename);

		if (!stream.good())
		{
			throw std::runtime_error("File [" + filename + "] not found.");
		}

		std::string line;
		std::vector<std::string> lines;

		while (std::getline(stream, line))
		{
			lines.push_back(std::move(line));
		}

		stream.close();

		return lines;
	}

	std::vector<std::string> FileUtilities::GetFiles(const std::string& directory, const bool fullPath,
		const bool recursive)
	{
		std::vector<std::string> files;

		if (recursive)
		{
			for (const auto& entry : fs::recursive_directory_iterator(directory))
			{
				files.push_back(ToString(entry, fullPath));
			}
		}
		else
		{
			for (const auto& entry : fs::directory_iterator(directory))
			{
				files.push_back(ToString(entry, fullPath));
			}
		}

		return files;
	}

	std::vector<std::string> FileUtilities::GetFiles(const std::string& directory, const std::string& extension,
		const bool fullPath, const bool recursive)
	{
		const std::array<std::string, 1> a = { extension };

		return GetFiles(directory, a, fullPath, recursive);
	}

	void FileUtilities::SkipLine(std::ifstream& stream)
	{
		stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	void FileUtilities::SkipLines(std::ifstream& stream, const int count)
	{
		for (int i = 0; i < count; i++)
		{
			// See https://stackoverflow.com/questions/477408/ifstream-end-of-line-and-move-to-next-line.
			stream.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
	}

	void FileUtilities::Write(const std::string& filename, const std::string& value, const WriteModes mode)
	{
		// See https://stackoverflow.com/questions/15056406/c-fstream-overwrite-instead-of-append.
		std::ofstream stream(filename, mode == WriteModes::Append ? std::ios_base::app : std::ios_base::out);
		stream << value;
		stream.close();
	}
}
