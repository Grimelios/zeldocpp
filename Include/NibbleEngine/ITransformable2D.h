#pragma once
#include "IPositionable2D.h"
#include "IRotatable.h"

namespace Nibble
{
	class ITransformable2D : public IPositionable2D, public IRotatable
	{
	public:

		virtual ~ITransformable2D() = default;
		virtual void SetTransform(const glm::vec2& position, float rotation) = 0;
	};
}
