#pragma once
#include "InputStates.h"

namespace Nibble
{
	enum class InputTypes
	{
		Keyboard,
		Mouse,
		Xbox360,
		Xbone,
		DS4,
		Count = 5
	};

	class InputData
	{
	public:

		virtual ~InputData() = default;

		// This function is useful in a handful of places (like the Press Start indicator on the title screen). Note that "button"
		// also refers to keys on a keyboard.
		virtual bool AnyButtonPressed() const = 0;
		virtual bool Query(int data, InputStates state) const = 0;
	};
}
