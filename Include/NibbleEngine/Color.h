#pragma once
#include <cstddef>
#include <glm/vec4.hpp>

namespace Nibble
{
	class Color
	{
	private:

		static std::byte Lerp(std::byte start, std::byte end, float t);

	public:

		static const Color White;
		static const Color Black;
		static const Color Red;
		static const Color Green;
		static const Color Blue;

		static Color Lerp(const Color& start, const Color& end, float t);

		Color();
		explicit Color(int value);
		Color(int r, int g, int b);
		Color(int r, int g, int b, int a);
		Color(std::byte r, std::byte g, std::byte b, std::byte a);

		std::byte r;
		std::byte g;
		std::byte b;
		std::byte a;

		float ToFloat() const;

		glm::vec4 ToVec4() const;
	};
}
