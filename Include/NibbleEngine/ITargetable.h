#pragma once

namespace Nibble
{
	class ITargetable
	{
	public:

		virtual ~ITargetable() = default;
		virtual void ApplyDamage(int damage) = 0;
	};
}
