#pragma once
#include "IDynamic.h"
#include <queue>
#include "CutsceneEvent.h"
#include <nlohmann/json_fwd.hpp>
#include "CutsceneEventTypes.h"
#include "CutsceneControls.h"

namespace Nibble
{
	class Scene;
	class CutscenePlayer : public IDynamic
	{
	private:

		using Json = nlohmann::json;
		using EventTypeMap = std::map<std::string, CutsceneEventTypes>;

		Scene& scene;
		EventTypeMap eventTypeMap;
		CutsceneControls controls;

		std::queue<std::unique_ptr<CutsceneEvent>> eventQueue;

		float accumulator = 0;

		std::unique_ptr<CutsceneEvent> CreateEvent(CutsceneEventTypes eventType, const Json& j) const;

	public:

		explicit CutscenePlayer(Scene& scene);

		void Load(const std::string& filename);
		void Play();
		void Update(float dt) override;
	};
}
