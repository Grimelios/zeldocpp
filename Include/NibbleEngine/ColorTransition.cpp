#include "ColorTransition.h"
#include "IColorable.h"

namespace Nibble
{
	ColorTransition::ColorTransition(IColorable* target) : target(target)
	{
	}

	void ColorTransition::Lerp(const float t)
	{
		target->SetColor(Color::Lerp(start, end, t));
	}
}
