#include "Model.h"
#include "ContentCache.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Nibble
{
	Model::Model(const std::string& filename) : mesh(ContentCache::GetMesh(Paths::Models + filename))
	{
	}

	const Mesh& Model::GetMesh() const
	{
		return mesh;
	}

	const glm::mat4& Model::GetWorldMatrix()
	{
		if (transformChanged)
		{
			worldMatrix = translate(glm::mat4(1), position) * mat4_cast(orientation) * glm::scale(glm::mat4(1), scale);
			transformChanged = false;
		}

		return worldMatrix;
	}

	const glm::vec3& Model::GetPosition() const
	{
		return position;
	}

	const glm::quat& Model::GetOrientation() const
	{
		return orientation;
	}

	const glm::vec3& Model::GetScale() const
	{
		return scale;
	}

	void Model::SetPosition(const glm::vec3& position)
	{
		this->position = position;

		transformChanged = true;
	}

	void Model::SetOrientation(const glm::quat& orientation)
	{
		this->orientation = orientation;

		transformChanged = true;
	}

	void Model::SetScale(const glm::vec3& scale)
	{
		this->scale = scale;

		transformChanged = true;
	}

	void Model::SetTransform(const glm::vec3& position, const glm::quat& orientation)
	{
		this->position = position;
		this->orientation = orientation;

		transformChanged = true;
	}
}
