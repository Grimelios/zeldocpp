#pragma once
#include <nlohmann/json_fwd.hpp>

namespace Nibble
{
	class ControlClass
	{
	protected:

		using Json = nlohmann::json;

	public:

		virtual ~ControlClass() = default;
		virtual void Refresh(const Json& j) = 0;
	};
}
