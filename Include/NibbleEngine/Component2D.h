#pragma once
#include "IRenderable2D.h"
#include "IPositionable2D.h"
#include "Color.h"
#include "Alignments.h"
#include "IRotatable.h"
#include "SpriteModifiers.h"
#include "IColorable.h"
#include <glm/vec2.hpp>

namespace Nibble
{
	class Component2D : public IPositionable2D, public IRotatable, public IColorable, public IRenderable2D
	{
	protected:

		static const int FloatsPerVertex = 5;
		static const int FloatsPerQuad = FloatsPerVertex * 4;
		static const int SourceIndex = 2;
		static const int ColorIndex = 4;

		Alignments alignment;
		Color color = Color(255);

		float rotation = 0;

		SpriteModifiers mods = SpriteModifiers::None;

		glm::vec2 position = glm::vec2(0);
		glm::vec2 origin = glm::vec2(0);
		glm::vec2 scale = glm::vec2(1);

		bool positionChanged = true;
		bool sourceChanged = true;
		bool colorChanged = true;

		explicit Component2D(Alignments alignment);

	public:

		const glm::vec2& GetPosition() const override;
		const glm::vec2& GetScale() const;

		float GetRotation() override;

		const Color& GetColor() const override;

		SpriteModifiers& GetMods();

		void SetPosition(const glm::vec2& position) override;
		void SetRotation(float rotation) override;

		template<class... Args>
		void SetScale(const Args&&... args);
		void SetColor(const Color& color) override;
	};

	template<class... Args>
	void Component2D::SetScale(const Args&&... args)
	{
		scale = glm::vec2(args...);
		positionChanged = true;
	}
}
