#include "Manifold.h"

namespace Nibble
{
	Manifold::Manifold(const glm::vec3& point, const glm::vec3& normal, const float depth) :
		point(point),
		normal(normal),
		depth(depth)
	{
	}

	const glm::vec3& Manifold::GetPoint() const
	{
		return point;
	}

	const glm::vec3& Manifold::GetNormal() const
	{
		return normal;
	}

	float Manifold::GetDepth() const
	{
		return depth;
	}
}
