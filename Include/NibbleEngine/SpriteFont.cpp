#include "SpriteFont.h"
#include "Texture2D.h"
#include <glm/vec2.hpp>
#include "ContentCache.h"
#include <vector>
#include "StringUtilities.h"
#include <fstream>
#include "FileUtilities.h"

namespace Nibble
{
	SpriteFont SpriteFont::Load(const std::string& name)
	{
		const Texture2D& texture = ContentCache::GetTexture(name + "_0.png", Paths::Fonts);

		const int tWidth = texture.GetWidth();
		const int tHeight = texture.GetHeight();

		std::ifstream stream(Paths::Fonts + name + ".fnt");
		std::string line;
		std::getline(stream, line);

		// The third token on the first line is "size=[value]", but spaces in the font name must be accounted for.
		const int index1 = StringUtilities::IndexOf(line, "size") + 5;
		const int index2 = StringUtilities::IndexOf(line, ' ', index1);
		const int size = std::stoi(line.substr(index1, index2 - index1));

		FileUtilities::SkipLines(stream, 2);

		std::getline(stream, line);

		// The fourth line looks like "chars count=XX".
		const int charCount = std::stoi(line.substr(12));

		SpriteFont::GlyphArray dataArray;

		for (int i = 0; i < charCount; i++)
		{
			std::getline(stream, line);
			std::vector<std::string> tokens = StringUtilities::Split(line, ' ', true);

			const int id = ParseGlyphValue(tokens[1]);
			const int x = ParseGlyphValue(tokens[2]);
			const int y = ParseGlyphValue(tokens[3]);
			const int width = ParseGlyphValue(tokens[4]);
			const int height = ParseGlyphValue(tokens[5]);
			const int offsetX = ParseGlyphValue(tokens[6]);
			const int offsetY = ParseGlyphValue(tokens[7]);
			const int advance = ParseGlyphValue(tokens[8]);

			dataArray[id] = GlyphData(x, y, width, height, advance, tWidth, tHeight, glm::ivec2(offsetX, offsetY));
		}

		return SpriteFont(texture, size, dataArray);
	}

	int SpriteFont::ParseGlyphValue(const std::string& s)
	{
		const int index = StringUtilities::IndexOf(s, '=') + 1;

		return std::stoi(s.substr(index));
	}

	SpriteFont::SpriteFont(const Texture2D& texture, const int size, const GlyphArray& dataArray) :
		texture(texture),
		dataArray(dataArray),
		size(size)
	{
	}

	const SpriteFont::GlyphArray& SpriteFont::GetGlyphs() const
	{
		return dataArray;
	}

	const Texture2D& SpriteFont::GetTexture() const
	{
		return texture;
	}

	int SpriteFont::GetSize() const
	{
		return size;
	}

	glm::ivec2 SpriteFont::Measure(const std::string& value) const
	{
		if (value.empty())
		{
			return glm::ivec2(0, size);
		}

		int sumWidth = 0;
		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length - 1; i++)
		{
			sumWidth += dataArray[value[i]]->GetAdvance();
		}

		sumWidth += dataArray[value[length - 1]]->GetWidth();

		return glm::ivec2(sumWidth, size);
	}

	glm::ivec2 SpriteFont::MeasureLiteral(const std::string& value, glm::ivec2& offset) const
	{
		if (value.empty())
		{
			return glm::ivec2(0);
		}

		int sumWidth = 0;
		int top = INT_MAX;
		int bottom = 0;

		const int length = static_cast<int>(value.size());

		for (int i = 0; i < length; i++)
		{
			const GlyphData& data = dataArray[value[i]].value();
			
			const int x = data.GetOffset().x;
			const int y = data.GetOffset().y;
			const int advance = data.GetAdvance();

			if (i == 0)
			{
				offset.x = x;
				sumWidth += advance - x;
			}
			else if (i == length - 1)
			{
				sumWidth += x + data.GetWidth();
			}
			else
			{
				sumWidth += advance;
			}

			top = std::min(top, y);
			bottom = std::max(bottom, y + data.GetHeight());
		}

		offset.y = top;

		return glm::ivec2(sumWidth, bottom - top);
	}
}
