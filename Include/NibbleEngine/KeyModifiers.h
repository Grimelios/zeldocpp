#pragma once
#include <glad/glad.h>
#include <glfw3.h>

namespace Nibble
{
	// See http://www.glfw.org/docs/latest/group__mods.html.
	enum class KeyModifiers
	{
		Control = GLFW_MOD_CONTROL,
		Shift = GLFW_MOD_SHIFT,
		Alt = GLFW_MOD_ALT,
		None = 0
	};

	inline int operator&(KeyModifiers m1, KeyModifiers m2)
	{
		return static_cast<int>(m1) & static_cast<int>(m2);
	}

	inline KeyModifiers operator|(KeyModifiers m1, KeyModifiers m2)
	{
		return static_cast<KeyModifiers>(static_cast<int>(m1) | static_cast<int>(m2));
	}
}
