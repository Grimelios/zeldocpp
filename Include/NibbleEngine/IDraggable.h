#pragma once
#include "IClickable.h"

namespace Nibble
{
	class IDraggable : public IClickable
	{
	public:

		virtual ~IDraggable() = default;
		virtual glm::vec2 ConstrainDrag(const glm::vec2& mousePosition) = 0;
	};
}
