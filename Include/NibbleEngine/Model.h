#pragma once
#include <string>
#include <vector>
#include <glm/mat4x4.hpp>
#include "Mesh.h"
#include "ITransformable.h"
#include "IScalable3D.h"

namespace Nibble
{
	class Model : public ITransformable, public IScalable3D
	{
	private:

		// Note that MeshRenderer could be listed as a friend class, but that's intentionally not done to force a call
		// to GetWorldMatrix (which guarantees the matrix is properly recomputed before each usage).

		glm::vec3 position = glm::vec3(0);
		glm::vec3 scale = glm::vec3(1);
		glm::quat orientation = glm::quat(glm::vec3(0));
		glm::mat4 worldMatrix = glm::mat4(0);

		// It's assumed that each model will only contain one mesh.
		const Mesh& mesh;

		// By defaulting to true, transforms are guaranteed to be set correctly before first use.
		bool transformChanged = true;

	public:

		explicit Model(const std::string& filename);

		const Mesh& GetMesh() const;

		const glm::mat4& GetWorldMatrix();
		const glm::vec3& GetPosition() const override;
		const glm::quat& GetOrientation() const override;
		const glm::vec3& GetScale() const override;

		void SetPosition(const glm::vec3& position) override;
		void SetOrientation(const glm::quat& orientation) override;
		void SetScale(const glm::vec3& scale) override;
		void SetTransform(const glm::vec3& position, const glm::quat& orientation) override;
	};
}
