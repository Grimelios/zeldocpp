#include "RenderHandle.h"
#include "RenderContainer.h"

namespace Nibble
{
	RenderHandle::~RenderHandle()
	{
		container->instanceCount--;
	}
}
