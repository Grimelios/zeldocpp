#pragma once

namespace Nibble
{
	enum class CollisionTypes
	{
		Entity,

		// These three are associated with static map collisions.
		Face,
		Edge,
		Point
	};
}
