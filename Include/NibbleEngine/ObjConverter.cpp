#include "ObjConverter.h"
#include "FileUtilities.h"
#include <fstream>
#include "StringUtilities.h"
#include "VectorUtilities.h"

namespace Nibble
{
	MeshConverter::MeshPair ObjConverter::ParseData(const std::string& path)
	{
		std::vector<std::string> lines = FileUtilities::ReadAllLines(path + ".obj");

		// It's assumed that .obj files exported from Blender won't use object groups. They will use materials if
		// texturing is applied (materials will likely later be used for lighting parameters as well).
		const bool usesMaterial = lines[2][0] == 'm';

		int lineIndex = usesMaterial ? 3 : 2;

		// Parse positions.
		std::vector<glm::vec3> pointList;
		std::string& line = lines[lineIndex];

		do
		{
			pointList.push_back(ParseVec3(line));
			line = lines[++lineIndex];
		}
		// Texture lines are listed after vertices (starting with "vt"). However, not all meshes will have texturing
		// during development, so it's also possible to hit a normal line ("vn").
		while (line[1] == ' ');

		// Parse texture coordinates.
		const bool usesTexturing = line[1] == 't';

		std::vector<glm::vec2> sourceList;

		if (usesTexturing)
		{
			do
			{
				sourceList.push_back(ParseVec2(line));
				line = lines[++lineIndex];
			}
			while (line[1] == 't');
		}
		else
		{
			// If no texture is bound, a default grey texture is used instead (with every vertex sampling from the
			// top-left corner).
			sourceList.emplace_back(0.0f);
		}

		// Parse normals.
		std::vector<glm::vec3> normalList;

		do
		{
			normalList.push_back(ParseVec3(line));
			line = lines[++lineIndex];
		}
		while (line[0] == 'v');

		// The two lines following normals ("usemtl" and smoothing group) can be ignored.
		lineIndex += 2;

		// Parse vertices and indices.
		std::vector<MeshVertex> vertexList;
		std::vector<IndexType> indexList;

		do
		{
			line = lines[lineIndex++];

			// Each mesh is assumed to use only a single texture. If certain faces don't have texturing applied,
			// though, additional "usemtl" lines can be present. These lines are ignored.
			if (line[0] == 'u')
			{
				line = lines[lineIndex++];
			}

			// Each face line starts with 'f', then lists three vertices. Each vertex has its components delimeted
			// using '/'. Note that because faces are triangulated on export, there will always be exactly three 
			// vertices for each face line.
			const auto tokens = StringUtilities::Split(line, ' ');

			for (int i = 1; i <= 3; i++)
			{
				const auto subTokens = StringUtilities::Split(tokens[i], '/');

				// .obj files use 1-indexing rather than 0-indexing.
				const int pointIndex = std::stoi(subTokens[0]) - 1;
				const int sourceIndex = usesTexturing ? std::stoi(subTokens[1]) - 1 : 0;
				const int normalIndex = std::stoi(subTokens[2]) - 1;

				const MeshVertex vertex(pointIndex, sourceIndex, normalIndex);

				int index;

				if (VectorUtilities::Contains(vertexList, vertex, index))
				{
					indexList.push_back(index);
				}
				else
				{
					indexList.push_back(static_cast<IndexType>(vertexList.size()));
					vertexList.push_back(vertex);
				}
			}
		}
		while (lineIndex < static_cast<int>(lines.size()));

		// Parse texture.
		std::string texture = !usesTexturing ? "Untextured.png" : ParseTexture(path + ".mtl");

		return MeshPair(Mesh(pointList, sourceList, normalList, nullptr, vertexList, indexList), texture);
	}

	std::string ObjConverter::ParseTexture(const std::string& materialPath) const
	{
		std::ifstream stream(materialPath);

		// If a mesh doesn't have a material exported, it's assumed it doesn't use texturing. Similarly, if a material
		// WAS exported, it's assumed that texturing IS used.
		if (!stream.good())
		{
			throw std::exception("Material file not found.");
		}

		std::string line;

		do
		{
			std::getline(stream, line);
		}
		// This assumes that a valid texture line will be present in the material file.
		while (!StringUtilities::StartsWith(line, "map_Kd"));

		return StringUtilities::RemovePath(line);
	}
}
