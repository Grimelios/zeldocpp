#include "RenderTarget.h"
#include <exception>

namespace Nibble
{
	RenderTarget::RenderTarget(const int width, const int height, const RenderTargetFlags flags) :
		QuadSource(width, height)
	{
		Initialize(width, height, flags);
	}

	RenderTarget::~RenderTarget()
	{
		glDeleteTextures(1, &textureId);
		glDeleteFramebuffers(1, &frameBuffer);
		glDeleteRenderbuffers(1, &renderBuffer);
	}

	void RenderTarget::Initialize(const int width, const int height, const RenderTargetFlags flags)
	{
		// See http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/ and
		// https://learnopengl.com/Advanced-OpenGL/Framebuffers.
		glGenFramebuffers(1, &frameBuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

		colorEnabled = (flags & RenderTargetFlags::Color) > 0;
		depthEnabled = (flags & RenderTargetFlags::Depth) > 0;
		stencilEnabled = (flags & RenderTargetFlags::DepthStencil) > 0;

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);

		GLenum texFormat;
		GLenum texAttachment;
		GLenum texType;

		if (colorEnabled)
		{
			texFormat = GL_RGBA;
			texType = GL_UNSIGNED_BYTE;
			texAttachment = GL_COLOR_ATTACHMENT0;
		}
		// This assumes a depth-only buffer (no stencil).
		else
		{
			texFormat = GL_DEPTH_COMPONENT;
			texType = GL_FLOAT;
			texAttachment = GL_DEPTH_ATTACHMENT;
		}

		glTexImage2D(GL_TEXTURE_2D, 0, texFormat, width, height, 0, texFormat, texType, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, texAttachment, GL_TEXTURE_2D, textureId, 0);

		if (!colorEnabled)
		{
			// See https://learnopengl.com/Advanced-Lighting/Shadows/Shadow-Mapping.
			glDrawBuffer(GL_NONE);
			glReadBuffer(GL_NONE);
		}
		// If the stencil buffer is being used, depth will also be enabled.
		else if (depthEnabled)
		{
			const GLenum format = stencilEnabled ? GL_DEPTH24_STENCIL8 : GL_DEPTH_COMPONENT;
			const GLenum attachment = stencilEnabled ? GL_DEPTH_STENCIL_ATTACHMENT : GL_DEPTH_ATTACHMENT;

			glGenRenderbuffers(1, &renderBuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
			glRenderbufferStorage(GL_RENDERBUFFER, format, width, height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderBuffer);
			glBindRenderbuffer(GL_RENDERBUFFER, 0);
		}

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			throw std::exception("Error creating render target.");
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void RenderTarget::Apply(const bool clear) const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
		glViewport(0, 0, width, height);

		if (!clear)
		{
			return;
		}

		GLbitfield clearBits = 0;

		if (colorEnabled)
		{
			clearBits |= GL_COLOR_BUFFER_BIT;
		}

		if (depthEnabled)
		{
			clearBits |= GL_DEPTH_BUFFER_BIT;
		}

		if (stencilEnabled)
		{
			clearBits |= GL_STENCIL_BUFFER_BIT;
		}

		glClear(clearBits);
	}

	GLuint RenderTarget::GetFrameBuffer() const
	{
		return frameBuffer;
	}

	GLuint RenderTarget::GetRenderBuffer() const
	{
		return renderBuffer;
	}

	bool RenderTarget::IsColorEnabled() const
	{
		return colorEnabled;
	}

	bool RenderTarget::IsDepthEnabled() const
	{
		return depthEnabled;
	}

	bool RenderTarget::IsStencilEnabled() const
	{
		return stencilEnabled;
	}

	void RenderTarget::Use() const
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	}
}
