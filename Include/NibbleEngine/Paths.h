#pragma once
#include <string>

// TODO: Consider removing this class entirely (since most paths are only used once or twice).
namespace Nibble::Paths
{
	const std::string Content = "../Content/";
	const std::string Atlases = "../Content/Atlases/";
	const std::string Fonts = "../Content/Fonts/";
	const std::string Json = "../Content/Json/";
	const std::string Models = "../Content/Models/";
	const std::string Textures = "../Content/Textures/";
	const std::string Properties = "../Content/Properties/";
	const std::string Shaders = "../Content/Shaders/";
	const std::string Xml = "../Content/Xml/";
}
