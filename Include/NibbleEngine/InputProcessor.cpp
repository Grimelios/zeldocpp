#include "InputProcessor.h"
#include "AggregateData.h"
#include "KeyboardData.h"
#include "MessageSystem.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Resolution.h"

namespace Nibble
{
	InputProcessor::InputProcessor()
	{
		std::fill_n(&keyArray[0], GLFW_KEY_LAST, InputStates::Released);
		std::fill_n(&buttonArray[0], GLFW_MOUSE_BUTTON_LAST, InputStates::Released);
	}

	void InputProcessor::OnKeyPress(const int key, const int mods)
	{
		keyArray[key] = InputStates::PressedThisFrame;
		keyPresses.emplace_back(key, static_cast<KeyModifiers>(mods));
	}

	void InputProcessor::OnKeyRelease(const int key)
	{
		keyArray[key] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseButtonPress(const int button)
	{
		buttonArray[button] = InputStates::PressedThisFrame;
	}

	void InputProcessor::OnMouseButtonRelease(const int button)
	{
		buttonArray[button] = InputStates::ReleasedThisFrame;
	}

	void InputProcessor::OnMouseMove(const float x, const float y)
	{
		mousePosition.x = x;
		mousePosition.y = y;
	}

	void InputProcessor::Update(const float dt)
	{
		KeyboardData keyboardData = CreateKeyboardData();
		MouseData mouseData = CreateMouseData();

		AggregateData aggregateData;
		aggregateData.SetData(InputTypes::Keyboard, keyboardData);
		aggregateData.SetData(InputTypes::Mouse, mouseData);

		MessageSystem::Send(MessageTypes::Mouse, mouseData, dt);
		MessageSystem::Send(MessageTypes::Keyboard, keyboardData, dt);
		MessageSystem::Send(MessageTypes::Input, aggregateData, dt);

		keyPresses.clear();
	}

	KeyboardData InputProcessor::CreateKeyboardData()
	{
		std::vector<int> keysDown;
		std::vector<int> keysReleasedThisFrame;

		// Keys pressed this frame are handled in the key callback function.
		for (int key = 0; key < GLFW_KEY_LAST; key++)
		{
			switch (keyArray[key])
			{
				case InputStates::Held:
				case InputStates::PressedThisFrame:
					keysDown.push_back(key);

					break;

				case InputStates::ReleasedThisFrame:
					keysReleasedThisFrame.push_back(key);

					break;
			}
		}

		KeyboardData data(keysDown, keyPresses, keysReleasedThisFrame, keyArray);

		for (InputStates& state : keyArray)
		{
			switch (state)
			{
				case InputStates::PressedThisFrame: state = InputStates::Held; break;
				case InputStates::ReleasedThisFrame: state = InputStates::Released; break;
			}
		}

		return data;
	}

	MouseData InputProcessor::CreateMouseData()
	{
		if (firstFrame)
		{
			oldMousePosition = mousePosition;
			firstFrame = false;
		}

		MouseData data = MouseData(mousePosition, oldMousePosition, buttonArray);

		oldMousePosition = mousePosition;

		for (InputStates& state : buttonArray)
		{
			switch (state)
			{
				case InputStates::PressedThisFrame: state = InputStates::Held; break;
				case InputStates::ReleasedThisFrame: state = InputStates::Released; break;
			}
		}

		return data;
	}
}
