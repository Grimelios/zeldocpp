#include "Shape2D.h"

namespace Nibble
{
	Shape2D::Shape2D(const ShapeTypes2D shapeType) : shapeType(shapeType)
	{
	}

	ShapeTypes2D Shape2D::GetShapeType() const
	{
		return shapeType;
	}
}
