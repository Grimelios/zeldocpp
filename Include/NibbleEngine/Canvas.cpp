#include "Canvas.h"
#include "SpriteBatch.h"
#include "Resolution.h"
#include "MessageSystem.h"
#include <glm/gtc/matrix_transform.hpp>
#include <nlohmann/json.hpp>

namespace Nibble
{
	Canvas::Canvas()
	{
		MessageSystem::Subscribe(MessageTypes::Resize, handle, [this](const std::any& data, const float dt)
		{
			OnResize();
		});
	}

	void Canvas::OnResize()
	{
		for (auto& e : elements)
		{
			PlaceElement(e);
		}
	}

	void Canvas::PlaceElement(std::unique_ptr<CanvasElement>& e)
	{
		const int w = Resolution::WindowWidth;
		const int h = Resolution::WindowHeight;

		const int anchor = static_cast<int>(e->anchor);

		const bool left = (anchor & static_cast<int>(Alignments::Left)) > 0;
		const bool right = (anchor & static_cast<int>(Alignments::Right)) > 0;
		const bool top = (anchor & static_cast<int>(Alignments::Top)) > 0;
		const bool bottom = (anchor & static_cast<int>(Alignments::Bottom)) > 0;

		const glm::ivec2& offset = e->offset;

		const int x = left ? offset.x : (right ? w - offset.x : w / 2 + offset.x);
		const int y = top ? offset.y : (bottom ? h - offset.y : h / 2 + offset.y);

		e->SetPosition(glm::ivec2(x, y));
	}

	void Canvas::Update(const float dt)
	{
		for (auto& e : elements)
		{
			if (e->visible)
			{
				e->Update(dt);
			}
		}
	}

	void Canvas::Draw(SpriteBatch& sb)
	{
		sb.SetMode(GL_TRIANGLE_STRIP);

		for (auto& e : elements)
		{
			if (e->visible)
			{
				e->Draw(sb);
			}
		}
	}
}
