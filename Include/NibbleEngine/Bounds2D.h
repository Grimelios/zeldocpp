#pragma once
#include <glm/vec2.hpp>
#include <array>

namespace Nibble
{
	class Bounds2D
	{
	public:

		Bounds2D();
		Bounds2D(int width, int height);
		Bounds2D(int x, int y, int width, int height);
		Bounds2D(const glm::ivec2& location, int width, int height);

		int x;
		int y;
		int width;
		int height;

		int GetLeft() const;
		int GetRight() const;
		int GetTop() const;
		int GetBottom() const;

		glm::ivec2 GetLocation() const;
		glm::ivec2 GetCenter() const;

		void SetLocation(const glm::ivec2& location);
		void SetCenter(const glm::ivec2& center);
		void SetLeft(int left);
		void SetRight(int right);
		void SetTop(int top);
		void SetBottom(int bottom);
		void Expand(int amount);

		bool Contains(const glm::vec2& position) const;

		std::array<glm::ivec2, 4> ComputeCorners() const;
	};
}
