#include "CameraPanEvent.h"
#include "Scene.h"
#include "Camera.h"

namespace Nibble
{
	CameraPanEvent::CameraPanEvent(const Json& j)
	{
	}

	void CameraPanEvent::Execute(Scene& scene, float elapsed)
	{
		Camera* camera = scene.camera;
		camera->mode = CameraModes::Pan;
	}
}
