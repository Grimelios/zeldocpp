#pragma once
#include "ShapeTypes2D.h"
#include <glm/vec2.hpp>
#include "IPositionable2D.h"
#include "IRotatable.h"

namespace Nibble
{
	class Shape2D : public IPositionable2D, public IRotatable
	{
	private:

		ShapeTypes2D shapeType;

	protected:

		explicit Shape2D(ShapeTypes2D shapeType);

	public:

		virtual ~Shape2D() = default;

		ShapeTypes2D GetShapeType() const;
	};
}
