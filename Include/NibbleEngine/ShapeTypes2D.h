#pragma once

namespace Nibble
{
	enum class ShapeTypes2D
	{
		Circle,
		Square,
		Rectangle,
		Line
	};
}
