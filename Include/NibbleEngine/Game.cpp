#include "Game.h"
#include "MessageSystem.h"
#include <glm/vec2.hpp>
#include "Resolution.h"
#include "GLConstants.h"

namespace Nibble
{
	Game::Game(const std::string& title, const int windowWidth, const int windowHeight)
	{
		glfwInit();
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		window = glfwCreateWindow(windowWidth, windowHeight, title.c_str(), nullptr, nullptr);

		if (window == nullptr)
		{
			glfwTerminate();

			return;
		}

		glfwMakeContextCurrent(window);
		glfwSetWindowUserPointer(window, this);

		if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
		{
			glfwTerminate();

			return;
		}

		Resolution::WindowWidth = windowWidth;
		Resolution::WindowHeight = windowHeight;

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPrimitiveRestartIndex(GLConstants::RestartIndex);
		glViewport(0, 0, windowWidth, windowHeight);

		RegisterCallbacks();

		// This assumes that when the game is exited (through a menu, for example), it's safe to finish out the current frame before
		// closing the window.
		MessageSystem::Subscribe(MessageTypes::Exit, handle, [this](const std::any& data, const float dt)
		{
			glfwSetWindowShouldClose(window, true);
		});

		// Resetting the index prevents an error when exiting the game.
		handle.index = -1;
	}

	void Game::RegisterCallbacks() const
	{
		glfwSetKeyCallback(window, KeyCallback);
		glfwSetCharCallback(window, CharCallback);
		glfwSetCursorPosCallback(window, CursorCallback);
		glfwSetMouseButtonCallback(window, ButtonCallback);
		glfwSetFramebufferSizeCallback(window, ResizeCallback);
	}

	void Game::KeyCallback(GLFWwindow* window, const int key, const int scancode, const int action, const int mods)
	{
		// Key can be negative in some cases (such as Alt + Printscreen).
		if (key == -1)
		{
			return;
		}

		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();

		switch (action)
		{
			case GLFW_PRESS:
				if (key == GLFW_KEY_ESCAPE)
				{
					glfwTerminate();
					exit(0);
				}
				
				processor.OnKeyPress(key, mods); break;

			case GLFW_RELEASE: processor.OnKeyRelease(key); break;
		}
	}

	void Game::CharCallback(GLFWwindow* window, const unsigned int codepoint)
	{
	}

	void Game::CursorCallback(GLFWwindow* window, const double x, const double y)
	{
		const float mX = static_cast<float>(x);
		const float mY = static_cast<float>(y);

		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();
		processor.OnMouseMove(mX, mY);
	}

	void Game::ButtonCallback(GLFWwindow* window, const int button, const int action, const int mods)
	{
		InputProcessor& processor = static_cast<Game*>(glfwGetWindowUserPointer(window))->GetInputProcessor();

		switch (action)
		{
			case GLFW_PRESS: processor.OnMouseButtonPress(button); break;
			case GLFW_RELEASE: processor.OnMouseButtonRelease(button); break;
		}
	}

	void Game::ResizeCallback(GLFWwindow* window, const int width, const int height)
	{
		Resolution::WindowWidth = width;
		Resolution::WindowHeight = height;

		MessageSystem::Send(MessageTypes::Resize, nullptr);
	}

	InputProcessor& Game::GetInputProcessor()
	{
		return inputProcessor;
	}

	void Game::Run()
	{
		while (!glfwWindowShouldClose(window))
		{
			const float time = static_cast<float>(glfwGetTime());
			const float target = 1.0f / TargetFramerate;

			// Using an accumulator helps limit FPS to the desired target.
			accumulator += time - previousTime;
			previousTime = time;

			const bool shouldUpdate = accumulator >= target;

			if (shouldUpdate)
			{
				glfwPollEvents();

				while (accumulator >= target)
				{
					Update(target);

					accumulator -= target;
				}

				Draw();

				glfwSwapBuffers(window);
			}
		}

		glfwTerminate();
	}
}
