#include "ClickableSet.h"
#include "IClickable.h"
#include "MouseData.h"

namespace Nibble
{
	void ClickableSet::ProcessMouse(const MouseData& data)
	{
		const auto& mousePosition = data.GetPosition();

		// Unhover is intentionally checked first so that everything will work correctly if the mouse instantly moves
		// from one item to another (in a single frame).
		if (hoveredItem != nullptr && !hoveredItem->Contains(mousePosition))
		{
			hoveredItem->OnUnhover();
			hoveredItem = nullptr;
		}

		// The ordering of these first two conditions is intentional. If the mouse releases and moves off an item on
		// the same frame, OnUnhover will be called before OnRelease (although it's not really the responsibility of
		// this class to properly manage an item's internal state).
		if (clickedItem != nullptr && data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::ReleasedThisFrame))
		{
			// Note that OnRelease is called on the previously-clicked item even if the mouse is no longer hovering
			// over that item.
			clickedItem->OnRelease();
			clickedItem = nullptr;
		}

		for (auto* item : items)
		{
			if (item != hoveredItem && item->Contains(mousePosition))
			{
				hoveredItem = item;
				hoveredItem->OnHover(mousePosition);

				// This assumes that no clickable items overlap. If they do, the earlier item in the list is hovered.
				break;
			}
		}

		// It's possible for the mouse to instantly move to a new position and click on the same frame.
		if (hoveredItem != nullptr && data.Query(GLFW_MOUSE_BUTTON_LEFT, InputStates::PressedThisFrame))
		{
			clickedItem = hoveredItem;
			clickedItem->OnClick(mousePosition);
		}
	}
}
