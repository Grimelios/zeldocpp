#pragma once
#include "DoublyLinkedListNode.h"

namespace Nibble
{
	template<class T>
	class DoublyLinkedList
	{
	private:

		DoublyLinkedListNode<T>* head = nullptr;
		DoublyLinkedListNode<T>* tail = nullptr;

		int count = 0;

		T* Add(DoublyLinkedListNode<T>* node);

	public:

		~DoublyLinkedList();

		template<class... Args>
		T* Add(Args&&... args);
		T* Add(const T& data);

		DoublyLinkedListNode<T>* GetHead() const;
		DoublyLinkedListNode<T>* GetTail() const;

		void Remove(const T* data);
		void Remove(const T& data);
		void Remove(const DoublyLinkedListNode<T>* node);

		int GetCount() const;

		bool IsEmpty() const;
		bool Contains(const T& item);
	};

	template<class T>
	DoublyLinkedList<T>::~DoublyLinkedList()
	{
		while (head != nullptr)
		{
			DoublyLinkedListNode<T>* temp = head;
			head = head->next;

			delete temp;
		}
	}

	template<class T>
	template<class... Args>
	T* DoublyLinkedList<T>::Add(Args&&... args)
	{
		return Add(new DoublyLinkedListNode<T>(args...));
	}

	template<class T>
	T* DoublyLinkedList<T>::Add(const T& data)
	{
		return Add(new DoublyLinkedListNode<T>(data));
	}

	template<class T>
	T* DoublyLinkedList<T>::Add(DoublyLinkedListNode<T>* node)
	{
		if (count == 0)
		{
			head = node;
			tail = node;
		}
		else
		{
			node->previous = tail;
			tail->next = node;
			tail = node;
		}

		count++;

		return &node->data;
	}

	template<class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::GetHead() const
	{
		return head;
	}

	template<class T>
	DoublyLinkedListNode<T>* DoublyLinkedList<T>::GetTail() const
	{
		return tail;
	}

	template<class T>
	void DoublyLinkedList<T>::Remove(const T* data)
	{
		for (DoublyLinkedListNode<T>* node = head; node != nullptr; node = node->next)
		{
			if (&node->data == data)
			{
				Remove(node);

				return;
			}
		}
	}

	template<class T>
	void DoublyLinkedList<T>::Remove(const T& data)
	{
		for (DoublyLinkedListNode<T>* node = head; node != nullptr; node = node->next)
		{
			if (node->data == data)
			{
				Remove(node);

				return;
			}
		}
	}

	template<class T>
	void DoublyLinkedList<T>::Remove(const DoublyLinkedListNode<T>* node)
	{
		if (count == 1)
		{
			delete head;

			head = nullptr;
			tail = nullptr;
		}
		else if (node == head)
		{
			head->next->previous = nullptr;
			head = head->next;

			delete node;
		}
		else if (node == tail)
		{
			tail->previous->next = nullptr;
			tail = tail->previous;

			delete node;
		}
		else
		{
			node->previous->next = node->next;
			node->next->previous = node->previous;

			delete node;
		}

		count--;
	}

	template<class T>
	int DoublyLinkedList<T>::GetCount() const
	{
		return count;
	}

	template<class T>
	bool DoublyLinkedList<T>::IsEmpty() const
	{
		return count == 0;
	}

	template<class T>
	bool DoublyLinkedList<T>::Contains(const T& item)
	{
		if (count == 0)
		{
			return false;
		}

		auto* node = head;

		do
		{
			if (&node->data == &item)
			{
				return true;
			}
		}
		while ((node = node->next) != nullptr);

		return false;
	}
}
