#include "KeyPress.h"

namespace Nibble
{
	KeyPress::KeyPress(const int key, const KeyModifiers mods) :
		key(key),
		mods(mods)
	{

	}

	int KeyPress::GetKey() const
	{
		return key;
	}

	KeyModifiers KeyPress::GetMods() const
	{
		return mods;
	}
}
