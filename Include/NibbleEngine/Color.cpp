#include "Color.h"
#include <cstring>

namespace Nibble
{
	const Color Color::White;
	const Color Color::Black(0);
	const Color Color::Red(255, 0, 0);
	const Color Color::Green(0, 255, 0);
	const Color Color::Blue(0, 0, 255);

	Color Color::Lerp(const Color& start, const Color& end, const float t)
	{
		const std::byte r = Lerp(start.r, end.r, t);
		const std::byte g = Lerp(start.g, end.g, t);
		const std::byte b = Lerp(start.b, end.b, t);
		const std::byte a = Lerp(start.a, end.a, t);

		return Color(r, g, b, a);
	}

	std::byte Color::Lerp(const std::byte start, const std::byte end, const float t)
	{
		const int sInt = static_cast<int>(start);
		const int eInt = static_cast<int>(end);

		return static_cast<std::byte>(sInt + static_cast<int>(static_cast<float>(eInt - sInt) * t));
	}

	Color::Color() : Color(255, 255, 255, 255)
	{
	}

	Color::Color(const int value) : Color(value, value, value, 255)
	{
	}

	Color::Color(const int r, const int g, const int b) : Color(r, g, b, 255)
	{
	}

	Color::Color(const int r, const int g, const int b, const int a) :
		Color(static_cast<std::byte>(r), static_cast<std::byte>(g), static_cast<std::byte>(b), static_cast<std::byte>(a))
	{
	}

	Color::Color(const std::byte r, const std::byte g, const std::byte b, const std::byte a) :
		r(r),
		g(g),
		b(b),
		a(a)
	{
	}

	float Color::ToFloat() const
	{
		// See https://stackoverflow.com/questions/3991478/building-a-32-bit-float-out-of-its-4-composite-bytes.
		float f;

		std::byte bytes[] = { r, g, b, a };

		memcpy(&f, bytes, 4);

		return f;
	}

	glm::vec4 Color::ToVec4() const
	{
		const float rF = static_cast<float>(r);
		const float gF = static_cast<float>(g);
		const float bF = static_cast<float>(b);
		const float aF = static_cast<float>(a);

		return glm::vec4(rF, gF, bF, aF) / 255.0f;
	}
}
