#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"

namespace Nibble
{
	// Note that although this class is called "renderer", it also handles visual updates to the control (e.g. the
	// blinking cursor of a textbox is managed withing a textbox renderer).
	template<class T>
	class InputRenderer : public IDynamic, public IRenderable2D
	{
	protected:

		T& parent;

	public:

		explicit InputRenderer(T& parent);

		void Update(float dt) override;
	};

	template<class T>
	InputRenderer<T>::InputRenderer(T& parent) : parent(parent)
	{
	}

	template<class T>
	void InputRenderer<T>::Update(float dt)
	{
	}
}
