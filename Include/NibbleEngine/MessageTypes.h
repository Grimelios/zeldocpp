#pragma once

namespace Nibble
{
	enum class MessageTypes
	{
		Keyboard,
		Mouse,
		Input,
		Exit,
		Resize,
		Count = 5,
		Undefined
	};
}
