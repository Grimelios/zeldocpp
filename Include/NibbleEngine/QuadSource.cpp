#include "QuadSource.h"

namespace Nibble
{
	QuadSource::QuadSource() : QuadSource(0, 0, 0)
	{
	}

	QuadSource::QuadSource(const int width, const int height) : QuadSource(width, height, 0)
	{
	}

	QuadSource::QuadSource(const int width, const int height, const GLuint textureId) :
		width(width),
		height(height),
		textureId(textureId)
	{
	}

	int QuadSource::GetWidth() const
	{
		return width;
	}

	int QuadSource::GetHeight() const
	{
		return height;
	}

	GLuint QuadSource::GetTextureId() const
	{
		return textureId;
	}
}
