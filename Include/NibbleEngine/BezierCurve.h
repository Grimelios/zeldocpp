#pragma once
#include <vector>
#include "BezierPoint.h"

namespace Nibble
{
	class BezierCurve
	{
	private:

		std::vector<BezierPoint> controlPoints;

	public:

		std::vector<BezierPoint> ComputePoints(int count);
	};
}
