#pragma once

namespace Nibble
{
	enum class ShaderTypes
	{
		Vertex,
		TesselationControl,
		TesselationEvaluation,
		Geometry,
		Fragment
	};
}
