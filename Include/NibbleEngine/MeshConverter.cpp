#include "MeshConverter.h"
#include "StringUtilities.h"

namespace Nibble
{
	glm::vec2 MeshConverter::ParseVec2(const std::string& line)
	{
		const auto tokens = StringUtilities::Split(line, ' ');

		const float x = std::stof(tokens[1]);
		const float y = std::stof(tokens[2]);

		return glm::vec2(x, y);
	}

	glm::vec3 MeshConverter::ParseVec3(const std::string& line)
	{
		const auto tokens = StringUtilities::Split(line, ' ');

		const float x = std::stof(tokens[1]);
		const float y = std::stof(tokens[2]);
		const float z = std::stof(tokens[3]);

		return glm::vec3(x, y, z);
	}
}
