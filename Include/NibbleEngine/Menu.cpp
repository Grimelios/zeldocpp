#include "Menu.h"
#include "AggregateData.h"

namespace Nibble
{
	MenuControls Menu::controls;

	void Menu::ProcessInput(const AggregateData& data, float dt)
	{
		// The various menu controls are processed with a specific priority. Highest priority is back, followed by
		// submit, then the arrow keys. If multiple are pressed on the same frame, the highest-priority control is
		// executed (assuming it's valid) and others are ignored.
		if (data.Query(controls.back, InputStates::PressedThisFrame))
		{
		}

		if (data.Query(controls.submit, InputStates::PressedThisFrame))
		{
			Submit(selectedIndex);
		}

		const bool up = data.Query(controls.up, InputStates::PressedThisFrame);
		const bool down = data.Query(controls.down, InputStates::PressedThisFrame);

		if (up ^ down)
		{
			const int finalIndex = static_cast<int>(items.size()) - 1;

			if (up)
			{
				// Using a loop ensures that the selected index will correctly move past all disabled items.
				do
				{
					selectedIndex = selectedIndex == 0 ? finalIndex : --selectedIndex;
				}
				while (!items[selectedIndex]->enabled);
			}
			else
			{
				do
				{
					selectedIndex = selectedIndex == finalIndex ? 0 : ++selectedIndex;
				}
				while (!items[selectedIndex]->enabled);
			}
		}
	}
}
