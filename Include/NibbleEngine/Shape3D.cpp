#include "Shape3D.h"
#include "ShapeHelper.h"

namespace Nibble
{
	Shape3D::Shape3D(const ShapeTypes3D type) : shapeType(type)
	{
	}

	ShapeTypes3D Shape3D::GetShapeType() const
	{
		return shapeType;
	}

	bool Shape3D::Intersects(const Shape3D& other) const
	{
		return ShapeHelper::CheckIntersection(*this, other);
	}
}
