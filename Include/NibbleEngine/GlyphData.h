#pragma once
#include <glm/vec2.hpp>
#include <array>

namespace Nibble
{
	class GlyphData
	{
	private:

		int width;
		int height;
		int advance;

		glm::ivec2 offset;
		std::array<glm::vec2, 4> coords = { };

	public:

		GlyphData(int x, int y, int width, int height, int advance, int tWidth, int tHeight, const glm::ivec2& offset);

		int GetWidth() const;
		int GetHeight() const;
		int GetAdvance() const;

		const glm::ivec2& GetOffset() const;
		const std::array<glm::vec2, 4>& GetCoords() const;
	};
}
