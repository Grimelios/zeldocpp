#pragma once
#include "KeyModifiers.h"

namespace Nibble
{
	class KeyPress
	{
	private:

		int key;

		KeyModifiers mods;

	public:

		KeyPress(int key, KeyModifiers mods);

		int GetKey() const;

		KeyModifiers GetMods() const;
	};
}
