#pragma once
#include "Shape2D.h"

namespace Nibble
{
	class Circle : public Shape2D
	{
	public:

		Circle();

		glm::vec2 position = glm::vec2(0);

		float radius = 0;
	};
}
