#include "MapPhysicsHelper.h"
#include "RigidBody3D.h"
#include "Mesh.h"
#include "World.h"
#include "Shape3D.h"
#include "Sphere.h"
#include "Plane.h"
#include <algorithm>
#include "PhysicsUtilities.h"
#include "GameFunctions.h"

namespace Nibble
{
	MapPhysicsHelper::MapPhysicsHelper(World& world) : PhysicsHelper(world)
	{
	}

	void MapPhysicsHelper::ProcessBody(RigidBody3D& body)
	{
		auto& shape = body.shape;

		switch (shape.GetShapeType())
		{
			case ShapeTypes3D::Sphere: ProcessSphere(body, static_cast<Sphere&>(shape)); break;
		}
	}

	void MapPhysicsHelper::ProcessSphere(RigidBody3D& body, Sphere& sphere) const
	{
		const float r = sphere.radius;

		glm::vec3& p = sphere.position;

		const auto& pointList = worldMesh->GetPointList();
		const auto& vertexList = worldMesh->GetVertexList();
		const auto& indexList = worldMesh->GetIndexList();

		for (int i = 0; i < static_cast<int>(indexList.size()); i += 3)
		{
			const auto& p0 = pointList[std::get<0>(vertexList[indexList[i]])];
			const auto& p1 = pointList[std::get<0>(vertexList[indexList[i + 1]])];
			const auto& p2 = pointList[std::get<0>(vertexList[indexList[i + 2]])];

			const Plane plane(p0, p1, p2);
			
			const glm::vec3& n = plane.GetNormal();

			const float signedDistance = plane.ComputeSignedDistance(p);

			// If the sphere isn't touching the plane, it definitely isn't touching the triangle.
			if (std::abs(signedDistance) > r)
			{
				continue;
			}

			const glm::vec3 projected = p - signedDistance * n;

			const bool withinTriangle = PhysicsUtilities::IsPointWithinTriangle(projected, p0, p1, p2);

			if (withinTriangle)
			{
				p = projected + n * r;
				body.velocity = GameFunctions::ReflectVector(body.velocity, n) * body.material.restitution;

				//const glm::vec3 destination = basePoint + velocity;
				//const glm::vec3 newDestination = destination + planeNormal * plane.ComputeSignedDistance(destination);
				//const glm::vec3& correctedNormal = planeNormal * radius;

				//const Contact contact(planeIntersection * radius, correctedNormal, 1 - signedDistance);
				//const Face face(pArrayOriginal, correctedNormal, 0);

				////body.shape.position = (planeIntersection + planeNormal) * radius;
				////body.velocity = glm::vec3(0);
				//body.onCollision.Invoke(nullptr, contact, &face);
			}
		}
	}
}
