#include "Sphere.h"

namespace Nibble
{
	Sphere::Sphere() : Sphere(1.0f)
	{
	}

	Sphere::Sphere(const float radius) : Shape3D(ShapeTypes3D::Sphere),
		radius(radius)
	{
	}

	float Sphere::ComputeVolume() const
	{
		return 4.0f / 3.0f * glm::pi<float>() * radius * radius * radius;
	}
}
