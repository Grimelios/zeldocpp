#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class IPositionable3D
	{
	public:

		virtual ~IPositionable3D() = default;

		virtual const glm::vec3& GetPosition() const = 0;
		virtual void SetPosition(const glm::vec3& position) = 0;
	};
}
