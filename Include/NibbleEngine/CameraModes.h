#pragma once

namespace Nibble
{
	// Defining the separation of responsibilities for camera control between the engine and the game is tricky. Camera
	// "modes" are generally common among different 3D games (as far as the way the camera generally moves on a frame-
	// by-frame basis), but the specific details of that behavior should be customizable (e.g. subtle view bob while
	// running). To that end, this enumeration is used more as metadata than any specific implementation for camera
	// movement.
	enum class CameraModes
	{
		Fixed,
		Free,
		Follow,
		Pan
	};
}
