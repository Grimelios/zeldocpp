#pragma once
#include <glm/vec3.hpp>

namespace Nibble::PhysicsUtilities
{
	bool IsPointWithinTriangle(const glm::vec3& p, const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2);
}
