#pragma once
#include "InputData.h"

namespace Nibble
{
	class InputBind
	{
	public:

		InputBind(InputTypes type, int data);

		InputTypes type;

		// My assumption is that all input data will be rebindable to integers. That's already the case with KBM data
		// (using GFLW enums).
		int data;
	};
}
