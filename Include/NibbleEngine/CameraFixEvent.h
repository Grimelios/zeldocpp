#pragma once
#include "CutsceneEvent.h"
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class CameraFixEvent : public CutsceneEvent
	{
	private:

		glm::vec3 position;
		glm::quat orientation;

	protected:

		void Execute(Scene& scene, float elapsed) override;

	public:

		explicit CameraFixEvent(const Json& j);
	};
}
