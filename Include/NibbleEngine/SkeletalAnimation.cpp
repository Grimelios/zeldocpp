#include "SkeletalAnimation.h"

namespace Nibble
{
	SkeletalAnimation SkeletalAnimation::Load(const std::string& filename)
	{
		return SkeletalAnimation(std::vector<Timeline>(), 10.0f);
	}

	SkeletalAnimation::SkeletalAnimation(std::vector<Timeline> timelines, const float length) :
		timelines(std::move(timelines)),
		length(length)
	{
	}
}
