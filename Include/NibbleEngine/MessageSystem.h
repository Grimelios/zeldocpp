#pragma once
#include <vector>
#include <optional>
#include <functional>
#include <array>
#include <any>
#include "MessageTypes.h"

namespace Nibble
{
	class MessageHandle;
	class MessageSystem
	{
	private:

		static const int TypeCount = static_cast<int>(MessageTypes::Count);

		// Functions are stored as optional due to the way actions are stored in an array. When a receiver
		// unsubscribes, its corresponding index can be set to nullopt while leaving later functions intact.
		using ReceiverFunction = std::function<void(const std::any&, float)>;
		using ReceiverVector = std::vector<std::optional<ReceiverFunction>>;
		using ReceiverArray = std::array<ReceiverVector, TypeCount>;
		using IndexArray = std::array<int, TypeCount>;

		static ReceiverArray receiverArray;
		static IndexArray indexArray;

		static void Subscribe(MessageTypes messageType, MessageHandle* handle, const ReceiverFunction& action);

	public:

		static void Initialize();
		static void Subscribe(MessageTypes messageType, const ReceiverFunction& action);
		static void Subscribe(MessageTypes messageType, MessageHandle& handle, const ReceiverFunction& action);
		static void Unsubscribe(MessageTypes messageType, int index);
		static void Send(MessageTypes messageType, const std::any& data, float dt = 0);
		static void ProcessChanges();
	};
}
