#pragma once

namespace Nibble
{
	enum class SpriteModifiers
	{
		None = 0,
		FlipVertical = 1,
		FlipHorizontal = 2
	};

	inline int operator&(const SpriteModifiers m1, const SpriteModifiers m2)
	{
		return static_cast<int>(m1) & static_cast<int>(m2);
	}

	inline SpriteModifiers operator|(SpriteModifiers m1, SpriteModifiers m2)
	{
		return static_cast<SpriteModifiers>(static_cast<int>(m1) | static_cast<int>(m2));
	}
}
