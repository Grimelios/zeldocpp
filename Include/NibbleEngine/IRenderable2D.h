#pragma once

namespace Nibble
{
	class SpriteBatch;
	class IRenderable2D
	{
	public:

		virtual ~IRenderable2D() = default;
		virtual void Draw(SpriteBatch& sb) = 0;
	};
}
