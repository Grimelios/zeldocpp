#pragma once
#include <vector>

namespace Nibble
{
	class IReloadable;
	class KeyboardData;
	class ReloadHelper
	{
	private:

		static std::vector<IReloadable*> items;

		static void ProcessKeyboard(const KeyboardData& data);

	public:

		static void Initialize();
		static void Add(IReloadable* item);
	};
}
