#pragma once

namespace Nibble
{
	class Material
	{
	private:

		float diffuse;
		float specular;

	public:

		Material(float diffuse, float specular);

		float GetDiffuse() const;
		float GetSpecular() const;
	};
}
