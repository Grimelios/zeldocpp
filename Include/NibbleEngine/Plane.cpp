#include "Plane.h"
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	Plane::Plane(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2) :
		normal(normalize(cross(p1 - p0, p2 - p0))),
		equation(-dot(normal, p0))
	{
	}

	const glm::vec3& Plane::GetNormal() const
	{
		return normal;
	}

	float Plane::ComputeSignedDistance(const glm::vec3& p) const
	{
		return dot(p, normal) + equation;
	}
}
