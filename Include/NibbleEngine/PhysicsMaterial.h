#pragma once

namespace Nibble
{
	class PhysicsMaterial
	{
	public:

		// These default values are arbitrary. The assumption is that most objects will set their own values anyway.
		float staticFriction = 0.2f;
		float dynamicFriction = 0.2f;
		float restitution = 0.2f;
	};
}
