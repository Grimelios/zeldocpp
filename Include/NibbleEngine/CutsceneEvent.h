#pragma once
#include <nlohmann/json.hpp>

namespace Nibble
{
	class Scene;
	class CutsceneEvent
	{
	private:

		friend class CutscenePlayer;

		// Setting this value externally (rather than through a constructor) simplifies derived constructors. This will
		// be especially true if additional fields are added.
		float timestamp = 0;

	protected:

		using Json = nlohmann::json;

		// Passing elapsed time as a parameter ensures various events will stay timed as accurately as possible in
		// relation to each other (since event timestamps might not exactly correspond to a frame boundary).
		virtual void Execute(Scene& scene, float elapsed) = 0;

	public:

		virtual ~CutsceneEvent() = default;
	};
}
