#pragma once
#include <glm/vec2.hpp>

namespace Nibble
{
	class IPositionable2D
	{
	public:

		virtual ~IPositionable2D() = default;

		virtual const glm::vec2& GetPosition() const = 0;
		virtual void SetPosition(const glm::vec2& position) = 0;
	};
}
