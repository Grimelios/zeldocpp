#pragma once
#include <array>

namespace Nibble
{
	template<class T, int W, int H>
	class Grid
	{
	private:

		std::array<T, W * H> array = { };

	public:

		Grid() = default;
		explicit Grid(const T& value);

		T& operator()(int x, int y);
	};

	template<class T, int W, int H>
	Grid<T, W, H>::Grid(const T& value)
	{
		std::fill(array.begin(), array.end(), value);
	}

	template<class T, int W, int H>
	T& Grid<T, W, H>::operator()(const int x, const int y)
	{
		return array[y * W + x];
	}
}
