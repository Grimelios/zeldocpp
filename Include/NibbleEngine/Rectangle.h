#pragma once
#include <array>
#include <glm/vec2.hpp>

namespace Nibble
{
	// This class is conceptually the same as Bounds2D, but bounds have additional functionality largely meant for
	// mouse processing and UI work. In contrast, rectangles are simpler and are only used as simple data (such as
	// source rectangles for sprites).
	class Rectangle
	{
	public:

		Rectangle();
		Rectangle(int width, int height);
		Rectangle(int x, int y, int width, int height);

		int x;
		int y;
		int width;
		int height;

		std::array<glm::ivec2, 4> ComputeCorners() const;
	};
}
