#pragma once
#include "Timer.h"

namespace Nibble
{
	class SingleTimer : public Timer
	{
	private:

		using SingleFunction = std::function<void(float)>;

		SingleFunction trigger;
		
	public:

		// This first constructor is useful when duration needs to be loaded externally.
		explicit SingleTimer(const SingleFunction& trigger, float elapsed = 0);
		SingleTimer(int duration, SingleFunction trigger, float elapsed = 0);

		void Update(float dt) override;
	};
}
