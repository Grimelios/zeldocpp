#pragma once
#include "Texture2D.h"
#include <string>
#include "Paths.h"
#include <map>
#include "SpriteFont.h"
#include "Mesh.h"
#include "Atlas.h"

namespace Nibble
{
	class ContentCache
	{
	private:

		using AtlasCache = std::map<std::string, Atlas>;
		using FontCache = std::map<std::string, SpriteFont>;
		using MeshCache = std::map<std::string, Mesh>;
		using TextureCache = std::map<std::string, Texture2D>;

		static AtlasCache atlasCache;
		static FontCache fontCache;
		static MeshCache meshCache;
		static TextureCache textureCache;

	public:

		static const Atlas& GetAtlas(const std::string& filename);
		static const Mesh& GetMesh(const std::string& filename);
		static const SpriteFont& GetFont(const std::string& name);
		static const Texture2D& GetTexture(const std::string& filename, const std::string& folder = Paths::Textures);
	};
}
