#pragma once
#include "IPositionable3D.h"
#include "IOrientable.h"

namespace Nibble
{
	class ITransformable : public IPositionable3D, public IOrientable
	{
	public:

		virtual ~ITransformable() = default;

		// This function allows transformable items to not duplicate their work by calling SetPosition and
		// SetOrientation separately.
		virtual void SetTransform(const glm::vec3& position, const glm::quat& orientation) = 0;
	};
}
