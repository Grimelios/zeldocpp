#pragma once
#include <rapidxml/rapidxml_utils.hpp>

namespace Nibble
{
	class XmlUtilities
	{
	private:

		using XDoc = rapidxml::xml_document<>;

	public:

		static void Parse(const std::string& filename, XDoc& doc);
	};
}
