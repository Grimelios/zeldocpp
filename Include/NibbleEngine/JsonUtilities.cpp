#include "JsonUtilities.h"
#include "FileUtilities.h"
#include "Paths.h"

namespace Nibble
{
	nlohmann::json JsonUtilities::Load(const std::string& filename)
	{
		return nlohmann::json::parse(FileUtilities::ReadAllText(Paths::Json + filename));
	}
}
