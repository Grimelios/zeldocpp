#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class VerletPoint
	{
	private:

		glm::vec3 position = glm::vec3(0);
		glm::vec3 oldPosition = glm::vec3(0);

		bool fixed = false;
	};
}
