#pragma once
#include <map>
#include <vector>
#include <string>
#include <glad/glad.h>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include "ShaderTypes.h"
#include "VertexAttribute.h"

namespace Nibble
{
	class BufferStore;
	class Shader
	{
	private:

		static void LoadShader(const std::string& filename, GLenum type, GLuint& shaderId);

		GLuint vShader = 0;
		GLuint tcShader = 0;
		GLuint teShader = 0;
		GLuint gShader = 0;
		GLuint fShader = 0;
		GLuint program = 0;
		GLuint vao = 0;

		GLuint buffer = 0;
		GLuint indexBuffer = 0;

		std::map<std::string, GLint> uniforms;
		std::vector<VertexAttribute> attributes;

		unsigned stride = 0;

		// Binding vertex attributes first requires binding a buffer. Since this buffer is generally stored in the
		// batch, this value allows the batch to finish binding the shader on first draw.
		bool bindingComplete = false;

		void GetUniforms();

	public:

		~Shader();

		GLuint GetProgram() const;

		unsigned GetStride() const;

		bool IsBindingComplete() const;

		void Attach(ShaderTypes shaderType, const std::string& filename);
		void CreateProgram();

		template<class T>
		void AddAttribute(int count, GLenum type, bool normalized, int padding = 0);
		void CompleteBinding(const BufferStore& store);
		void CompleteBinding(GLuint buffer, GLuint indexBuffer);

		// It's sometimes useful to bind the shader without binding the VAO and buffers (such as when setting uniforms
		// in a constructor).
		void Use() const;
		void Apply() const;

		// Each function must be overloaded explicitly (rather than using a template) because OpenGL uses dedicated
		// functions for different kinds of data
		void SetUniform(const std::string& name, int value);
		void SetUniform(const std::string& name, float value);
		void SetUniform(const std::string& name, const glm::ivec2& value);
		void SetUniform(const std::string& name, const glm::vec2& value);
		void SetUniform(const std::string& name, const glm::vec3& value);
		void SetUniform(const std::string& name, const glm::vec4& value);
		void SetUniform(const std::string& name, const glm::mat4& value);
	};

	template<class T>
	void Shader::AddAttribute(const int count, const GLenum type, const bool normalized, const int padding)
	{
		attributes.emplace_back(count, stride, type, normalized);

		// Padding can be useful when using the same GPU buffer for multiple shaders with different vertex
		// requirements.
		stride += sizeof(T) * (count + padding);
	}
}
