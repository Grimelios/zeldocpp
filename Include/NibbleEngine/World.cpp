#include "World.h"
#include "Entity.h"
#include "Mesh.h"
#include "Color.h"
#include "Shape3D.h"

namespace Nibble
{
	World::World() :
		mapHelper(*this),
		primitives(4000, 2000)
	{
	}

	void World::RemoveBody(RigidBody3D* body)
	{
	}

	void World::SetWorldMesh(const Mesh* mesh)
	{
		worldMesh = mesh;
		mapHelper.worldMesh = mesh;
	}

	void World::Step(const float dt)
	{
		if (!enabled)
		{
			return;
		}

		// Velocity is applied to all bodies first, followed by collision detection and resolution.
		for (auto& b : bodies)
		{
			switch (b.bodyType)
			{
				case BodyTypes::Static: continue;
				case BodyTypes::Kinematic: b.shape.position += b.velocity * dt; continue;
			}

			auto& v = b.velocity;

			if (b.affectedByGravity)
			{
				v.y -= gravity * dt;

				if (v.y < -TerminalVelocity)
				{
					v.y = -TerminalVelocity;
				}
			}

			b.shape.orientation += b.angularVelocity * dt;
			b.shape.position += v * dt;
		}

		for (auto& b : bodies)
		{
			if (b.bodyType != BodyTypes::Dynamic)
			{
				continue;
			}

			// This assumes that all bodies collide with the map.
			mapHelper.ProcessBody(b);

			b.parent.SetTransformFromBody(b.shape.position + b.velocity * dt, b.shape.orientation);
		}
	}

	void World::Draw(const Camera& camera)
	{
		if (primitives.camera == nullptr)
		{
			primitives.camera = &camera;
		}

		const auto& pointList = worldMesh->GetPointList();
		const auto& vertexList = worldMesh->GetVertexList();
		const auto& indexList = worldMesh->GetIndexList();

		const Color triangleColor = Color(255, 0, 0);

		for (int i = 0; i < static_cast<int>(indexList.size()); i += 3)
		{
			const glm::vec3& p0 = pointList[std::get<0>(vertexList[indexList[i]])];
			const glm::vec3& p1 = pointList[std::get<0>(vertexList[indexList[i + 1]])];
			const glm::vec3& p2 = pointList[std::get<0>(vertexList[indexList[i + 2]])];

			primitives.DrawLine(p0, p1, triangleColor);
			primitives.DrawLine(p1, p2, triangleColor);
			primitives.DrawLine(p2, p0, triangleColor);
		}

		primitives.Flush();
	}
}
