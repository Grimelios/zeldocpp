#include "Button.h"
#include "InputRenderer.h"

namespace Nibble
{
	Button::Button() : InputControl(InputControlTypes::Button)
	{
	}

	void Button::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Button::Draw(SpriteBatch& sb)
	{
		renderer->Draw(sb);
	}
}
