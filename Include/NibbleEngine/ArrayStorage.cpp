#include "ArrayStorage.h"

namespace Nibble
{
	ArrayStorage::ArrayStorage(const int capacity)
	{
		buffer.reserve(capacity);

		glGenBuffers(1, &bufferId);
		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * capacity, nullptr, GL_DYNAMIC_DRAW);
	}

	GLuint ArrayStorage::GetBufferId() const
	{
		return bufferId;
	}

	std::vector<float>& ArrayStorage::GetBuffer()
	{
		return buffer;
	}

	void ArrayStorage::Apply(const bool clearBuffer)
	{
		if (buffer.empty())
		{
			return;
		}

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * buffer.size(), &buffer[0]);

		if (clearBuffer)
		{
			buffer.clear();
		}
	}
}
