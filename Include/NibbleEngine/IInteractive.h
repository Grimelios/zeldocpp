#pragma once
#include <string>

namespace Nibble
{
	class Entity;

	class IInteractive
	{
	public:

		virtual ~IInteractive() = default;

		// Some objects respond to interaction immediately, while others wait for specific animation cues from their
		// target entity. It's also possible for an interative object to react to multiple cues during a single
		// animation. In those cases, this function is called multiple times, with the expectation that the object
		// itself will track its own internal state.
		virtual void OnInteract(Entity* target) = 0;

		// This function tests contextual interaction (as opposed to the entire sensor being enabled or disabled).
		virtual bool TestInteraction(Entity* target) = 0;

		virtual int GetNextAnimationCue() = 0;

		// Having interative objects tell entities how to animate themselves helps keep classes decoupled (with the
		// assumption that animations and cues will be correctly synchronized in files).
		virtual std::string GetTargetAnimationName() = 0;
	};
}
