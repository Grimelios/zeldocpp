#include "PrimitiveRenderer3D.h"
#include "Camera.h"
#include "Color.h"
#include <array>

namespace Nibble
{
	PrimitiveRenderer3D::PrimitiveRenderer3D(const int bufferCapacity, const int indexCapacity) :
		storage(bufferCapacity, indexCapacity)
	{
		defaultShader.Attach(ShaderTypes::Vertex, "Primitives3D.vert");
		defaultShader.Attach(ShaderTypes::Fragment, "Primitives.frag");
		defaultShader.CreateProgram();
		defaultShader.AddAttribute<float>(3, GL_FLOAT, false);
		defaultShader.AddAttribute<std::byte>(4, GL_UNSIGNED_BYTE, true);
		defaultShader.CompleteBinding(storage);
	}

	void PrimitiveRenderer3D::DrawLine(const glm::vec3& p0, const glm::vec3& p1, const Color& color)
	{
		VerifyMode(GL_LINES);
		Buffer(std::array<glm::vec3, 2>({ p0, p1 }), color);
	}

	void PrimitiveRenderer3D::DrawTriangle(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2,
		const Color& color)
	{
		VerifyMode(GL_TRIANGLES);
		Buffer(std::array<glm::vec3, 3>({ p0, p1, p2 }), color);
	}

	void PrimitiveRenderer3D::VerifyMode(const GLenum mode)
	{
		if (this->mode != 0 && this->mode != mode)
		{
			Flush();
		}

		this->mode = mode;
	}

	void PrimitiveRenderer3D::Flush(Shader* customShader)
	{
		if (storage.IsEmpty())
		{
			return;
		}

		glDisable(GL_CULL_FACE);

		Shader* shader = customShader != nullptr ? customShader : &defaultShader;

		// It's assumed that all primitive shaders take an MVP matrix using the camera. Other uniforms are assumed to
		// have already been set externally.
		shader->Apply();
		shader->SetUniform("mvp", camera->GetViewProjection());

		glDrawElements(mode, storage.Flush(), GL_UNSIGNED_SHORT, nullptr);
	}
}
