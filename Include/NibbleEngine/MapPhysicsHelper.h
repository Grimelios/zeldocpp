#pragma once
#include "PhysicsHelper.h"

namespace Nibble
{
	class Mesh;
	class MapPhysicsHelper : public PhysicsHelper
	{
	private:

		friend class World;

		const Mesh* worldMesh = nullptr;

		void ProcessSphere(RigidBody3D& body, Sphere& sphere) const;

	protected:

		void ProcessBody(RigidBody3D& body) override;

	public:

		explicit MapPhysicsHelper(World& world);
	};
}
