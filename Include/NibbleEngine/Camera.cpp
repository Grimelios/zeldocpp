#include "Camera.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Resolution.h"
#include "MessageSystem.h"
#include "Entity.h"

namespace Nibble
{
	Camera::Camera()
	{
		MessageSystem::Subscribe(MessageTypes::Resize, handle, [this](const std::any& data, const float dt)
		{
			OnResize();
		});
	}

	void Camera::OnResize()
	{
		const float w = static_cast<float>(Resolution::WindowWidth);
		const float h = static_cast<float>(Resolution::WindowHeight);

		projection = glm::perspectiveFov(fieldOfView, w, h, nearPlane, farPlane);
	}

	const glm::mat4& Camera::GetViewProjection() const
	{
		return viewProjection;
	}

	void Camera::Update(const float dt)
	{
		// In theory, this function could be removed entirely (since external controllers could just directly compute
		// the view matrix). It still feels better to keep this function intact since some common functionality could
		// be included here going forward (such as pan points during a cutscene).
		glm::mat4 view;

		switch (mode)
		{
			case CameraModes::Free:
			case CameraModes::Fixed:
				view = mat4_cast(orientation) * translate(glm::mat4(1), position);

				break;

			case CameraModes::Follow:
				const float followDistance = 4.0f;

				const glm::vec3& targetPosition = target->GetPosition();

				position = targetPosition + orientation * glm::vec3(0, 0, followDistance);
				view = lookAt(position, targetPosition, glm::vec3(0, 1, 0));

				break;
		}	

		viewProjection = projection * view;
	}
}
