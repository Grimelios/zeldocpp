#pragma once
#include <optional>
#include <string>
#include <array>
#include "Alignments.h"
#include "Component2D.h"
#include "Rectangle.h"

namespace Nibble
{
	class QuadSource;
	class Sprite : public Component2D
	{
	private:

		using SourceRect = std::optional<Rectangle>;

		const QuadSource& source;

		// The vertex structure is interleaved (position, texture coordinates, and color).
		std::array<float, FloatsPerQuad> vertexData { };

		SourceRect sourceRect;

		Sprite(const QuadSource& source, const SourceRect& sourceRect, Alignments alignment);

		void RecomputeOrigin();

	public:

		explicit Sprite(const std::string& filename, Alignments alignment = Alignments::Center);
		explicit Sprite(const QuadSource& source, Alignments alignment = Alignments::Center);

		Sprite(const std::string& filename, const Rectangle& sourceRect, Alignments alignment = Alignments::Center);
		Sprite(const QuadSource& source, const Rectangle& sourceRect, Alignments alignment = Alignments::Center);

		const QuadSource& GetSource() const;

		void ScaleTo(int width, int height);
		void SetSourceRect(const SourceRect& sourceRect);
		void Draw(SpriteBatch& sb) override;
	};
}
