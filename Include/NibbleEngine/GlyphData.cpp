#include "GlyphData.h"
#include <glm/vec2.hpp>

namespace Nibble
{
	GlyphData::GlyphData(const int x, const int y, const int width, const int height, const int advance,
		const int tWidth, const int tHeight, const glm::ivec2& offset) :
		
		width(width),
		height(height),
		advance(advance),
		offset(offset)
	{
		const float fX = static_cast<float>(x) / tWidth;
		const float fY = static_cast<float>(y) / tHeight;
		const float fWidth = static_cast<float>(width) / tWidth;
		const float fHeight = static_cast<float>(height) / tHeight;

		coords =
		{
			glm::vec2(fX, fY),
			glm::vec2(fX, fY + fHeight),
			glm::vec2(fX + fWidth, fY),
			glm::vec2(fX + fWidth, fY + fHeight)
		};
	}

	int GlyphData::GetWidth() const
	{
		return width;
	}

	int GlyphData::GetHeight() const
	{
		return height;
	}

	int GlyphData::GetAdvance() const
	{
		return advance;
	}

	const glm::ivec2& GlyphData::GetOffset() const
	{
		return offset;
	}

	const std::array<glm::vec2, 4>& GlyphData::GetCoords() const
	{
		return coords;
	}
}
