#include "Material.h"

namespace Nibble
{
	Material::Material(const float diffuse, const float specular) :
		diffuse(diffuse),
		specular(specular)
	{
	}

	float Material::GetDiffuse() const
	{
		return diffuse;
	}

	float Material::GetSpecular() const
	{
		return specular;
	}
}
