#pragma once
#include "IRenderable2D.h"
#include "IDynamic.h"
#include "InputControlTypes.h"

namespace Nibble
{
	template<class T>
	class InputRenderer;
	class InputControl : public IDynamic, public IRenderable2D
	{
	private:

		// Input type is require to properly create renderers in some cases where several control types are present
		// (such as screens).
		InputControlTypes controlType;

	protected:

		explicit InputControl(InputControlTypes type);

		// This variable allows controls to stay more generic since derived classes can freely store their own data
		// as needed. Classes that use controls (like screens) can retrieve the appropriate type of data when needed
		// (such as when saving game options). Note that not all controls store data (like buttons).
		void* valuePointer = nullptr;

	public:

		InputControlTypes GetControlType() const;

		// D stands for "data". Note that two versions of this function are needed (one that returns an object and one
		// that returns a pointer) because function calls can be ambigious with the same name (for the compiler).
		template<class D>
		D GetValue() const;

		template<class D>
		const D* GetValuePointer() const;
	};

	template<class D>
	D InputControl::GetValue() const
	{
		return *static_cast<const D*>(valuePointer);
	}

	template<class D>
	const D* InputControl::GetValuePointer() const
	{
		return static_cast<const D*>(valuePointer);
	}
}
