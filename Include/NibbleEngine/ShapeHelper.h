#pragma once

namespace Nibble
{
	class Sphere;
	class Shape3D;
	class ShapeHelper
	{
	private:

		static bool CheckIntersection(const Sphere& s1, const Sphere& s2);

	public:

		static bool CheckIntersection(const Shape3D& shape1, const Shape3D& shape2);
	};
}
