#pragma once
#include <vector>
#include <glm/vec3.hpp>

namespace Nibble
{
	class Path
	{
	public:

		std::vector<glm::vec3> controlPoints;
	};
}
