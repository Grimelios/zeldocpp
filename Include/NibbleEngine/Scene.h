#pragma once
#include "Entity.h"
#include "IDynamic.h"
#include "DoublyLinkedList.h"
#include "IRenderTargetUser.h"
#include "RenderTarget.h"
#include "MeshRenderer.h"
#include "EntityFactory.h"

namespace Nibble
{
	class World;
	class Space;
	class Entity;
	class Camera;
	class Canvas;
	class Scene : public IDynamic, public IRenderTargetUser
	{
	private:

		std::vector<std::unique_ptr<Entity>> addList;
		std::vector<Entity*> removeList;

		DoublyLinkedList<std::unique_ptr<Entity>> mainList;
		RenderTarget renderTarget;

		void ProcessChanges();

		template<class T>
		T* AddInternal(std::unique_ptr<Entity>& p);

	public:

		Scene();

		std::unique_ptr<EntityFactory> entityFactory = nullptr;

		World* world = nullptr;
		Space* space = nullptr;
		Camera* camera = nullptr;
		Canvas* canvas = nullptr;
		MeshRenderer* meshRenderer = nullptr;

		const RenderTarget& GetTarget() const override;

		// This function allows external classes to more easily create entities manually before adding to the scene.
		// Since the scene ultimately owns its entities, the given object is moved and a pointer to the new copy is
		// returned.
		template<class T>
		T* AddEntity(T& entity);

		template<class T, class... Args>
		T* CreateEntity(Args&&... args);

		void LoadFragment(const std::string& filename);
		void RemoveEntity(Entity* entity);
		void Update(float dt) override;
		void DrawTarget(const Camera& camera) override;
	};

	template<class T>
	T* Scene::AddInternal(std::unique_ptr<Entity>& p)
	{
		T* entity = static_cast<T*>(mainList.Add(p)->get());
		entity->scene = this;
		entity->Initialize(this, nullptr);

		return entity;
	}

	template<class T>
	T* Scene::AddEntity(T& entity)
	{
		std::unique_ptr<Entity> pointer = std::make_unique<T>(std::move(entity));

		return AddInternal<T>(pointer);
	}

	template<class T, class... Args>
	T* Scene::CreateEntity(Args&&... args)
	{
		std::unique_ptr<Entity> pointer = std::make_unique<T>(args...);

		return AddInternal<T>(pointer);
	}
}
