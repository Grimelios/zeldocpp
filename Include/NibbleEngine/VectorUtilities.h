#pragma once
#include <vector>

namespace Nibble::VectorUtilities
{
	template<class T, size_t S>
	void Push(std::vector<T>& v, const std::array<T, S>& a)
	{
		std::copy(a.begin(), a.end(), std::back_inserter(v));
	}

	template<class T>
	void Push(std::vector<T>& v1, const std::vector<T>& v2)
	{
		std::copy(v2.begin(), v2.end(), std::back_inserter(v1));
	}

	template<class T>
	void Repeat(std::vector<T>& v, const T value, const int count)
	{
		v.insert(v.end(), count, value);
	}

	template<class T>
	void RemoveAt(std::vector<T>& v, const int index)
	{
		v.erase(v.begin() + index);
	}

	template<class T>
	void RemoveFrom(std::vector<T>& v, const int index)
	{
		v.erase(v.begin() + index, v.end());
	}

	template<class T>
	int IndexOf(const std::vector<T>& v, const T& item)
	{
		const auto& i = std::find(v.begin(), v.end(), item);

		if (i == v.end())
		{
			return -1;
		}

		return static_cast<int>(i - v.begin());
	}

	template<class T>
	bool Contains(const std::vector<T>& v, const T& value)
	{
		return std::find(v.begin(), v.end(), value) != v.end();
	}

	template<class T>
	bool Contains(const std::vector<T>& v, const T& value, int& index)
	{
		return (index = IndexOf(v, value)) != -1;
	}
}