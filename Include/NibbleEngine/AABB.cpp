#include "AABB.h"

namespace Nibble
{
	bool AABB::Overlaps(const AABB& other) const
	{
		const float dX = std::abs(center.x - other.center.x);
		const float dY = std::abs(center.y - other.center.y);
		const float dZ = std::abs(center.z - other.center.z);

		return dX <= (bounds.x + other.bounds.x) / 2 &&
			dY <= (bounds.y + other.bounds.y) / 2 &&
			dZ <= (bounds.z + other.bounds.z) / 2;
	}
}
