#include "Rng.h"

namespace Nibble
{
	// See https://stackoverflow.com/a/7560564/7281613.
	std::mt19937 Rng::generator = std::mt19937(std::random_device()());

	int Rng::GetValue(const int min, const int max)
	{
		const std::uniform_int_distribution<> d(min, max);

		return d(generator);
	}

	float Rng::GetValue(const float min, const float max)
	{
		const std::uniform_real_distribution<> d(min, max);

		return static_cast<float>(d(generator));
	}
}
