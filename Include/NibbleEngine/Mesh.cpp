#include "Mesh.h"
#include <utility>
#include "StringUtilities.h"
#include "FileUtilities.h"
#include "ContentCache.h"

namespace Nibble
{
	Mesh Mesh::Load(const std::string& filename)
	{
		const auto lines = FileUtilities::ReadAllLines(filename);

		// Parse texture.
		const Texture2D* texture = !lines[0].empty() ? &ContentCache::GetTexture(lines[0]) : nullptr;

		// Parse vertex data.
		std::vector<glm::vec3> pointList = ParseVec3List(lines[1]);
		std::vector<glm::vec2> sourceList = ParseVec2List(lines[2]);
		std::vector<glm::vec3> normalList = ParseVec3List(lines[3]);

		// Parse vertices.
		std::vector<std::string> tokens = StringUtilities::Split(lines[4], ',');
		std::vector<MeshVertex> vertexList;
		vertexList.reserve(tokens.size() / 3);

		for (int i = 0; i < static_cast<int>(tokens.size()); i += 3)
		{
			const IndexType pointIndex = static_cast<IndexType>(std::stoi(tokens[i]));
			const IndexType sourceIndex = static_cast<IndexType>(std::stoi(tokens[i + 1]));
			const IndexType normalIndex = static_cast<IndexType>(std::stoi(tokens[i + 2]));

			vertexList.emplace_back(pointIndex, sourceIndex, normalIndex);
		}

		// Parse index list.
		tokens = StringUtilities::Split(lines[5], ',');

		std::vector<IndexType> indexList;
		indexList.reserve(tokens.size());

		for (const auto& s : tokens)
		{
			indexList.push_back(static_cast<IndexType>(std::stoi(s)));
		}

		return Mesh(pointList, sourceList, normalList, texture, vertexList, indexList);
	}

	std::vector<glm::vec2> Mesh::ParseVec2List(const std::string& line)
	{
		std::vector<std::string> tokens = StringUtilities::Split(line, ',');
		std::vector<glm::vec2> list;
		list.reserve(tokens.size() / 3);

		for (int i = 0; i < static_cast<int>(tokens.size()); i += 2)
		{
			const float x = std::stof(tokens[i]);
			const float y = std::stof(tokens[i + 1]);

			list.emplace_back(x, y);
		}

		return list;
	}

	std::vector<glm::vec3> Mesh::ParseVec3List(const std::string& line)
	{
		std::vector<std::string> tokens = StringUtilities::Split(line, ',');
		std::vector<glm::vec3> list;
		list.reserve(tokens.size() / 3);

		for (int i = 0; i < static_cast<int>(tokens.size()); i += 3)
		{
			const float x = std::stof(tokens[i]);
			const float y = std::stof(tokens[i + 1]);
			const float z = std::stof(tokens[i + 2]);

			list.emplace_back(x, y, z);
		}

		return list;
	}

	Mesh::Mesh(std::vector<glm::vec3>& pointList, std::vector<glm::vec2>& sourceList,
		std::vector<glm::vec3>& normalList, const Texture2D* texture, std::vector<MeshVertex>& vertexList,
		std::vector<IndexType>& indexList) :

		pointList(std::move(pointList)),
		sourceList(std::move(sourceList)),
		normalList(std::move(normalList)),
		vertexList(std::move(vertexList)),
		indexList(std::move(indexList)),
		texture(texture)
	{
	}

	const std::vector<glm::vec3>& Mesh::GetPointList() const
	{
		return pointList;
	}

	const std::vector<glm::vec2>& Mesh::GetSourceList() const
	{
		return sourceList;
	}

	const std::vector<glm::vec3>& Mesh::GetNormalList() const
	{
		return normalList;
	}

	const std::vector<Mesh::MeshVertex>& Mesh::GetVertexList() const
	{
		return vertexList;
	}

	const std::vector<Mesh::IndexType>& Mesh::GetIndexList() const
	{
		return indexList;
	}

	const Texture2D* Mesh::GetTexture() const
	{
		return texture;
	}
}
