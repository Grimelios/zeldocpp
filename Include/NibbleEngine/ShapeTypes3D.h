#pragma once

namespace Nibble
{
	enum class ShapeTypes3D
	{
		Box,
		Capsule,
		Sphere
	};
}
