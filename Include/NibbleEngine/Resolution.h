#pragma once

namespace Nibble
{
	class Resolution
	{
	private:

	public:

		// To handle different window sizes, the full game is always rendered at a preset resolution, then scaled to
		// fit the current window. This ensures that all players will see the same portion of the world, such that it's
		// not possible to gain an advantage through an increased visual range.
		static const int Width = 1920;
		static const int Height = 1080;

		static int WindowWidth;
		static int WindowHeight;
	};
}
