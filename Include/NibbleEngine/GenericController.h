#pragma once
#include "IDynamic.h"

namespace Nibble
{
	template<class T>
	class GenericController : public IDynamic
	{
	protected:

		T start;
		T end;

		float elapsed = 0;
		float duration = 0;

		bool complete = false;

		virtual void Lerp(float t) = 0;

	public:

		bool IsComplete() const;

		void Refresh(const T& start, const T& end, float duration);
		void Update(float dt) override;
	};

	template<class T>
	bool GenericController<T>::IsComplete() const
	{
		return complete;
	}

	template<class T>
	void GenericController<T>::Refresh(const T& start, const T& end, const float duration)
	{
		this->start = start;
		this->end = end;
		this->duration = duration;

		elapsed = 0;
	}

	template<class T>
	void GenericController<T>::Update(const float dt)
	{
		elapsed += dt;

		if (elapsed >= duration)
		{
			Lerp(1);

			complete = true;
		}
		else
		{
			Lerp(elapsed / duration);
		}
	}
}
