#pragma once
#include "BufferStore.h"
#include <vector>
#include "VectorUtilities.h"

namespace Nibble
{
	class DynamicBufferStore : public BufferStore
	{
	private:

		std::vector<float> buffer;
		std::vector<IndexType> indexBuffer;

	public:

		DynamicBufferStore(int bufferCapacity, int indexCapacity);

		bool IsEmpty() const;

		std::vector<float>& GetBuffer();
		std::vector<IndexType>& GetIndexBuffer();

		template<int S>
		void Append(const std::array<float, S>& data);

		template<int S>
		void AppendIndex(const std::array<IndexType, S>& indexData);

		// For classes that use dynamic storage, it's possible to update vertex data without affecting the connectivity
		// of those points (i.e. the index buffer). This is commonly true for non-rigid surfaces (like water or soft-
		// body entities).
		int Flush(bool clearBuffer = true, bool clearIndices = true);
	};

	template<int S>
	void DynamicBufferStore::Append(const std::array<float, S>& data)
	{
		VectorUtilities::Push(buffer, data);
	}

	template<int S>
	void DynamicBufferStore::AppendIndex(const std::array<IndexType, S>& indexData)
	{
		VectorUtilities::Push(indexBuffer, indexData);
	}
}
