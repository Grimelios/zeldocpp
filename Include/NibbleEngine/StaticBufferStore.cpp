#include "StaticBufferStore.h"

namespace Nibble
{
	StaticBufferStore::StaticBufferStore(const int bufferCapacity, const int indexCapacity) :
		BufferStore(bufferCapacity, indexCapacity, GL_STATIC_DRAW)
	{
	}

	void StaticBufferStore::Append(const std::vector<float>& data)
	{
		const int size = sizeof(float) * static_cast<int>(data.size());

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferSubData(GL_ARRAY_BUFFER, bufferSize, size, &data[0]);

		bufferSize += size;
	}

	void StaticBufferStore::AppendIndex(const std::vector<IndexType>& data)
	{
		const int size = sizeof(IndexType) * static_cast<int>(data.size());

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, indexBufferSize, size, &data[0]);

		indexBufferSize += size;
	}
}
