#include "MeshUtilities.h"
#include "FileUtilities.h"
#include "Paths.h"
#include "StringUtilities.h"
#include <sstream>
#include <array>

namespace Nibble
{
	DaeConverter MeshUtilities::daeConverter;
	ObjConverter MeshUtilities::objConverter;

	void MeshUtilities::ConvertAll()
	{
		const std::array<std::string, 2> extensions = { ".dae", ".obj" };

		std::vector<std::string> files = FileUtilities::GetFiles(Paths::Models, extensions, true, true);

		for (const auto& f : files)
		{
			const auto path = StringUtilities::RemoveExtension(f);
			const auto extension = StringUtilities::GetExtension(f);

			if (extension == "dae") { }
			if (extension == "obj") { SaveMesh(objConverter.ParseData(path), path + ".m"); }
		}
	}

	void MeshUtilities::SaveMesh(const std::pair<Mesh, std::string>& pair, const std::string& path)
	{
		std::stringstream s;

		const auto& mesh = pair.first;

		s << pair.second << '\n';

		WriteList(s, mesh.pointList);
		WriteList(s, mesh.sourceList);
		WriteList(s, mesh.normalList);

		const auto& vertexList = mesh.vertexList;
		const auto& indexList = mesh.indexList;

		const int vertexCount = static_cast<int>(vertexList.size());
		const int indexCount = static_cast<int>(indexList.size());

		for (int i = 0; i < vertexCount; i++)
		{
			const auto& t = vertexList[i];

			s << std::to_string(std::get<0>(t)) << ',';
			s << std::to_string(std::get<1>(t)) << ',';
			s << std::to_string(std::get<2>(t));

			if (i < vertexCount - 1)
			{
				s << ',';
			}
		}

		s << '\n';

		for (int i = 0; i < indexCount; i++)
		{
			s << indexList[i];

			if (i < indexCount - 1)
			{
				s << ',';
			}
		}

		FileUtilities::Write(path, s.str(), WriteModes::Overwrite);
	}
}
