#include "PhysicsUtilities.h"
#include <glm/detail/func_geometric.inl>

namespace Nibble
{
	bool PhysicsUtilities::IsPointWithinTriangle(const glm::vec3& p, const glm::vec3& p0, const glm::vec3& p1,
		const glm::vec3& p2)
	{
		// See http://www.peroxide.dk/papers/collision/collision.pdf (appendix E).
		const glm::vec3 e10 = p1 - p0;
		const glm::vec3 e20 = p2 - p0;

		const float a = dot(e10, e10);
		const float b = dot(e10, e20);
		const float c = dot(e20, e20);
		const float v = a * c - b * b;

		const glm::vec3 vp = p - p0;

		const float d = dot(vp, e10);
		const float e = dot(vp, e20);
		const float x = d * c - e * b;
		const float y = e * a - d * b;
		const float z = x + y - v;

		return static_cast<unsigned>(z) & ~(static_cast<unsigned>(x) | static_cast<unsigned>(y)) & 0x80000000;
	}
}
