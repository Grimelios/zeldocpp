#pragma once
#include <glm/vec3.hpp>
#include <array>

namespace Nibble
{
	// Note that in this game, all world faces are triangles.
	class Face
	{
	private:

		std::array<glm::vec3, 3> points;

		glm::vec3 normal;

		int flags;

	public:

		Face(const glm::vec3& p0, const glm::vec3& p1, const glm::vec3& p2, int flags);

		const std::array<glm::vec3, 3>& GetPoints() const;
		const glm::vec3& GetNormal() const;

		int GetFlags() const;
	};
}
