#pragma once
#include "RigidBody3D.h"
#include "RigidBodyHandle.h"
#include "PrimitiveRenderer3D.h"
#include "IRenderable3D.h"
#include "MapPhysicsHelper.h"

namespace Nibble
{
	class Mesh;
	class Entity;
	class Camera;
	class Shape3D;
	class World : public IRenderable3D
	{
	private:

		friend class MapPhysicsHelper;

		static constexpr float TerminalVelocity = 20.0f;
		static constexpr float Epsilon = 0.001f;
		static constexpr float AngularEpsilon = 0.001f;

		std::vector<RigidBody3D> bodies;

		const Mesh* worldMesh = nullptr;

		// Gravity is non-constant under the assumption that it will change in at least one area of the game.
		float gravity = 20.0f;

		MapPhysicsHelper mapHelper;
		PrimitiveRenderer3D primitives;

	public:

		World();

		bool enabled = true;

		template<class... Args>
		RigidBody3D* CreateBody(RigidBodyHandle& handle, Args&&... args);

		void RemoveBody(RigidBody3D* body);
		void SetWorldMesh(const Mesh* mesh);
		void Step(float dt);
		void Draw(const Camera& camera) override;
	};

	template<class... Args>
	RigidBody3D* World::CreateBody(RigidBodyHandle& handle, Args&&... args)
	{
		handle.body = &bodies.emplace_back(args...);
		handle.world = this;

		return handle.body;
	}
}
