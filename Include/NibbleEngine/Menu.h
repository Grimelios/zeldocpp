#pragma once
#include "CanvasElement.h"
#include "Bounds2D.h"
#include "IControllable.h"
#include "MenuControls.h"
#include "MenuItem.h"

namespace Nibble
{
	class Menu : public CanvasElement, public IControllable
	{
	private:

		static MenuControls controls;

		Bounds2D bounds;

		int selectedIndex = 0;

	protected:

		// Menu items are conceptually abstract (since menus can take a huge number of forms). The idea here, then, is
		// that derived classes can create their own lists of items in their constructors (whatever those items may
		// look like) and then move the item list down.
		std::vector<std::unique_ptr<MenuItem>> items;

	public:

		virtual ~Menu() = 0;
		virtual void Submit(int index) = 0;

		void ProcessInput(const AggregateData& data, float dt) override;
	};

	inline Menu::~Menu() = default;
}
