#pragma once
#include <glm/vec2.hpp>

namespace Nibble
{
	class IClickable
	{
	public:

		virtual ~IClickable() = default;

		virtual void OnHover(const glm::vec2& mousePosition) = 0;
		virtual void OnUnhover() = 0;
		virtual void OnClick(const glm::vec2& mousePosition) = 0;
		virtual void OnRelease() = 0;

		virtual bool Contains(const glm::vec2& mousePosition) = 0;
	};
}
