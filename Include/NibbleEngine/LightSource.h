#pragma once
#include <glm/vec3.hpp>
#include "Color.h"

namespace Nibble
{
	class LightSource
	{
	public:

		virtual ~LightSource() = 0;

		glm::vec3 position = glm::vec3(0);

		float range = 0;

		Color color;
	};

	inline LightSource::~LightSource() = default;
}
