#pragma once
#include <vector>
#include "DoublyLinkedList.h"

namespace Nibble
{
	template<class T>
	class SafeList
	{
	private:

		std::vector<T> addList;
		std::vector<T*> removeList;

		DoublyLinkedList<T> mainList;

	public:

		DoublyLinkedListNode<T>* GetHead() const;

		template<class... Args>
		void Add(Args&&... args);
		void Remove(T& item);
		void ProcessChanges();
	};

	template<class T>
	DoublyLinkedListNode<T>* SafeList<T>::GetHead() const
	{
		return mainList.GetHead();
	}

	template<class T>
	template<class... Args>
	void SafeList<T>::Add(Args&&... args)
	{
		addList.emplace_back(args...);
	}

	template<class T>
	void SafeList<T>::Remove(T& item)
	{
		removeList.push_back(&item);
	}

	template<class T>
	void SafeList<T>::ProcessChanges()
	{
		if (!removeList.empty())
		{
			for (T* item : removeList)
			{
				auto* node = mainList.GetHead();
				
				while (node != nullptr)
				{
					if (&node->data == item)
					{
						mainList.Remove(node);

						break;
					}

					node = node->next;
				}
			}

			removeList.clear();
		}

		if (!addList.empty())
		{
			for (T& item : addList)
			{
				mainList.Add(std::move(item));
			}

			addList.clear();
		}
	}
}
