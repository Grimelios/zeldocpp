#pragma once
#include <vector>
#include "CanvasElement.h"
#include <memory>
#include "InputControl.h"
#include "Component2D.h"

namespace Nibble
{
	class Screen : public CanvasElement
	{
	protected:

		std::vector<std::unique_ptr<Component2D>> components;
		std::vector<std::unique_ptr<InputControl>> controls;

		template<class T, class... Args>
		void AddComponent(Args&&... args);

		template<class T, class... Args>
		void AddControl(Args&&... args);

	public:

		virtual ~Screen() = 0;

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	inline Screen::~Screen() = default;

	template<class T, class... Args>
	void Screen::AddComponent(Args&&... args)
	{
		components.push_back(std::make_unique<T>(args...));
	}

	template<class T, class... Args>
	void Screen::AddControl(Args&&... args)
	{
		controls.push_back(std::make_unique<T>(args...));
	}
}
