#pragma once
#include "Color.h"

namespace Nibble
{
	class IColorable
	{
	public:

		virtual const Color& GetColor() const = 0;
		virtual void SetColor(const Color& color) = 0;
	};
}
