#pragma once
#include <string>
#include <vector>
#include <glm/vec3.hpp>
#include "DaeConverter.h"
#include "ObjConverter.h"
#include "GameFunctions.h"

namespace Nibble
{
	class MeshConversionData;
	class MeshUtilities
	{
	private:

		using IndexType = unsigned short;

		static DaeConverter daeConverter;
		static ObjConverter objConverter;
		
		static void SaveMesh(const std::pair<Mesh, std::string>& pair, const std::string& path);

		template<class T>
		static void WriteList(std::stringstream& s, const std::vector<T>& list);

	public:

		static void ConvertAll();
	};

	template<class T>
	void MeshUtilities::WriteList(std::stringstream& s, const std::vector<T>& list)
	{
		// Each vector list (points and normals) are written in a single line (comma-separated).
		const int count = static_cast<int>(list.size());

		for (int i = 0; i < count; i++)
		{
			const auto& v = list[i];

			s << GameFunctions::ToString(v);

			if (i < count - 1)
			{
				s << ',';
			}
		}

		s << '\n';
	}
}
