#pragma once
#include "Shape3D.h"

namespace Nibble
{
	class Capsule : public Shape3D
	{
	public:

		Capsule();
		Capsule(float radius, float height);

		// Note that height refers to the height of the cylindrical part of the capsule, not the full height.
		float radius;
		float height;

		float ComputeVolume() const override;
	};
}
