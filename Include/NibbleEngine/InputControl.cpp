#include "InputControl.h"

namespace Nibble
{
	InputControl::InputControl(const InputControlTypes type) : controlType(type)
	{
	}

	InputControlTypes InputControl::GetControlType() const
	{
		return controlType;
	}
}
