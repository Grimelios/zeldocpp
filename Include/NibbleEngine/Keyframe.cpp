#include "Keyframe.h"

namespace Nibble
{
	Keyframe::Keyframe(const glm::vec3& position, const glm::quat& orientation, const float time) :
		position(position),
		orientation(orientation),
		time(time)
	{
	}
}
