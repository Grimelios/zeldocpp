#pragma once
#include <vector>

namespace Nibble
{
	class MouseData;
	class IClickable;
	class ClickableSet
	{
	private:

		IClickable* hoveredItem = nullptr;
		IClickable* clickedItem = nullptr;

	public:

		std::vector<IClickable*> items;

		void ProcessMouse(const MouseData& data);
	};
}
