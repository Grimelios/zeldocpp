#include "OrientationController.h"
#include "IOrientable.h"

namespace Nibble
{
	OrientationController::OrientationController(IOrientable* target) : target(target)
	{
	}

	void OrientationController::Lerp(const float t)
	{
		target->SetOrientation(lerp(start, end, t));
	}
}
