#pragma once
#include <glad/glad.h>
#include <string>
#include "QuadSource.h"

namespace Nibble
{
	class Texture2D : public QuadSource
	{
	public:

		static Texture2D Load(const std::string& filename);

		Texture2D(int width, int height, GLuint textureId);
	};
}
