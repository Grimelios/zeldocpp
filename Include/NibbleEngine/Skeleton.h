#pragma once
#include <vector>
#include "Bone.h"

namespace Nibble
{
	class Skeleton
	{
	private:

		// Bones are conceptually arranged into a hierarchy, but they can be organized such that a regular loop (with
		// parent indices) is sufficient to compute bone transforms. Since bone hierarchies never change, this
		// reorganization can be stored in the animation file itself.
		std::vector<Bone> bones;
	};
}
