#include "VertexAttribute.h"

namespace Nibble
{
	VertexAttribute::VertexAttribute(const int count, const int offset, const GLenum type, const bool normalized) :
		type(type),
		count(count),
		offset(offset),
		normalized(normalized)
	{
	}
}
