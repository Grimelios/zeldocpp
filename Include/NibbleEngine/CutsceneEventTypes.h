#pragma once

namespace Nibble
{
	enum class CutsceneEventTypes
	{
		CameraFix,
		CameraFollow,
		CameraPan,
		EntityAnimation,
		EntityMovement,
		Sound,
		Speech
	};
}
