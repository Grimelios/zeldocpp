#pragma once
#include <glm/vec3.hpp>

namespace Nibble
{
	class AABB
	{
	private:

		glm::vec3 center = glm::vec3(0);
		glm::vec3 bounds = glm::vec3(0);

	public:

		bool Overlaps(const AABB& other) const;
	};
}
