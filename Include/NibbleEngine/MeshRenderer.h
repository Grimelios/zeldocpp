#pragma once
#include <vector>
#include <glm/mat4x4.hpp>
#include "Shader.h"
#include "RenderTarget.h"
#include "IRenderTargetUser.h"
#include "IRenderable3D.h"
#include "RenderContainer.h"
#include "DoublyLinkedList.h"
#include "StaticBufferStore.h"

namespace Nibble
{
	class Mesh;
	class Model;
	class Camera;
	class Texture2D;
	class RenderHandle;
	class MeshRenderer : public IRenderTargetUser, public IRenderable3D
	{
	private:

		static const int ShadowMapSize = 2048;

		static void DrawMesh(const Mesh& mesh, int& indexOffset, int& baseVertex);

		// Note that the mesh renderer needs models rather than pure meshes in order to have access to world matrices.
		using RenderPair = std::pair<Model*, RenderContainer>;
		using IndexType = unsigned short;

		// A list is required rather than a map because of ordering. Data is buffered to the GPU based on the order in
		// which models are added, so they must be rendered in that same order.
		using RenderList = DoublyLinkedList<RenderPair>;

		Shader shadowShader;
		Shader modelShader;
		StaticBufferStore storage;
		RenderTarget shadowTarget;
		RenderList renderList;

		const Texture2D& greyTexture;

		// Storing these values allows them to be reused for both creating the shadow map and actually drawing meshes.
		glm::vec3 lightDirection = glm::vec3(0);
		glm::mat4 lightVp = glm::mat4(1);

		void AddModel(Model& model, RenderHandle* handle);

	public:

		MeshRenderer();

		const RenderTarget& GetTarget() const override;

		// Toggling textures can be useful for testing.
		bool texturesEnabled = true;

		// The model reference is passed non-const to allow retrieving the world matrix while drawing.
		void AddModel(Model& model);
		void AddModel(Model& model, RenderHandle& handle);
		void DrawTarget(const Camera& camera) override;
		void Draw(const Camera& camera) override;
	};
}
