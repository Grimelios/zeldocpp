#include "Component2D.h"

namespace Nibble
{
	Component2D::Component2D(const Alignments alignment) : alignment(alignment)
	{
	}

	const glm::vec2& Component2D::GetPosition() const
	{
		return position;
	}

	float Component2D::GetRotation()
	{
		return rotation;
	}

	const glm::vec2& Component2D::GetScale() const
	{
		return scale;
	}

	const Color& Component2D::GetColor() const
	{
		return color;
	}

	SpriteModifiers& Component2D::GetMods()
	{
		sourceChanged = true;

		return mods;
	}

	void Component2D::SetColor(const Color& color)
	{
		this->color = color;

		colorChanged = true;
	}

	void Component2D::SetRotation(const float rotation)
	{
		this->rotation = rotation;

		positionChanged = true;
	}

	void Component2D::SetPosition(const glm::vec2& position)
	{
		this->position = position;

		positionChanged = true;
	}
}
