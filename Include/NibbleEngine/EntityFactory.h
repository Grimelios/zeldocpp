#pragma once
#include "Entity.h"

namespace Nibble
{
	// Scenes can load entities from files, but the specific entities created vary on a game-by-game basis. That's why
	// this class is abstract.
	class EntityFactory
	{
	public:

		virtual ~EntityFactory() = default;
		virtual std::unique_ptr<Entity> CreateEntity(int group, int type) = 0;
	};
}
