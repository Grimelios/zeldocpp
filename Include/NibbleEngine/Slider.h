#pragma once
#include "InputControl.h"
#include "IDraggable.h"
#include <memory>

namespace Nibble
{
	class Slider : public InputControl, public IDraggable
	{
	private:

		friend class InputRenderer<Slider>;

		int value = 0;

		std::unique_ptr<InputRenderer<Slider>> renderer;

	public:

		Slider();

		template<class T>
		T* CreateRenderer();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T>
	T* Slider::CreateRenderer()
	{
		renderer = std::make_unique<T>(*this);

		return static_cast<T*>(renderer.get());
	}
}
