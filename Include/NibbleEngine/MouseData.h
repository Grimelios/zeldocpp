#pragma once
#include <glm/vec2.hpp>
#include "InputData.h"
#include <glad/glad.h>
#include <glfw3.h>
#include <array>

namespace Nibble
{
	enum class MouseButtons
	{
		Left,
		Right,
		Middle
	};

	class MouseData : public InputData
	{
	private:

		using ButtonArray = std::array<InputStates, GLFW_MOUSE_BUTTON_LAST>;

		// For 2D games, this class would also contain the mouse's world position (based on a 2D camera), but that's
		// not required for a 3D game. Note that the mouse's screen position could still be raycast into the world,
		// but that likely won't be needed for this game.
		glm::vec2 position;
		glm::vec2 oldPosition;

		ButtonArray buttonArray;

	public:

		MouseData(const glm::vec2& position, const glm::vec2& oldPosition, const ButtonArray& buttonArray);

		glm::vec2 GetPosition() const;
		glm::vec2 GetOldPosition() const;

		InputStates GetState(int button) const;

		bool AnyButtonPressed() const override;
		bool Query(int data, InputStates state) const override;
	};
}
