#pragma once
#include <nlohmann/json.hpp>

namespace Nibble::JsonUtilities
{
	nlohmann::json Load(const std::string& filename);
}
