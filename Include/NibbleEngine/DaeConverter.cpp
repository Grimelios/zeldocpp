#include "DaeConverter.h"

namespace Nibble
{
	MeshConverter::MeshPair DaeConverter::ParseData(const std::string& path)
	{
		std::vector<glm::vec3> pointList;
		std::vector<glm::vec2> sourceList;
		std::vector<glm::vec3> normalList;
		std::vector<MeshVertex> vertexList;
		std::vector<IndexType> indexList;
		std::string texture;

		return MeshPair(Mesh(pointList, sourceList, normalList, nullptr, vertexList, indexList), texture);
	}
}
