#pragma once
#include <vector>
#include <functional>

namespace Nibble
{
	// Singular functions have an optional return value. That doesn't make sense for multi-events since you'd have to
	// somehow compose several return values.
	template<class... Args>
	class Event
	{
	private:

		using Func = std::function<void(Args...)>;

		std::vector<Func> functions;

	public:

		void Add(Func function);
		void Invoke(Args... args);
	};

	template<class... Args>
	void Event<Args...>::Add(Func function)
	{
		functions.push_back(std::move(function));
	}

	template<class... Args>
	void Event<Args...>::Invoke(Args... args)
	{
		for (auto& f : functions)
		{
			f(args...);
		}
	}
}
