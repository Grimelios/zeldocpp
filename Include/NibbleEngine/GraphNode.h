#pragma once
#include <vector>

namespace Nibble
{
	template<class T>
	class GraphNode
	{
	private:
		
		T data;
		std::vector<GraphNode<T>*> neighbors;

	public:

		explicit GraphNode(T data);

		T& GetData();
		const T& GetData() const;

		std::vector<GraphNode<T>*>& GetNeighbors();
		const std::vector<GraphNode<T>*>& GetNeighbors() const;
	};

	template<class T>
	GraphNode<T>::GraphNode(T data) : data(std::move(data))
	{
	}

	template<class T>
	T& GraphNode<T>::GetData()
	{
		return data;
	}

	template<class T>
	const T& GraphNode<T>::GetData() const
	{
		return data;
	}

	template<class T>
	std::vector<GraphNode<T>*>& GraphNode<T>::GetNeighbors()
	{
		return neighbors;
	}

	template<class T>
	const std::vector<GraphNode<T>*>& GraphNode<T>::GetNeighbors() const
	{
		return neighbors;
	}
}
