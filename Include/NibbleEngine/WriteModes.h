#pragma once

namespace Nibble
{
	enum class WriteModes
	{
		Append,
		Overwrite
	};
}
