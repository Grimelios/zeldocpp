#pragma once
#include "IDynamic.h"
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include "MessageHandle.h"
#include "CameraModes.h"

namespace Nibble
{
	class Entity;
	class Camera : public IDynamic
	{
	private:

		MessageHandle handle;

		glm::mat4 projection = glm::mat4(1);
		glm::mat4 viewProjection = glm::mat4(1);

		void OnResize();

	public:

		Camera();

		// In practice, it was easier to expose these values publicly rather than using getter/setter functions.
		glm::vec3 position = glm::vec3(0);
		glm::quat orientation = glm::quat(glm::vec3(0));

		// See the note in CameraModes regarding how camera behavior code is split among the engine and the game.
		CameraModes mode = CameraModes::Fixed;

		float fieldOfView = glm::half_pi<float>();
		float nearPlane = 0.1f;
		float farPlane = 1000.0f;

		Entity* target = nullptr;

		const glm::mat4& GetViewProjection() const;

		void Update(float dt) override;
	};
}
