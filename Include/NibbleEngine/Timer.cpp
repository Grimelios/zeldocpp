#include "Timer.h"

namespace Nibble
{
	Timer::Timer(const int duration, const float elapsed) :

		// Durations are passed in milliseconds.
		duration(duration / 1000.0f),
		elapsed(elapsed)
	{
	}

	bool Timer::HasCompleted() const
	{
		return completed;
	}
}
