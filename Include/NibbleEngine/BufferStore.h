#pragma once
#include <glad/glad.h>

namespace Nibble
{
	class BufferStore
	{
	protected:

		using IndexType = unsigned short;

		GLuint bufferId = 0;
		GLuint indexBufferId = 0;

	public:

		BufferStore(int bufferCapacity, int indexCapacity, GLenum usage);

		virtual ~BufferStore() = 0;

		GLuint GetBufferId() const;
		GLuint GetIndexBufferId() const;
	};

	inline BufferStore::~BufferStore() = default;
}
