#include "AggregateData.h"

namespace Nibble
{
	AggregateData::AggregateData()
	{
		for (int i = 0; i < TypeCount; i++)
		{
			dataArray[i] = nullptr;
		}
	}

	void AggregateData::SetData(InputTypes type, InputData& data)
	{
		dataArray[static_cast<int>(type)] = &data;
	}

	bool AggregateData::AnyButtonPressed() const
	{
		for (InputData* data : dataArray)
		{
			if (data != nullptr && data->AnyButtonPressed())
			{
				return true;
			}
		}

		return false;
	}

	bool AggregateData::Query(const InputBind& bind, const InputStates state) const
	{
		return dataArray[static_cast<int>(bind.type)]->Query(bind.data, state);
	}

	bool AggregateData::Query(const std::vector<InputBind>& binds, const InputStates state) const
	{
		for (const InputBind& bind : binds)
		{
			if (Query(bind, state))
			{
				return true;
			}
		}

		return false;
	}

	const InputBind* AggregateData::QueryBind(const std::vector<InputBind>& binds,
		const InputStates state) const
	{
		for (const InputBind& bind : binds)
		{
			if (Query(bind, state))
			{
				return &bind;
			}
		}

		return nullptr;
	}
}
