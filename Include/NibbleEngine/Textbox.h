#pragma once
#include <string>
#include <optional>
#include <array>
#include "DoublyLinkedList.h"
#include "EventSingular.h"
#include "InputControl.h"

namespace Nibble
{
	class KeyboardData;
	class Textbox : public InputControl
	{
	private:

		friend class InputRenderer<Textbox>;

		static const std::array<char, 10> NumericSpecials;

		static std::optional<char> GetCharacter(int key, bool shift);

		// A linked list is used because the textbox's string value is frequently modified (based on cursor position).
		DoublyLinkedList<char> value;
		DoublyLinkedListNode<char>* cursorNode = nullptr;

		int cursorPosition = 0;

		// Given that renderers use a template parameter, there's unfortunately not a good way to genericize renderer
		// creation in the base class. Renderers are still returned publicly (in the creation function below) so that
		// external classes in the main game can customize renderers as needed.
		std::unique_ptr<InputRenderer<Textbox>> renderer;

		void ProcessKeyboard(const KeyboardData& data);

		std::string GetValueString() const;

	public:

		Textbox();

		EventSingular<void, const std::string&> onSubmit;
		EventSingular<bool, const std::string&> onValidate;

		template<class T>
		T* CreateRenderer();

		// These functions are public so that external keyboard display classes can modify the textbox (such as when
		// the player is using a controller).
		void Backspace();
		void Delete();
		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T>
	T* Textbox::CreateRenderer()
	{
		renderer = std::make_unique<T>(*this);

		return static_cast<T*>(renderer.get());
	}
}
