#pragma once
#include <optional>
#include <string>
#include <vector>
#include "Component2D.h"

namespace Nibble
{
	class SpriteFont;
	class SpriteText : public Component2D
	{
	private:

		const SpriteFont* font;

		std::vector<float> buffer;
		std::optional<std::string> value = std::nullopt;

		unsigned renderCount = 0;

	public:

		explicit SpriteText(Alignments alignment = Alignments::Left | Alignments::Top);
		explicit SpriteText(const std::string& font, Alignments alignment = Alignments::Left | Alignments::Top);
		explicit SpriteText(const SpriteFont& font, Alignments alignment = Alignments::Left | Alignments::Top);

		SpriteText(const std::string& font, const std::string& value,
			Alignments alignment = Alignments::Left | Alignments::Top);
		SpriteText(const SpriteFont& font, const std::string& value,
			Alignments alignment = Alignments::Left | Alignments::Top);

		const std::optional<std::string>& GetValue() const;
		const SpriteFont& GetFont() const;

		void Clear();
		void SetFont(const SpriteFont& font);
		void SetValue(const std::string& value);
		void Draw(SpriteBatch& sb) override;
	};
}
