#pragma once
#include <vector>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace Nibble
{
	class Texture2D;
	class Mesh
	{
	private:

		friend class MeshRenderer;
		friend class MeshUtilities;

		using IndexType = unsigned short;
		using MeshVertex = std::tuple<IndexType, IndexType, IndexType>;

		static std::vector<glm::vec2> ParseVec2List(const std::string& line);
		static std::vector<glm::vec3> ParseVec3List(const std::string& line);

		std::vector<glm::vec3> pointList;
		std::vector<glm::vec2> sourceList;
		std::vector<glm::vec3> normalList;
		std::vector<MeshVertex> vertexList;
		std::vector<IndexType> indexList;

		// For the time being, it's assumed meshes will use at most one texture.
		const Texture2D* texture = nullptr;

	public:

		static Mesh Load(const std::string& filename);

		Mesh(std::vector<glm::vec3>& pointList, std::vector<glm::vec2>& sourceList, std::vector<glm::vec3>& normalList,
			const Texture2D* texture, std::vector<MeshVertex>& vertexList, std::vector<IndexType>& indexList);

		const std::vector<glm::vec3>& GetPointList() const;
		const std::vector<glm::vec2>& GetSourceList() const;
		const std::vector<glm::vec3>& GetNormalList() const;
		const std::vector<MeshVertex>& GetVertexList() const;
		const std::vector<IndexType>& GetIndexList() const;

		const Texture2D* GetTexture() const;
	};
}
