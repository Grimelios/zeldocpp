#include "VectorUtilities.h"
#include "MessageSystem.h"
#include "MessageHandle.h"

namespace Nibble
{
	MessageSystem::ReceiverArray MessageSystem::receiverArray;
	MessageSystem::IndexArray MessageSystem::indexArray;

	void MessageSystem::Initialize()
	{
		for (int i = 0; i < TypeCount; i++)
		{
			indexArray[i] = 0;
		}
	}

	void MessageSystem::Subscribe(const MessageTypes messageType, const ReceiverFunction& action)
	{
		Subscribe(messageType, nullptr, action);
	}

	void MessageSystem::Subscribe(const MessageTypes messageType, MessageHandle& handle, const ReceiverFunction& action)
	{
		Subscribe(messageType, &handle, action);
	}

	void MessageSystem::Subscribe(MessageTypes messageType, MessageHandle* handle, const ReceiverFunction& action)
	{
		const int typeIndex = static_cast<int>(messageType);
		int positionIndex = indexArray[typeIndex];

		ReceiverVector& v = receiverArray[typeIndex];

		if (positionIndex == v.size())
		{
			v.emplace_back(action);
			indexArray[typeIndex]++;
		}
		else
		{
			v[positionIndex] = action;

			do
			{
				positionIndex++;
			}
			while (positionIndex < static_cast<int>(v.size()) && v[positionIndex].has_value());

			indexArray[typeIndex] = positionIndex;
		}

		// Handle can be null for static classes that will never be unsubscribed.
		if (handle != nullptr)
		{
			handle->messageType = messageType;
			handle->index = indexArray[typeIndex];
		}
	}

	void MessageSystem::Unsubscribe(const MessageTypes messageType, const int index)
	{
		const int typeIndex = static_cast<int>(messageType);
		const int positionIndex = indexArray[typeIndex];

		VectorUtilities::RemoveAt(receiverArray[typeIndex], index);

		indexArray[typeIndex] = std::min(positionIndex, index);
	}

	void MessageSystem::Send(MessageTypes messageType, const std::any& data, const float dt)
	{
		const ReceiverVector& v = receiverArray[static_cast<int>(messageType)];

		for (const auto& r : v)
		{
			if (r.has_value())
			{
				r.value()(data, dt);
			}
		}
	}

	void MessageSystem::ProcessChanges()
	{
	}
}
