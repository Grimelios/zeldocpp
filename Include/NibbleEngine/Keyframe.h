#pragma once
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class Keyframe
	{
	private:

		glm::vec3 position;
		glm::quat orientation;

		float time;

	public:

		Keyframe(const glm::vec3& position, const glm::quat& orientation, float time);
	};
}
