#pragma once

namespace Nibble
{
	class Space;
	class Sensor;
	class SensorHandle
	{
	private:

	public:

		~SensorHandle();

		Space* space = nullptr;
		Sensor* sensor = nullptr;
	};
}
