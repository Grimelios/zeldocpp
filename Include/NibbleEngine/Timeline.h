#pragma once
#include <vector>
#include "Keyframe.h"

namespace Nibble
{
	class Timeline
	{
	private:

		std::vector<Keyframe> keyframes;

	public:

		explicit Timeline(std::vector<Keyframe> keyframes);
	};
}
