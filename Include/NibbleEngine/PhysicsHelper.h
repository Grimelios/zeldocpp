#pragma once

namespace Nibble
{
	class World;
	class Sphere;
	class RigidBody3D;
	class PhysicsHelper
	{
	protected:

		World& world;

		explicit PhysicsHelper(World& world);

		virtual void ProcessBody(RigidBody3D& body) = 0;

	public:

		virtual ~PhysicsHelper() = default;
	};
}
