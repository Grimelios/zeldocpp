#include "MeshRenderer.h"
#include "Camera.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Model.h"
#include "Texture2D.h"
#include "RenderHandle.h"
#include "MapUtilities.h"
#include "ContentCache.h"

namespace Nibble
{
	MeshRenderer::MeshRenderer() :
		storage(80000, 12000),
		shadowTarget(ShadowMapSize, ShadowMapSize, RenderTargetFlags::Depth),
		greyTexture(ContentCache::GetTexture("Untextured.png")),
		lightDirection(normalize(glm::vec3(-1, -0.15f, 0.5f)))
	{
		const GLuint bufferId = storage.GetBufferId();
		const GLuint indexBufferId = storage.GetIndexBufferId();

		shadowShader.Attach(ShaderTypes::Vertex, "ShadowMap.vert");
		shadowShader.Attach(ShaderTypes::Fragment, "ShadowMap.frag");
		shadowShader.AddAttribute<float>(3, GL_FLOAT, false, 5);
		shadowShader.CreateProgram();
		shadowShader.CompleteBinding(bufferId, indexBufferId);

		modelShader.Attach(ShaderTypes::Vertex, "ModelShadow.vert");
		modelShader.Attach(ShaderTypes::Fragment, "ModelShadow.frag");
		modelShader.AddAttribute<float>(3, GL_FLOAT, false);
		modelShader.AddAttribute<float>(2, GL_FLOAT, false);
		modelShader.AddAttribute<float>(3, GL_FLOAT, false);
		modelShader.CreateProgram();
		modelShader.CompleteBinding(bufferId, indexBufferId);

		// Setting sampler uniforms likely isn't necessary (since it's implied by ordering within the shader file), but
		// it's safer to set them anyway.
		modelShader.Use();
		modelShader.SetUniform("shadowSampler", 0);
		modelShader.SetUniform("textureSampler", 1);
	}

	const RenderTarget& MeshRenderer::GetTarget() const
	{
		return shadowTarget;
	}

	void MeshRenderer::AddModel(Model& model)
	{
		AddModel(model, nullptr);
	}

	void MeshRenderer::AddModel(Model& model, RenderHandle& handle)
	{
		AddModel(model, &handle);
	}

	void MeshRenderer::AddModel(Model& model, RenderHandle* handle)
	{
		auto* node = renderList.GetHead();

		while (node != nullptr)
		{
			auto& pair = node->data;

			if (&model == pair.first)
			{
				pair.second.instanceCount++;

				return;
			}

			node = node->next;
		}

		const Mesh& mesh = model.GetMesh();

		const auto& pointList = mesh.pointList;
		const auto& sourceList = mesh.sourceList;
		const auto& normalList = mesh.normalList;
		const auto& vertexList = mesh.vertexList;

		const int indexCount = static_cast<int>(mesh.indexList.size());
		const int vertexCount = static_cast<int>(vertexList.size());

		// Each vertex stores position, texture coordinates, and normal.
		std::vector<float> buffer;
		buffer.reserve(vertexCount * 8);

		for (const auto& vertex : vertexList)
		{
			const auto& p = pointList[std::get<0>(vertex)];
			const auto& s = sourceList[std::get<1>(vertex)];
			const auto& n = normalList[std::get<2>(vertex)];

			// Vertex data is ordered position, source, then normal. It's assumed that every vertex will have texture
			// coordinates attached.
			buffer.push_back(p.x);
			buffer.push_back(p.y);
			buffer.push_back(p.z);
			buffer.push_back(s.x);
			buffer.push_back(s.y);
			buffer.push_back(n.x);
			buffer.push_back(n.y);
			buffer.push_back(n.z);
		}

		storage.Append(buffer);
		storage.AppendIndex(mesh.indexList);

		RenderContainer* container = &renderList.Add(&model, RenderContainer(vertexCount, indexCount))->second;

		// Using a null handle is useful for adding debug models to the scene.
		if (handle != nullptr)
		{
			handle->container = container;
		}
	}

	void MeshRenderer::DrawTarget(const Camera& camera)
	{
		constexpr float orthoSize = 40.0f;
		constexpr float nearPlane = 0.1f;
		constexpr float farPlane = 150.0f;

		const glm::mat4 lightView = lookAt(-lightDirection * farPlane / 2.0f, glm::vec3(0), glm::vec3(0, 1, 0));
		const glm::mat4 lightProjection = glm::ortho(-orthoSize, orthoSize, -orthoSize, orthoSize, nearPlane, farPlane);
		
		lightVp = lightProjection * lightView;

		// Front-face culling for shadow maps works as long as every object 1) has some depth, and 2) has no gaps in
		// the mesh. The exception is the edges of the static world mesh, which (in theory) should never be viewed from
		// the wrong side.
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		shadowTarget.Apply();
		shadowShader.Apply();

		int indexOffset = 0;
		int baseVertex = 0;

		auto* node = renderList.GetHead();

		while (node != nullptr)
		{
			auto& pair = node->data;
			auto& container = pair.second;

			if (container.instanceCount > 0)
			{
				Model* model = pair.first;

				shadowShader.SetUniform("lightMatrix", lightVp * model->GetWorldMatrix());

				DrawMesh(model->GetMesh(), indexOffset, baseVertex);
			}

			indexOffset += container.indexCount;
			baseVertex += container.vertexCount;
			node = node->next;
		}
	}

	void MeshRenderer::Draw(const Camera& camera)
	{
		glCullFace(GL_BACK);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, shadowTarget.GetTextureId());

		// This sets up texture binding in the loop below.
		glActiveTexture(GL_TEXTURE1);

		if (!texturesEnabled)
		{
			glBindTexture(GL_TEXTURE_2D, greyTexture.GetTextureId());
		}

		modelShader.Apply();
		modelShader.SetUniform("lightColor", glm::vec3(1));
		modelShader.SetUniform("lightDirection", lightDirection);
		modelShader.SetUniform("ambientIntensity", 0.1f);

		// See http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-16-shadow-mapping/.
		const glm::mat4 biasMatrix
		(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
		);

		int indexOffset = 0;
		int baseVertex = 0;

		const glm::mat4& cameraVp = camera.GetViewProjection();

		auto* node = renderList.GetHead();

		while (node != nullptr)
		{
			auto& pair = node->data;
			auto& container = pair.second;

			// TODO: Draw instanced meshes when appropriate.
			if (container.instanceCount > 0)
			{
				Model* model = pair.first;

				const glm::mat4& world = model->GetWorldMatrix();
				const glm::quat& orientation = model->GetOrientation();

				modelShader.SetUniform("orientation", mat4_cast(orientation));
				modelShader.SetUniform("mvp", cameraVp * world);
				modelShader.SetUniform("lightBiasMatrix", biasMatrix * lightVp * world);

				const auto& mesh = model->GetMesh();

				// This assumes the mesh has a valid texture attached (even if that texture is the default grey
				// texture).
				if (texturesEnabled)
				{
					glBindTexture(GL_TEXTURE_2D, mesh.texture->GetTextureId());
				}

				DrawMesh(mesh, indexOffset, baseVertex);
			}

			indexOffset += container.indexCount;
			baseVertex += container.vertexCount;
			node = node->next;
		}
	}

	void MeshRenderer::DrawMesh(const Mesh& mesh, int& indexOffset, int& baseVertex)
	{
		const int indexCount = static_cast<int>(mesh.indexList.size());

		const void* offset = reinterpret_cast<const void*>(sizeof(IndexType) * indexOffset);

		glDrawElementsBaseVertex(GL_TRIANGLES, indexCount, GL_UNSIGNED_SHORT, offset, baseVertex);
	}
}
