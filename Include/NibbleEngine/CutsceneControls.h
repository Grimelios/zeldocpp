#pragma once
#include <vector>
#include "InputBind.h"
#include "ControlClass.h"

namespace Nibble
{
	class CutsceneControls : public ControlClass
	{
	public:

		std::vector<InputBind> pause;
		std::vector<InputBind> skip;

		void Refresh(const Json& j) override;
	};
}
