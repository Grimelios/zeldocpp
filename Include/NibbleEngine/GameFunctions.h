#pragma once
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <string>
#include <vector>
#include "Alignments.h"
#include "Rectangle.h"

namespace Nibble
{
	class Bounds2D;
	class SpriteFont;

	namespace GameFunctions
	{
		int ComputeSign(float value);

		float ComputeAngle(const glm::vec2& value);
		float ComputeAngle(const glm::vec2& start, const glm::vec2& end);
		float ComputeAngle(const glm::vec3& start, const glm::vec3& end);
		float ComputeDistanceToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p, glm::vec2& result);
		float ComputeDistanceSquaredToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
			glm::vec2& result);

		float ComputeLengthSquared(const glm::vec2& value);
		float ComputeLengthSquared(const glm::vec3& value);
		float ComputeDistanceSquared(const glm::vec2& p1, const glm::vec2& p2);
		float ComputeDistanceSquared(const glm::vec3& p1, const glm::vec3& p2);

		bool IsInteger(const std::string& s);
		bool SolveQuadratic(float a, float b, float c, float maxRoot, float& root);

		std::string ToString(const glm::vec2& v);
		std::string ToString(const glm::vec3& v);
		std::string ToString(const Bounds2D& bounds);
		std::vector<std::string> WrapLines(const std::string& value, const SpriteFont& font, int width);

		glm::vec2 ParseVec2(const std::string& s);
		glm::vec3 ParseVec3(const std::string& s);
		glm::quat ParseQuat(const std::string& s);

		Rectangle ParseRect(const std::string& s);

		glm::vec2 ComputeDirection(float angle);
		glm::vec2 ComputeOrigin(int width, int height, Alignments alignment);

		glm::vec3 ReflectVector(const glm::vec3& v, const glm::vec3& normal);
	}
}
