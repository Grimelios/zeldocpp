#include "Entity.h"
#include "Shape3D.h"
#include "Scene.h"
#include "World.h"
#include "Space.h"
#include "RigidBody3D.h"
#include "GameFunctions.h"

namespace Nibble
{
	Entity::Entity(const int entityGroup) : entityGroup(entityGroup)
	{
	}

	void Entity::Initialize(Scene* scene, const Json* j)
	{
		// All entities will likely have position, but it's safer to verify anyway. Fewer will have orientation defined
		// (since default orientation is pretty common).
		const auto p = j->find("Position");
		const auto o = j->find("Orientation");

		const glm::vec3 pValue = p != j->end()
			? GameFunctions::ParseVec3(p->get<std::string>())
			: glm::vec3(0);

		const glm::quat oValue = o != j->end()
			? GameFunctions::ParseQuat(o->get<std::string>())
			: glm::quat(glm::vec3(0));

		SetTransform(pValue, oValue);
	}

	Sensor* Entity::CreateSensor(SensorHandle& handle, Shape3D& shape)
	{
		// This assumes the scene has been correctly set before Initialize is called.
		return scene->space->CreateSensor(this, SensorTypes::Entity, shape, handle);
	}

	RigidBody3D* Entity::CreateBody(RigidBodyHandle& handle, const BodyTypes bodyType, Shape3D& shape,
		const float density, const bool useCollisionCallback, const bool useSeparationCallback)
	{
		// See the note above about the scene pointer being set.
		auto* body = scene->world->CreateBody(handle, bodyType, this, shape, density);

		if (useCollisionCallback)
		{
			body->onCollision = [this](CollisionTypes collisionType, void* target, const Manifold& manifold)
			{
				return OnCollision(collisionType, target, manifold);
			};
		}

		if (useSeparationCallback)
		{
			body->onSeparation = [this](Entity& entity)
			{
				OnSeparation(entity);
			};
		}

		return body;
	}

	void Entity::Attach(ITransformable* item)
	{
		Attach(item, glm::vec3(0), glm::quat(glm::vec3(0)));
	}

	void Entity::Attach(ITransformable* item, const glm::vec3& position)
	{
		Attach(item, position, glm::quat(glm::vec3(0)));
	}

	void Entity::Attach(ITransformable* item, const glm::vec3& position, const glm::quat& orientation)
	{
		attachments.emplace_back(item, position, orientation);

		Model* model = dynamic_cast<Model*>(item);

		if (model != nullptr)
		{
			scene->meshRenderer->AddModel(*model);
		}
	}

	bool Entity::OnCollision(const CollisionTypes collisionType, void* target, const Manifold& manifold)
	{
		return true;
	}

	void Entity::OnSeparation(Entity& entity)
	{
	}

	const glm::vec3& Entity::GetPosition() const
	{
		return position;
	}

	const glm::quat& Entity::GetOrientation() const
	{
		return orientation;
	}

	int Entity::GetEntityGroup() const
	{
		return entityGroup;
	}

	void Entity::SetPosition(const glm::vec3& position)
	{
		this->position = position;

		UpdateAttachments();
	}

	void Entity::SetOrientation(const glm::quat& orientation)
	{
		this->orientation = orientation;

		UpdateAttachments();
	}

	void Entity::SetTransform(const glm::vec3& position, const glm::quat& orientation)
	{
		this->position = position;
		this->orientation = orientation;

		UpdateAttachments();
	}

	void Entity::SetTransformFromBody(const glm::vec3& position, const glm::quat& orientation)
	{
		this->position = position;
		this->orientation = orientation;

		// It's assumed that the master physics body is attachment zero.
		for (int i = 1; i < static_cast<int>(attachments.size()); i++)
		{
			auto& a = attachments[i];

			const glm::vec3 p = position + std::get<1>(a);
			const glm::quat o = orientation * std::get<2>(a);

			std::get<0>(a)->SetTransform(p, o);
		}
	}

	void Entity::UpdateAttachments()
	{
		for (auto& a : attachments)
		{
			const glm::vec3 p = position + std::get<1>(a);
			const glm::quat o = orientation * std::get<2>(a);

			// Note that attached entities don't have local transforms. I don't think this will be a problem in
			// practice.
			std::get<0>(a)->SetTransform(p, o);
		}
	}

	void Entity::Update(const float dt)
	{
	}
}
