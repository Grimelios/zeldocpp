#pragma once
#include "RenderTargetFlags.h"
#include <glad/glad.h>
#include "QuadSource.h"

namespace Nibble
{
	class RenderTarget : public QuadSource
	{
	private:

		GLuint frameBuffer = 0;
		GLuint renderBuffer = 0;

		bool colorEnabled = false;
		bool depthEnabled = false;
		bool stencilEnabled = false;

	public:

		RenderTarget() = default;
		RenderTarget(int width, int height, RenderTargetFlags flags);
		~RenderTarget();

		void Initialize(int width, int height, RenderTargetFlags flags);
		void Apply(bool clear = true) const;

		GLuint GetFrameBuffer() const;
		GLuint GetRenderBuffer() const;

		bool IsColorEnabled() const;
		bool IsDepthEnabled() const;
		bool IsStencilEnabled() const;

		void Use() const;
	};
}
