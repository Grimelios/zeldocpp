#pragma once
#include "InputData.h"
#include <vector>
#include <array>
#include "KeyPress.h"
#include <glad/glad.h>
#include <glfw3.h>

namespace Nibble
{
	class KeyboardData : public InputData
	{
	private:

		using KeyArray = std::array<InputStates, GLFW_KEY_LAST>;

		// In practice, full key presses (including key modifiers) are only required for keys pressed this frame.
		std::vector<int> keysHeld;
		std::vector<KeyPress> keysPressedThisFrame;
		std::vector<int> keysReleasedThisFrame;

		// The key array must be copied to properly process keys pressed or released this frame.
		KeyArray keyArray;

	public:

		KeyboardData(std::vector<int> keysHeld, std::vector<KeyPress> keysPressedThisFrame,
					std::vector<int> keysReleasedThisFrame, const KeyArray& keyArray);

		const std::vector<int>& GetKeysHeld() const;
		const std::vector<KeyPress>& GetKeysPressedThisFrame() const;
		const std::vector<int>& GetKeysReleasedThisFrame() const;

		bool AnyButtonPressed() const override;
		bool Query(int data, InputStates state) const override;
		bool QueryPress(int key, KeyModifiers mods) const;
	};
}
