#pragma once
#include "Shape3D.h"

namespace Nibble
{
	class Box : public Shape3D
	{
	public:

		Box();
		explicit Box(float size);
		Box(float width, float height, float depth);

		float width;
		float height;
		float depth;

		void SetDimensions(const glm::vec3& v);

		float ComputeVolume() const override;
	};
}
