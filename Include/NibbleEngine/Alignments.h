#pragma once

namespace Nibble
{
	enum class Alignments
	{
		Center = 0,
		Left = 1,
		Right = 2,
		Top = 4,
		Bottom = 8
	};

	inline Alignments operator&(Alignments a1, Alignments a2)
	{
		return static_cast<Alignments>(static_cast<int>(a1) & static_cast<int>(a2));
	}

	inline Alignments operator|(Alignments a1, Alignments a2)
	{
		return static_cast<Alignments>(static_cast<int>(a1) | static_cast<int>(a2));
	}
}
