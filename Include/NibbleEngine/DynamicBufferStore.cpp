#include "DynamicBufferStore.h"

namespace Nibble
{
	DynamicBufferStore::DynamicBufferStore(const int bufferCapacity, const int indexCapacity) :
		BufferStore(bufferCapacity, indexCapacity, GL_DYNAMIC_DRAW)
	{
		buffer.reserve(bufferCapacity);
		indexBuffer.reserve(indexCapacity);
	}

	bool DynamicBufferStore::IsEmpty() const
	{
		return buffer.empty();
	}

	std::vector<float>& DynamicBufferStore::GetBuffer()
	{
		return buffer;
	}

	std::vector<BufferStore::IndexType>& DynamicBufferStore::GetIndexBuffer()
	{
		return indexBuffer;
	}

	int DynamicBufferStore::Flush(const bool clearBuffer, const bool clearIndices)
	{
		if (buffer.empty())
		{
			return 0;
		}

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * buffer.size(), &buffer[0]);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(IndexType) * indexBuffer.size(), &indexBuffer[0]);

		// In some cases (such as fluids), it's more efficient to not clear the buffer each frame in favor of directly
		// modifying individual components.
		if (clearBuffer)
		{
			buffer.clear();
		}

		const int indexCount = static_cast<int>(indexBuffer.size());

		if (clearIndices)
		{
			indexBuffer.clear();
		}

		// Returning the index count allows external classes to render their data.
		return indexCount;
	}
}
