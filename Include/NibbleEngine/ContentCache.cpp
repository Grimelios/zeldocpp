#include "ContentCache.h"
#include "MapUtilities.h"

namespace Nibble
{
	ContentCache::AtlasCache ContentCache::atlasCache;
	ContentCache::FontCache ContentCache::fontCache;
	ContentCache::MeshCache ContentCache::meshCache;
	ContentCache::TextureCache ContentCache::textureCache;

	const Atlas& ContentCache::GetAtlas(const std::string& filename)
	{
		Atlas* atlas;

		if (MapUtilities::HasValue(atlasCache, filename, atlas))
		{
			return *atlas;
		}

		return MapUtilities::Add(atlasCache, filename, Atlas::Load(filename));
	}

	const Mesh& ContentCache::GetMesh(const std::string& filename)
	{
		Mesh* mesh;

		if (MapUtilities::HasValue(meshCache, filename, mesh))
		{
			return *mesh;
		}

		return MapUtilities::Add(meshCache, filename, Mesh::Load(filename));
	}

	const SpriteFont& ContentCache::GetFont(const std::string& name)
	{
		const auto i = fontCache.find(name);

		if (i != fontCache.end())
		{
			return i->second;
		}

		return MapUtilities::Add(fontCache, name, SpriteFont::Load(name));
	}

	const Texture2D& ContentCache::GetTexture(const std::string& filename, const std::string& folder)
	{
		const auto i = textureCache.find(filename);

		if (i != textureCache.end())
		{
			return i->second;
		}

		return MapUtilities::Add(textureCache, filename, Texture2D::Load(folder + filename));
	}
}
