#include "GameFunctions.h"
#include "Bounds2D.h"
#include "StringUtilities.h"
#include "SpriteFont.h"

namespace Nibble
{
	int GameFunctions::ComputeSign(const float value)
	{
		// See https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c.
		return (0 < value) - (value < 0);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& value)
	{
		return ComputeAngle(glm::vec2(0), value);
	}

	float GameFunctions::ComputeAngle(const glm::vec2& start, const glm::vec2& end)
	{
		const float dX = end.x - start.x;
		const float dY = end.y - start.y;

		return std::atan2(dY, dX);
	}

	float GameFunctions::ComputeAngle(const glm::vec3& start, const glm::vec3& end)
	{
		// See https://stackoverflow.com/questions/43749543/c-calculate-angle-0-to-360-between-two-3d-vectors and
		// http://www.analyzemath.com/stepbystep_mathworksheets/vectors/vector3D_angle.html.
		const float a = dot(start, end);
		const float b = length(start);
		const float c = length(end);

		// Note that this should return an angle between 0 and 180 (0 and pi in radians).
		return std::acos(a / (b * c));
	}

	float GameFunctions::ComputeDistanceToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
		glm::vec2& result)
	{
		return std::sqrt(ComputeDistanceSquaredToLine(l1, l2, p, result));
	}

	float GameFunctions::ComputeDistanceSquaredToLine(const glm::vec2& l1, const glm::vec2& l2, const glm::vec2& p,
		glm::vec2& result)
	{
		// See https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment.
		const float squared = ComputeDistanceSquared(l1, l2);

		if (squared == 0.0)
		{
			result = l1;

			return ComputeDistanceSquared(p, l1);
		}

		const float t = std::max(0.0f, std::min(1.0f, dot(p - l1, l2 - l1) / squared));

		result = l1 + t * (l2 - l1);

		return ComputeDistanceSquared(p, result);
	}

	float GameFunctions::ComputeLengthSquared(const glm::vec2& value)
	{
		return value.x * value.x + value.y * value.y;
	}

	float GameFunctions::ComputeLengthSquared(const glm::vec3& value)
	{
		return value.x * value.x + value.y * value.y + value.z * value.z;
	}

	float GameFunctions::ComputeDistanceSquared(const glm::vec2& p1, const glm::vec2& p2)
	{
		return ComputeLengthSquared(p2 - p1);
	}

	float GameFunctions::ComputeDistanceSquared(const glm::vec3& p1, const glm::vec3& p2)
	{
		return ComputeLengthSquared(p2 - p1);
	}

	bool GameFunctions::IsInteger(const std::string& s)
	{
		// See https://stackoverflow.com/questions/2844817/how-do-i-check-if-a-c-string-is-an-int.
		if (s.empty() || (!isdigit(s[0]) && s[0] != '-' && s[0] != '+'))
		{
			return false;
		}

		char* p;
		strtol(s.c_str(), &p, 10);

		return *p == 0;
	}

	bool GameFunctions::SolveQuadratic(const float a, const float b, const float c, const float maxRoot, float& root)
	{
		// See http://www.peroxide.dk/papers/collision/collision.pdf, Appendix D.
		const float determinant = b * b - 4.0f * a * c;

		// If the determinant is negative, there are no solutions.
		if (determinant < 0.0f)
		{
			return false;
		}

		// If the determinant is zero, then x1 == x2, but that small optimization is ignored for now.
		const float sqrtD = sqrt(determinant);
		
		float r1 = (-b - sqrtD) / (2 * a);
		float r2 = (-b + sqrtD) / (2 * a);

		// Sort so r1 < r2.
		if (r1 > r2)
		{
			std::swap(r1, r2);
		}

		if (r1 > 0 && r1 < maxRoot)
		{
			root = r1;

			return true;
		}

		if (r2 > 0 && r2 < maxRoot)
		{
			root = r2;

			return true;
		}

		// No valid solutions.
		return false;
	}

	std::string GameFunctions::ToString(const glm::vec2& v)
	{
		return std::to_string(v.x) + "," + std::to_string(v.y);
	}

	std::string GameFunctions::ToString(const glm::vec3& v)
	{
		return std::to_string(v.x) + "," + std::to_string(v.y) + "," + std::to_string(v.z);
	}

	std::string GameFunctions::ToString(const Bounds2D& bounds)
	{
		return std::to_string(bounds.x) + ", " + std::to_string(bounds.y) + ", " + std::to_string(bounds.width) +
			", " + std::to_string(bounds.height);
	}

	std::vector<std::string> GameFunctions::WrapLines(const std::string& value, const SpriteFont& font,
		const int width)
	{
		std::vector<std::string> lines;
		std::vector<std::string> words = StringUtilities::Split(value, ' ');
		std::string currentLine;

		int currentWidth = 0;

		const int spaceWidth = font.Measure(" ").x;

		for (const std::string& word : words)
		{
			const int wordWidth = font.Measure(word).x;

			if (currentWidth + wordWidth > width)
			{
				lines.push_back(std::move(currentLine));
				currentLine = word + " ";
				currentWidth = wordWidth + spaceWidth;
			}
			else
			{
				currentWidth += wordWidth + spaceWidth;
				currentLine += word + " ";
			}
		}

		if (!currentLine.empty())
		{
			lines.push_back(std::move(currentLine));
		}

		return lines;
	}

	glm::vec2 GameFunctions::ParseVec2(const std::string& s)
	{
		const int index = StringUtilities::IndexOf(s, ',');
		const float x = std::stof(s.substr(0, index));
		const float y = std::stof(s.substr(index + 1, s.size() - index - 1));

		return glm::vec2(x, y);
	}

	glm::vec3 GameFunctions::ParseVec3(const std::string& s)
	{
		const auto tokens = StringUtilities::Split(s, ',');

		const float x = std::stof(tokens[0]);
		const float y = std::stof(tokens[1]);
		const float z = std::stof(tokens[2]);

		return glm::vec3(x, y, z);
	}

	glm::quat GameFunctions::ParseQuat(const std::string& s)
	{
		const auto tokens = StringUtilities::Split(s, ',');

		const float x = std::stof(tokens[0]);
		const float y = std::stof(tokens[1]);
		const float z = std::stof(tokens[2]);

		return glm::quat(glm::vec3(x, y, z));
	}

	Rectangle GameFunctions::ParseRect(const std::string& s)
	{
		const auto tokens = StringUtilities::Split(s, ',');

		const int x = std::stoi(tokens[0]);
		const int y = std::stoi(tokens[1]);
		const int w = std::stoi(tokens[2]);
		const int h = std::stoi(tokens[3]);

		return Rectangle(x, y, w, h);
	}

	glm::vec2 GameFunctions::ComputeDirection(const float angle)
	{
		const float x = std::cos(angle);
		const float y = std::sin(angle);

		return glm::vec2(x, y);
	}

	glm::vec2 GameFunctions::ComputeOrigin(const int width, const int height, Alignments alignment)
	{
		const int alignmentValue = static_cast<int>(alignment);

		const bool left = (alignmentValue & static_cast<int>(Alignments::Left)) > 0;
		const bool right = (alignmentValue & static_cast<int>(Alignments::Right)) > 0;
		const bool top = (alignmentValue & static_cast<int>(Alignments::Top)) > 0;
		const bool bottom = (alignmentValue & static_cast<int>(Alignments::Bottom)) > 0;

		const float x = static_cast<float>(left ? 0 : (right ? width : width / 2));
		const float y = static_cast<float>(top ? 0 : (bottom ? height : height / 2));

		return glm::vec2(x, y);
	}

	glm::vec3 GameFunctions::ReflectVector(const glm::vec3& v, const glm::vec3& normal)
	{
		// See https://math.stackexchange.com/a/13263.
		return v - 2 * dot(v, normal) * normal;
	}
}
