#pragma once

namespace Nibble
{
	class IRotatable
	{
	public:

		virtual ~IRotatable() = default;

		virtual float GetRotation() = 0;
		virtual void SetRotation(float rotation) = 0;
	};
}
