#pragma once

namespace Nibble
{
	class Camera;
	class RenderTarget;
	class IRenderTargetUser
	{
	public:

		virtual ~IRenderTargetUser() = default;
		virtual const RenderTarget& GetTarget() const = 0;
		virtual void DrawTarget(const Camera& camera) = 0;
	};
}
