#include "Scene.h"
#include "Resolution.h"
#include "World.h"

namespace Nibble
{
	Scene::Scene() :
		renderTarget(Resolution::Width, Resolution::Height, RenderTargetFlags::Color | RenderTargetFlags::Depth)
	{
	}

	const RenderTarget& Scene::GetTarget() const
	{
		return renderTarget;
	}

	void Scene::LoadFragment(const std::string& filename)
	{
	}

	void Scene::RemoveEntity(Entity* entity)
	{
		removeList.push_back(entity);
		entity->markedForDestruction = true;
	}

	void Scene::Update(const float dt)
	{
		auto* node = mainList.GetHead();

		while (node != nullptr)
		{
			node->data->Update(dt);
			node = node->next;
		}

		ProcessChanges();
	}

	void Scene::ProcessChanges()
	{
		if (!removeList.empty())
		{
			for (Entity* e : removeList)
			{
				auto* node = mainList.GetHead();

				while (node != nullptr)
				{
					if (node->data.get() == e)
					{
						mainList.Remove(node);

						break;
					}

					node = node->next;
				}
			}

			removeList.clear();
		}

		if (!addList.empty())
		{
			for (auto& e : addList)
			{
				mainList.Add(std::move(e));
			}

			addList.clear();
		}
	}

	void Scene::DrawTarget(const Camera& camera)
	{
		renderTarget.Apply();
		meshRenderer->Draw(camera);
		world->Draw(camera);
	}
}
