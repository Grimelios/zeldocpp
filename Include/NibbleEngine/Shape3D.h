#pragma once
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include "ShapeTypes3D.h"

namespace Nibble
{
	class Shape3D
	{
	private:

		ShapeTypes3D shapeType;

	protected:

		explicit Shape3D(ShapeTypes3D type);

	public:

		virtual ~Shape3D() = default;

		glm::vec3 position = glm::vec3(0);
		glm::quat orientation = glm::quat(glm::vec3(0));

		ShapeTypes3D GetShapeType() const;

		bool Intersects(const Shape3D& other) const;

		virtual float ComputeVolume() const = 0;
	};
}
