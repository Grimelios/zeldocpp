#pragma once

namespace Nibble
{
	enum class SensorTypes
	{
		Entity,
		Trigger
	};
}
