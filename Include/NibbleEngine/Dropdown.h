#pragma once
#include "InputControl.h"
#include <memory>

namespace Nibble
{
	class Dropdown : public InputControl
	{
	private:

		friend class InputRenderer<Dropdown>;

		std::unique_ptr<InputRenderer<Dropdown>> renderer;

	public:

		Dropdown();

		template<class T>
		T* CreateRenderer();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T>
	T* Dropdown::CreateRenderer()
	{
		renderer = std::make_unique<T>(*this);

		return static_cast<T*>(renderer.get());
	}
}
