#pragma once
#include <glad/glad.h>

namespace Nibble
{
	class VertexAttribute
	{
	private:

		friend class Shader;

		GLenum type;

		int count;
		int offset;

		bool normalized;

	public:

		VertexAttribute(int count, int offset, GLenum type, bool normalized);
	};
}
