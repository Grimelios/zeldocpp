#pragma once

namespace Nibble
{
	class RenderContainer
	{
	private:

		friend class RenderHandle;
		friend class MeshRenderer;

		// For flat shading, these two values (vertex count and index count) will be the same. For smooth shading,
		// they'll be different (fewer vertices).
		int vertexCount;
		int indexCount;
		int instanceCount = 1;

	public:

		RenderContainer(int vertexCount, int indexCount);
	};
}
