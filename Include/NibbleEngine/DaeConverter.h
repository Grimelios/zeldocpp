#pragma once
#include "MeshConverter.h"

namespace Nibble
{
	class DaeConverter : public MeshConverter
	{
	protected:

		MeshPair ParseData(const std::string& path) override;
	};
}
