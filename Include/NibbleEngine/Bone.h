#pragma once
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class Bone
	{
	private:

		glm::vec3 position = glm::vec3(0);
		glm::quat orientation = glm::quat(glm::vec3(0));
		glm::mat4 worldMatrix = glm::mat4(1);

		int parentIndex;

	public:

		explicit Bone(int parentIndex);
	};
}
