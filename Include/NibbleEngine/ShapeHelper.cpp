#include "ShapeHelper.h"
#include "Shape3D.h"
#include "Sphere.h"
#include "GameFunctions.h"

namespace Nibble
{
	bool ShapeHelper::CheckIntersection(const Shape3D& shape1, const Shape3D& shape2)
	{
		const auto type1 = shape1.GetShapeType();
		const auto type2 = shape2.GetShapeType();

		switch (type1)
		{
			case ShapeTypes3D::Box: break;
			case ShapeTypes3D::Capsule: break;
			case ShapeTypes3D::Sphere:
				const Sphere& s1 = dynamic_cast<const Sphere&>(shape1);
				const Sphere& s2 = dynamic_cast<const Sphere&>(shape1);

				return CheckIntersection(s1, s2);
		}

		return false;
	}

	bool ShapeHelper::CheckIntersection(const Sphere& s1, const Sphere& s2)
	{
		const float distanceSquared = GameFunctions::ComputeDistanceSquared(s1.position, s2.position);
		const float sumRadius = s1.radius + s2.radius;

		return distanceSquared <= sumRadius * sumRadius;
	}
}
