#pragma once
#include <glad/glad.h>

namespace Nibble
{
	class QuadSource
	{
	protected:

		QuadSource();
		QuadSource(int width, int height);
		QuadSource(int width, int height, GLuint textureId);

		int width;
		int height;

		GLuint textureId;

	public:

		virtual ~QuadSource() = 0;

		int GetWidth() const;
		int GetHeight() const;

		GLuint GetTextureId() const;
	};

	inline QuadSource::~QuadSource() = default;
}
