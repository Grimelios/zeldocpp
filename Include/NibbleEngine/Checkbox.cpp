#include "Checkbox.h"
#include "InputRenderer.h"

namespace Nibble
{
	Checkbox::Checkbox() : InputControl(InputControlTypes::Checkbox)
	{
		valuePointer = &checked;
	}

	void Checkbox::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Checkbox::Draw(SpriteBatch& sb)
	{
		renderer->Draw(sb);
	}
}
