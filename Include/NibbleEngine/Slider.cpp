#include "Slider.h"
#include "InputRenderer.h"

namespace Nibble
{
	Slider::Slider() : InputControl(InputControlTypes::Slider)
	{
		valuePointer = &value;
	}

	void Slider::Update(const float dt)
	{
		renderer->Update(dt);
	}

	void Slider::Draw(SpriteBatch& sb)
	{
		renderer->Draw(sb);
	}
}
