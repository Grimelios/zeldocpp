#pragma once
#include "DoublyLinkedList.h"
#include "Sensor.h"
#include "SensorTypes.h"

namespace Nibble
{
	class Shape3D;
	class Sensor;
	class SensorHandle;
	class Space
	{
	private:

		// Having the space own sensors allows external classes to more easily keep optional pointers (without being
		// forced to use optional or unique pointers).
		DoublyLinkedList<Sensor> sensorList;

	public:

		Sensor* CreateSensor(void* parent, SensorTypes parentType, Shape3D& shape, SensorHandle& handle);

		void RemoveSensor(Sensor* sensor);
		void Step(float dt) const;
	};
}
