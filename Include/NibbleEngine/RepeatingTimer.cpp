#include "RepeatingTimer.h"

namespace Nibble
{
	RepeatingTimer::RepeatingTimer(const RepeatingFunction& trigger, const float elapsed) :
		RepeatingTimer(0, trigger, elapsed)
	{
	}

	RepeatingTimer::RepeatingTimer(const int duration, RepeatingFunction trigger, const float elapsed) : Timer(duration, elapsed),
		trigger(std::move(trigger))
	{
	}

	void RepeatingTimer::Update(const float dt)
	{
		if (paused)
		{
			return;
		}

		elapsed += dt;

		// A loop is required for durations less than a frame. It's also possible for a timer to pause itself during a
		// repeating function.
		while (elapsed >= duration && !paused)
		{
			// It's possible for a repeating function to change the timer's duration.
			const float previousDuration = duration;

			// Returning false from a repeating function means that the timer should end.
			if (!trigger(std::fmod(elapsed, duration)))
			{
				completed = true;

				return;
			}

			elapsed -= previousDuration;
		}

		if (tick.has_value())
		{
			tick->operator()(elapsed / duration);
		}
	}
}
