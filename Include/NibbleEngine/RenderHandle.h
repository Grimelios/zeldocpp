#pragma once

namespace Nibble
{
	class RenderContainer;
	class RenderHandle
	{
	private:

		friend class MeshRenderer;

		RenderContainer* container = nullptr;

	public:

		~RenderHandle();
	};
}
