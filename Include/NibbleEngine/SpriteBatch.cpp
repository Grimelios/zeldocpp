#include "SpriteBatch.h"
#include "Texture2D.h"
#include "Resolution.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "MessageSystem.h"
#include "Color.h"
#include "Bounds2D.h"
#include "Rectangle.h"
#include "GLConstants.h"

namespace Nibble
{
	SpriteBatch::SpriteBatch()
	{
		spriteShader.Attach(ShaderTypes::Vertex, "Sprite.vert");
		spriteShader.Attach(ShaderTypes::Fragment, "Sprite.frag");
		spriteShader.CreateProgram();
		spriteShader.AddAttribute<float>(2, GL_FLOAT, false);
		spriteShader.AddAttribute<float>(2, GL_FLOAT, false);
		spriteShader.AddAttribute<std::byte>(4, GL_UNSIGNED_BYTE, true);

		primitiveShader.Attach(ShaderTypes::Vertex, "Primitives2D.vert");
		primitiveShader.Attach(ShaderTypes::Fragment, "Primitives.frag");
		primitiveShader.CreateProgram();
		primitiveShader.AddAttribute<float>(2, GL_FLOAT, false);
		primitiveShader.AddAttribute<std::byte>(4, GL_UNSIGNED_BYTE, true);
		
		GLuint buffers[2];

		glGenBuffers(2, buffers);

		bufferId = buffers[0];
		indexBufferId = buffers[1];

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, BufferCapacity, nullptr, GL_DYNAMIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexCapacity * sizeof(unsigned short), nullptr, GL_DYNAMIC_DRAW);

		Apply(spriteShader);
		SetMode(GL_TRIANGLE_STRIP);

		MessageSystem::Subscribe(MessageTypes::Resize, handle, [this](const std::any& data, float dt)
		{
			OnResize();
		});

		OnResize();
	}

	SpriteBatch::~SpriteBatch()
	{
		GLuint buffers[2] =
		{
			bufferId,
			indexBufferId
		};

		glDeleteBuffers(2, buffers);
	}

	void SpriteBatch::OnResize()
	{
		const int w = Resolution::WindowWidth;
		const int h = Resolution::WindowHeight;
		const float wF = static_cast<float>(w);
		const float hF = static_cast<float>(h);

		const glm::ivec2& halfDimensions = glm::ivec2(w, h) / 2;

		projection = glm::ortho(0.0f, wF, 0.0f, hF);
		screenMatrix = scale(glm::mat4(1), glm::vec3(1.0f / halfDimensions.x, 1.0f / halfDimensions.y, 1));
		screenMatrix = translate(screenMatrix, glm::vec3(-halfDimensions, 0));
	}

	void SpriteBatch::Buffer(const std::byte* buffer, const unsigned vertexCount, const unsigned byteCount)
	{
		memcpy(currentPointer, buffer, byteCount);

		bufferSize += byteCount;
		currentPointer += byteCount;

		for (unsigned i = 0; i < vertexCount; i++)
		{
			indexBuffer[indexCount + i] = indexStart + i;
		}

		indexCount += vertexCount;
		indexStart += vertexCount;

		if (restartEnabled)
		{
			indexBuffer[indexCount] = GLConstants::RestartIndex;
			indexCount++;
		}
	}

	void SpriteBatch::Apply(Shader& shader)
	{
		if (activeShader == &shader)
		{
			return;
		}

		Flush();

		activeShader = &shader;

		if (!activeShader->IsBindingComplete())
		{
			activeShader->CompleteBinding(bufferId, indexBufferId);
		}
	}

	void SpriteBatch::UseSpriteShader()
	{
		Apply(spriteShader);
	}

	void SpriteBatch::SetMode(const GLenum mode)
	{
		if (this->mode == mode)
		{
			return;
		}

		Flush();

		this->mode = mode;

		if (mode == GL_TRIANGLE_STRIP || mode == GL_TRIANGLE_FAN || mode == GL_LINE_STRIP || mode == GL_LINE_LOOP)
		{
			restartEnabled = true;
		}
		else
		{
			restartEnabled = false;
		}
	}

	void SpriteBatch::BindTexture(const GLuint textureId)
	{
		if (activeTexture == textureId)
		{
			return;
		}

		Flush();

		activeTexture = textureId;
	}

	void SpriteBatch::DrawLine(const glm::vec2& p1, const glm::vec2& p2, const Color& color)
	{
		SetMode(GL_LINES);
		BindTexture(0);
		Apply(primitiveShader);

		const float f = color.ToFloat();
		const std::array<float, 6> data =
		{
			p1.x,
			p1.y,
			f,
			p2.x,
			p2.y,
			f
		};

		Buffer(data);
	}

	void SpriteBatch::DrawBounds(const Bounds2D& bounds, const Color& color)
	{
		SetMode(GL_LINE_LOOP);
		BindTexture(0);
		Apply(primitiveShader);

		const float f = color.ToFloat();
		const auto corners = bounds.ComputeCorners();
		
		std::array<float, 12> data { };

		unsigned index = 0;

		for (const glm::vec2& p : corners)
		{
			data[index] = p.x;
			data[index + 1] = p.y;
			data[index + 2] = f;
			
			index += 3;
		}

		Buffer(data);
	}

	void SpriteBatch::DrawRectangle(const Rectangle& rect, const Color& color)
	{
		SetMode(GL_LINE_LOOP);
		BindTexture(0);
		Apply(primitiveShader);

		const float f = color.ToFloat();
		const auto corners = rect.ComputeCorners();

		std::array<float, 12> data{};

		unsigned index = 0;

		for (const glm::ivec2& p : corners)
		{
			data[index] = static_cast<float>(p.x);
			data[index + 1] = static_cast<float>(p.y);
			data[index + 2] = f;

			index += 3;
		}

		Buffer(data);
	}

	void SpriteBatch::FillBounds(const Bounds2D& bounds, const Color& color)
	{
		FillArea(bounds.ComputeCorners(), color);
	}

	void SpriteBatch::FillRectangle(const Rectangle& rect, const Color& color)
	{
		FillArea(rect.ComputeCorners(), color);
	}

	void SpriteBatch::FillArea(const std::array<glm::ivec2, 4>& points, const Color& color)
	{
		SetMode(GL_TRIANGLE_STRIP);
		BindTexture(0);
		Apply(primitiveShader);

		const float f = color.ToFloat();

		std::array<float, 12> data{};
		std::array<int, 4> order = { 0, 1, 3, 2 };

		unsigned index = 0;

		for (int i : order)
		{
			const auto& p = points[i];

			data[index] = static_cast<float>(p.x);
			data[index + 1] = static_cast<float>(p.y);
			data[index + 2] = f;

			index += 3;
		}

		Buffer(data);
	}

	void SpriteBatch::Flush()
	{
		if (bufferSize == 0)
		{
			return;
		}
		
		// This assumes that all shaders have a uniform matrix called mvp.
		activeShader->Apply();
		activeShader->SetUniform("mvp", coords == BatchCoords::Screen ? screenMatrix : glm::mat4(1));

		if (restartEnabled)
		{
			glEnable(GL_PRIMITIVE_RESTART);
		}
		else
		{
			glDisable(GL_PRIMITIVE_RESTART);
		}

		if (activeTexture != 0)
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, activeTexture);
		}

		glBufferSubData(GL_ARRAY_BUFFER, 0, bufferSize, &buffer[0]);
		glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(unsigned short) * indexCount, &indexBuffer[0]);
		glDrawElements(mode, indexCount, GL_UNSIGNED_SHORT, nullptr);

		if (restartEnabled)
		{
			glDisable(GL_PRIMITIVE_RESTART);
		}

		// Flushing the batch resets the shader back to the sprite shader (rather than null).
		activeShader = &spriteShader;
		activeTexture = 0;
		bufferSize = 0;
		indexCount = 0;
		indexStart = 0;
		currentPointer = &buffer[0];
	}
}
