#pragma once
#include "Timer.h"

namespace Nibble
{
	class RepeatingTimer : public Timer
	{
	private:

		using RepeatingFunction = std::function<bool(float)>;

		RepeatingFunction trigger;

	public:

		// See a similar note in SingleTimer. Durations sometimes aren't known on object construction.
		explicit RepeatingTimer(const RepeatingFunction& trigger, float elapsed = 0);
		RepeatingTimer(int duration, RepeatingFunction trigger, float elapsed = 0);

		void Update(float dt) override;
	};
}
