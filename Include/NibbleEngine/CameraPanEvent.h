#pragma once
#include "CutsceneEvent.h"

namespace Nibble
{
	class CameraPanEvent : public CutsceneEvent
	{
	private:



	protected:

		void Execute(Scene& scene, float elapsed) override;

	public:

		explicit CameraPanEvent(const Json& j);
	};
}
