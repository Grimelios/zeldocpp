#pragma once
#include <array>
#include <vector>
#include "InputBind.h"

namespace Nibble
{
	class AggregateData
	{
	private:

		friend class InputProcessor;

		static constexpr int TypeCount = static_cast<int>(InputTypes::Count);

		// Pointers are used because aggregate data doesn't own the input data. It just uses that data for the function
		// calls during which the data objects are still in memory.
		using DataArray = std::array<InputData*, TypeCount>;

		DataArray dataArray;

		void SetData(InputTypes type, InputData& data);

	public:

		// The data array is initialized to all null, then filled in based on which input devices are active.
		AggregateData();

		template<class T>
		const T& GetData(InputTypes type) const;

		bool AnyButtonPressed() const;
		bool Query(const InputBind& bind, InputStates state) const;
		bool Query(const std::vector<InputBind>& binds, InputStates state) const;

		// If multiple buttons/keys are bound to a single action, it can be useful to not only check whether any were
		// activated, but WHICH one was activated.
		const InputBind* QueryBind(const std::vector<InputBind>& binds, InputStates state) const;
	};

	template <class T>
	const T& AggregateData::GetData(const InputTypes type) const
	{
		return *static_cast<T*>(dataArray[static_cast<int>(type)]);
	}
}
