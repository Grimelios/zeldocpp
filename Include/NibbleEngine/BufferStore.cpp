#include "BufferStore.h"

namespace Nibble
{
	BufferStore::BufferStore(const int bufferCapacity, const int indexCapacity, const GLenum usage)
	{
		GLuint buffers[2];

		glGenBuffers(2, buffers);

		bufferId = buffers[0];
		indexBufferId = buffers[1];

		glBindBuffer(GL_ARRAY_BUFFER, bufferId);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * bufferCapacity, nullptr, usage);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IndexType) * indexCapacity, nullptr, usage);
	}

	GLuint BufferStore::GetBufferId() const
	{
		return bufferId;
	}

	GLuint BufferStore::GetIndexBufferId() const
	{
		return indexBufferId;
	}
}
