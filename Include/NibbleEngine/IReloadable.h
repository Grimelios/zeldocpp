#pragma once

namespace Nibble
{
	class IReloadable
	{
	public:

		virtual ~IReloadable() = default;
		virtual void Reload() = 0;
	};
}
