#include "Box.h"

namespace Nibble
{
	Box::Box() : Box(1.0f, 1.0f, 1.0f)
	{
	}

	Box::Box(const float size) : Box(size, size, size)
	{
	}

	Box::Box(const float width, const float height, const float depth) : Shape3D(ShapeTypes3D::Box),
		width(width),
		height(height),
		depth(depth)
	{
	}

	void Box::SetDimensions(const glm::vec3& v)
	{
		width = v.x;
		height = v.y;
		depth = v.z;
	}

	float Box::ComputeVolume() const
	{
		return width * height * depth;
	}
}
