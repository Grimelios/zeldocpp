#pragma once
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace Nibble
{
	class BezierPoint
	{
	public:

		glm::vec3 position = glm::vec3(0);
		glm::quat orientation = glm::quat(glm::vec3(0));
	};
}
