#pragma once
#include <map>

namespace Nibble::MapUtilities
{
	template<class K, class V>
	V& Add(std::map<K, V>& map, const K& key)
	{
		return map.insert(std::map<K, V>::value_type(key, V())).first->second;
	}

	template<class K, class V>
	V& Add(std::map<K, V>& map, const K& key, const V& value)
	{
		return map.insert(std::map<K, V>::value_type(key, value)).first->second;
	}

	template<class K, class V>
	V& Add(std::map<K, V>& map, const K& key, V&& value)
	{
		return map.insert(std::map<K, V>::value_type(key, value)).first->second;
	}

	template<class K, class V>
	bool HasValue(std::map<K, V>& map, const K& key, V*& value)
	{
		auto i = map.find(key);

		if (i == map.end())
		{
			return false;
		}

		value = &i->second;

		return true;
	}
}
