#include "LivingEntity.h"
#include <algorithm>

namespace Nibble
{
	LivingEntity::LivingEntity(const int entityType) : Entity(entityType)
	{
	}

	void LivingEntity::ApplyDamage(const int damage)
	{
		if (damage == 0)
		{
			return;
		}

		// As mentioned in the Kill() function, there's an assumption that once an entity dies, it's dead, and
		// therefore can't have additional damage applied. In other words, health is assumed greater than zero at this
		// point in the code.
		const int oldHealth = health;

		health -= damage;
		health = std::max(health, 0);

		// Note that this function is called every time health changes, including when the entity dies.
		OnHealthChange(oldHealth, health);

		if (health == 0)
		{
			OnDeath();
		}
	}

	void LivingEntity::OnHealthChange(const int oldHealth, const int newHealth)
	{
	}

	void LivingEntity::OnDeath()
	{
	}

	void LivingEntity::Kill()
	{
		// This assumes that this function will only be called for entities that are alive (i.e. their current health
		// is greater than zero).
		const int oldHealth = health;

		health = 0;

		OnHealthChange(oldHealth, 0);
		OnDeath();
	}
}
