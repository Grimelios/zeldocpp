#pragma once
#include "Color.h"
#include "GenericController.h"

namespace Nibble
{
	class IColorable;
	class ColorTransition : public GenericController<Color>
	{
	private:

		IColorable* target;

	protected:

		void Lerp(float t) override;

	public:

		explicit ColorTransition(IColorable* target);
	};
}
