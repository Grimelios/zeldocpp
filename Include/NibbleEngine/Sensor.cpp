#include "Sensor.h"
#include "Shape3D.h"
#include "Space.h"

namespace Nibble
{
	Sensor::Sensor(void* parent, const SensorTypes parentType, Shape3D& shape) :
		parent(parent),
		parentType(parentType),
		shape(shape)
	{
	}

	bool Sensor::IsEnabled() const
	{
		return enabled;
	}

	void Sensor::SetEnabled(const bool enabled)
	{
		if (!contactList.IsEmpty())
		{
			auto* node = contactList.GetHead();

			do
			{
				Sensor* other = node->data;
				other->onSeparate.Invoke(this, parentType);
				other->contactList.Remove(this);
				node = node->next;
			}
			while (node != nullptr);
		}

		this->enabled = enabled;
	}

	void* Sensor::GetParent() const
	{
		return parent;
	}

	Shape3D& Sensor::GetShape() const
	{
		return shape;
	}

	SensorTypes Sensor::GetParentType() const
	{
		return parentType;
	}

	const glm::vec3& Sensor::GetPosition() const
	{
		return shape.position;
	}

	const glm::quat& Sensor::GetOrientation() const
	{
		return shape.orientation;
	}

	void Sensor::SetPosition(const glm::vec3& position)
	{
		shape.position = position;
	}

	void Sensor::SetOrientation(const glm::quat& orientation)
	{
		shape.orientation = orientation;
	}

	void Sensor::SetTransform(const glm::vec3& position, const glm::quat& orientation)
	{
		shape.position = position;
		shape.orientation = orientation;
	}
}
