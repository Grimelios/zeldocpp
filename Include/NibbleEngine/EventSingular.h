#pragma once
#include <functional>

namespace Nibble
{
	template<class R, class... Args>
	class EventSingular
	{
	private:

		using Func = std::function<R(Args...)>;

		Func func;

	public:

		EventSingular& operator=(Func&& other) noexcept;

		// The idea behind this class is to wrap std::function in order to avoid null checks all over the codebase.
		// That said, this function is useful for classes that execute some default behavior if another callback isn't
		// set.
		bool HasValue() const;

		R Invoke(Args... args);
	};

	template<class R, class... Args>
	EventSingular<R, Args...>& EventSingular<R, Args...>::operator=(Func&& other) noexcept
	{
		func = std::move(other);

		return *this;
	}

	template<class R, class ... Args>
	bool EventSingular<R, Args...>::HasValue() const
	{
		return func != nullptr;
	}

	template<class R, class... Args>
	R EventSingular<R, Args...>::Invoke(Args... args)
	{
		if (func)
		{
			return func(args...);
		}

		// See https://stackoverflow.com/questions/3305876/using-template-for-return-value-how-to-handle-void-return.
		return static_cast<R>(nullptr);
	}
}
