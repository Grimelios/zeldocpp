#pragma once
#include "IDynamic.h"
#include "IRenderable2D.h"
#include <vector>
#include <memory>
#include "CanvasElement.h"
#include "MessageHandle.h"
#include <nlohmann/json.hpp>

namespace Nibble
{
	class Canvas : public IDynamic, public IRenderable2D
	{
	private:

		using Json = nlohmann::json;

		// Using a separate function makes it easier to correctly position new elements that are created on the fly.
		static void PlaceElement(std::unique_ptr<CanvasElement>& e);

		std::vector<std::unique_ptr<CanvasElement>> elements;

		MessageHandle handle;

		void OnResize();

	public:

		Canvas();

		// This function is commonly used when loading HUD elements from a Json file.
		template<class T>
		T* CreateElement(const Json& j);

		template<class T, class... Args>
		T* CreateElement(Args&&... args);

		template<class T>
		T* GetElement();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T>
	T* Canvas::CreateElement(const Json& j)
	{
		// This assumes an empty constructor on all elements.
		auto e = std::make_unique<T>();

		const int x = j.at("OffsetX").get<int>();
		const int y = j.at("OffsetY").get<int>();

		e->anchor = j.at("Anchor").get<Alignments>();
		e->offset = glm::ivec2(x, y);

		elements.push_back(std::move(e));

		auto& b = elements.back();

		PlaceElement(b);

		return static_cast<T*>(b.get());
	}

	template<class T, class... Args>
	T* Canvas::CreateElement(Args&&... args)
	{
		elements.push_back(std::make_unique<T>(args...));

		auto& e = elements.back();

		PlaceElement(e);

		return static_cast<T*>(e.get());
	}

	template<class T>
	T* Canvas::GetElement()
	{
		for (auto& e : elements)
		{
			T* item = dynamic_cast<T*>(e.get());

			if (item != nullptr)
			{
				return item;
			}
		}

		return nullptr;
	}
}
