#pragma once
#include "InputControl.h"
#include <memory>

namespace Nibble
{
	class Checkbox : public InputControl
	{
	private:

		friend class InputRenderer<Checkbox>;

		bool checked = false;

		std::unique_ptr<InputRenderer<Checkbox>> renderer;

	public:

		Checkbox();

		template<class T>
		T* CreateRenderer();

		void Update(float dt) override;
		void Draw(SpriteBatch& sb) override;
	};

	template<class T>
	T* Checkbox::CreateRenderer()
	{
		renderer = std::make_unique<T>(*this);

		return static_cast<T*>(renderer.get());
	}
}
