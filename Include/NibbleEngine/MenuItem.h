#pragma once

namespace Nibble
{
	class Menu;
	class MenuItem
	{
	private:

		Menu* parent;

	protected:

		explicit MenuItem(Menu* parent);

	public:

		virtual ~MenuItem() = 0;

		bool enabled = true;
	};

	inline MenuItem::~MenuItem() = default;
}
