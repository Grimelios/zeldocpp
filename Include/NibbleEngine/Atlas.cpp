#include "Atlas.h"
#include "FileUtilities.h"
#include "StringUtilities.h"
#include "MapUtilities.h"
#include "GameFunctions.h"
#include "Paths.h"

namespace Nibble
{
	Atlas Atlas::Load(const std::string& filename)
	{
		const auto lines = FileUtilities::ReadAllLines(Paths::Atlases + filename);

		SourceMap map;

		for (const auto& line : lines)
		{
			if (line.empty())
			{
				continue;
			}

			const int index = StringUtilities::IndexOf(line, '=');

			const std::string key = line.substr(0, index - 1);
			const std::string value = line.substr(index + 2);

			MapUtilities::Add(map, key, GameFunctions::ParseRect(value));
		}

		return Atlas(map);
	}

	Atlas::Atlas(std::map<std::string, Rectangle> map) : map(std::move(map))
	{
	}

	const Rectangle& Atlas::operator[](const std::string& key) const
	{
		// This assumes the given key is valid.
		return map.at(key);
	}
}
