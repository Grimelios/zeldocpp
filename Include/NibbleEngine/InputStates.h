#pragma once

namespace Nibble
{
	enum class InputStates
	{
		Held = 1,
		Released = 4,
		PressedThisFrame = 3,
		ReleasedThisFrame = 12
	};

	inline InputStates operator&(InputStates s1, InputStates s2)
	{
		return static_cast<InputStates>(static_cast<int>(s1) & static_cast<int>(s2));
	}
}
