#pragma once
#include "DoublyLinkedList.h"
#include "SensorTypes.h"
#include "EventSingular.h"
#include "ITransformable.h"

namespace Nibble
{
	class Space;
	class Shape3D;
	class Entity;
	class Sensor : public ITransformable
	{
	private:

		using ContactList = DoublyLinkedList<Sensor*>;

		// While rigid bodies can use entities as their parents, sensors are more generic. For example, sensors can be
		// associated with invisible triggers.
		void* parent;

		SensorTypes parentType;
		Shape3D& shape;

		// This variable uses public methods so that, if disabled, all existing contacts can be safely removed.
		bool enabled = true;

	public:

		Sensor(void* parent, SensorTypes parentType, Shape3D& shape);

		// For physics bodies, the onCollision callback can return false to ignore that specific collision. That
		// restriction isn't needed here since sensor targets can simply do nothing if the callback should be
		// ignored.
		EventSingular<void, void*, SensorTypes> onSense;
		EventSingular<void, void*, SensorTypes> onSeparate;
		ContactList contactList;

		bool IsEnabled() const;

		void* GetParent() const;

		SensorTypes GetParentType() const;
		Shape3D& GetShape() const;

		const glm::vec3& GetPosition() const override;
		const glm::quat& GetOrientation() const override;

		void SetEnabled(bool enabled);
		void SetPosition(const glm::vec3& position) override;
		void SetOrientation(const glm::quat& orientation) override;
		void SetTransform(const glm::vec3& position, const glm::quat& orientation) override;
	};
}
